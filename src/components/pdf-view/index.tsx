import React, { useState } from 'react';

import { Document, Page, pdfjs } from 'react-pdf';

import PDFFile from '@assets/test.pdf';
pdfjs.GlobalWorkerOptions.workerSrc = `//cdnjs.cloudflare.com/ajax/libs/pdf.js/${pdfjs.version}/pdf.worker.min.js`;
const PDFView = () => {
  const [numPages, setNumPages] = useState(0);
  const [pageNumber, setPageNumber] = useState(1);
  // @ts-ignore
  const onDocumentLoadSuccess = ({ numPages }) => {
    setNumPages(numPages);
  };
  return (
    <div>
      <Document file={PDFFile} onLoadSuccess={onDocumentLoadSuccess} renderMode="svg">
        {new Array(numPages).fill('').map((item, index) => (
          <Page pageNumber={index + 1} />
        ))}
      </Document>
    </div>
  );
};
export default PDFView;
