import { translations } from '@locales/i18n';
import { ResponseType } from '@modal/reducer/modal';
import { message, Table } from 'antd';
import { ColumnsType } from 'antd/lib/table';
import React, { useEffect, useState, useImperativeHandle } from 'react';
import { useTranslation } from 'react-i18next';
import FilterForm from './filter-form';
import { FieldType } from './filter-form/model';
import Style from './index.module.less';
interface TableTemplateProps {
  columns: ColumnsType<any>;
  initSearchKey?: any;
  queryAjax: (...args: any[]) => Promise<any>;
  rowKey: ((record: any) => string) | string;
  filterFields?: FieldType[];
  hidePagination?: boolean;
  showRowSelect?: boolean;
  topButtons?: any[];
  modalTable?: boolean;
  onSelectChange?: (selectedKeys: string[], selectRows: any[]) => void;
  selectType?: 'radio' | 'checkbox';
  refresh?: { count: number; isCurrent?: boolean };
  selectedKeys?: any[];
}
const TableTemplate = React.forwardRef((props: TableTemplateProps, ref) => {
  const {
    filterFields,
    columns,
    queryAjax,
    hidePagination,
    showRowSelect,
    rowKey,
    topButtons,
    modalTable,
    onSelectChange,
    selectType = 'checkbox',
    refresh,
    initSearchKey,
    selectedKeys,
  } = props;
  const {
    table: { NO },
  } = translations;
  const { t } = useTranslation();
  const [loading, setLoading] = useState(false);
  const [dataSource, setDataSource] = useState([]);
  const [pageInfo, setPageInfo] = useState({ currentPage: 1, pageSize: 10, total: 0 });
  const [searchKey, setSearchKey] = useState({} as any);
  const [selectedDataKeys, setSelectedDataKeys] = useState([] as string[]);
  const [selectedData, setSelectedData] = useState([] as any[]);
  const getSelectedDataKeys = () => selectedDataKeys;
  const getSelectedData = () => selectedData;
  const doSearch = (params: any) => {
    setLoading(true);
    queryAjax(params)
      .then((res: ResponseType) => {
        if (res.code === 200) {
          setDataSource(res.data.list);
          setPageInfo({ ...pageInfo, total: res.data?.total || 0 });
        } else {
          message.error(res.msg);
        }
      })
      .finally(() => setLoading(false));
  };
  const handleSearchButtonClick = (filterKey: { [key: string]: any }) => {
    const keys = { ...searchKey, ...filterKey };
    const params = {
      pageInfo: { ...pageInfo, currentPage: 1 },
      searchKeys: keys,
    };
    doSearch(params);
    setSearchKey(keys);
    setPageInfo(params.pageInfo);
  };
  const handlePageChange = (page: number) => {
    doSearch({
      pageInfo: { ...pageInfo, currentPage: page },
      ...searchKey,
    });
    setPageInfo({ ...pageInfo, currentPage: page });
  };
  const handleSelectChange = (selectedRowKeys: any[], selectedRows: any[]) => {
    setSelectedDataKeys(selectedRowKeys);
    setSelectedData(selectedRows);
    onSelectChange && onSelectChange(selectedRowKeys, selectedRows);
  };
  const refreshCurrentPage = () => {
    doSearch({ pageInfo, ...searchKey });
  };
  useImperativeHandle(
    ref,
    () => ({
      getSelectedDataKeys,
      getSelectedData,
      refreshCurrentPage,
    }),
    [selectedDataKeys],
  );
  useEffect(() => {
    doSearch({
      pageInfo: { ...pageInfo, currentPage: 1 },
      ...initSearchKey,
    });
    if (initSearchKey) {
      setSearchKey(initSearchKey);
    }
  }, [initSearchKey]);
  useEffect(() => {
    if (refresh) {
      doSearch({
        ...pageInfo,
        ...searchKey,
        currentPage: refresh.isCurrent ? pageInfo.currentPage : 1,
      });
    }
  }, [refresh]);
  useEffect(() => {
    if (selectedKeys) {
      setSelectedDataKeys(selectedKeys);
    }
  }, [selectedKeys]);
  return (
    <section className={Style.table_template + ' ' + (modalTable ? Style.isModal : '')}>
      {filterFields && <FilterForm modalTable={modalTable} fields={filterFields} onSearch={handleSearchButtonClick} />}
      {topButtons && <div className={Style.top_button}>{topButtons.map((item: any) => item)}</div>}

      <Table
        className={Style.customer_table}
        loading={loading}
        columns={
          [
            {
              title: t(NO),
              dataIndex: 'no',
              render: (text: any, record: any, index: number) => {
                return index * pageInfo.currentPage + 1;
              },
            },
            ,
            ...columns,
          ] as any
        }
        rowKey={rowKey || ((record: any) => record.id)}
        dataSource={dataSource}
        scroll={{ x: 'max-content' }}
        pagination={
          hidePagination
            ? false
            : {
                onChange: handlePageChange,
                pageSize: pageInfo.pageSize,
                total: pageInfo.total,
              }
        }
        rowSelection={
          !showRowSelect
            ? undefined
            : {
                type: selectType,
                selectedRowKeys: selectedDataKeys,
                onChange: handleSelectChange,
              }
        }
      />
    </section>
  );
});

export default TableTemplate;
