import { Button, Col, Form, Row } from 'antd';
import React, { useState } from 'react';
import FieldItem from './field-item';
import { FieldType } from './model';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import { DownOutlined, UpOutlined } from '@ant-design/icons';
import Style from './index.module.less';
interface FilterFormProps {
  fields: FieldType[];
  onSearch: (...args: any[]) => void;
  modalTable?: boolean;
}

const FilterForm = (props: FilterFormProps) => {
  const { fields = [], onSearch, modalTable } = props;
  const [showMore, setShowMore] = useState(false);
  const [form] = Form.useForm();
  const { t } = useTranslation();
  const { buttons } = translations;
  const handleSearch = () => {
    onSearch && onSearch(form.getFieldsValue());
  };
  const calculateOffset = (): number => {
    return fields.length % 4 === 0 && !showMore ? 0 : 18 - (fields.length % 4) * 6;
  };
  const switchShowMore = (show: boolean) => {
    setShowMore(show);
  };
  return (
    <section className={Style.filter_form + ' ' + (modalTable ? Style.isModal : '')}>
      <Form form={form} layout="horizontal">
        <Row gutter={24}>
          {fields.slice(0, showMore ? fields.length : 3).map((field: FieldType) => (
            <Col span={6} key={field.code}>
              <FieldItem field={field} key={field.code} />
            </Col>
          ))}
          <Col span={6} offset={calculateOffset()}>
            <Form.Item style={{ textAlign: 'right' }}>
              <Button type="primary" onClick={handleSearch}>
                {t(buttons.Search)}
              </Button>
              <Button style={{ margin: '0 10px' }} onClick={() => form.resetFields()}>
                {t(buttons.Clear)}
              </Button>
              {fields.length > 3 &&
                (showMore ? (
                  <Button type="link" icon={<UpOutlined />} onClick={() => switchShowMore(false)}>
                    {t(buttons.Packup)}
                  </Button>
                ) : (
                  <Button type="link" icon={<DownOutlined />} onClick={() => switchShowMore(true)}>
                    {t(buttons.Open)}
                  </Button>
                ))}
            </Form.Item>
          </Col>
        </Row>
      </Form>
    </section>
  );
};
export default FilterForm;
