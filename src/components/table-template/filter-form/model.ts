export interface FieldBaseType {
  label?: string;
  code: string;
}
export interface SelectFieldType extends FieldBaseType {
  type: 'select';
  isMultiple?: boolean;
  options?: { [key: string]: string | number }[];
  isAsync?: boolean;
  getOptions?: (...args: any[]) => Promise<any>;
}

export interface InputFieldType extends FieldBaseType {
  type: 'input';
  placeholder?: string;
  maxLength?: number;
  minLength?: number;
}

export interface DateRangeType extends FieldBaseType {
  type: 'dateRange';
}

export type FieldType = SelectFieldType | DateRangeType | InputFieldType;
