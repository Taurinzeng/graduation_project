import { DatePicker, Form, Input, Select } from 'antd';
import React, { useEffect, useState } from 'react';
import { FieldType } from '../model';

const FieldItem = (props: { field: FieldType }) => {
  const { field } = props;
  const [options, setOptions] = useState([]);
  const renderField = (field: FieldType) => {
    switch (field.type) {
      case 'input':
        return <Input maxLength={field.maxLength} minLength={field.minLength} placeholder={field.placeholder}/>;
      case 'select':
        if (field.isAsync && field.getOptions) {
          return (
            <Select>
              {options.map((option: any) => (
                <Select.Option key={option.value} value={option.value}>
                  {option.text}
                </Select.Option>
              ))}
            </Select>
          );
        } else {
          return (
            <Select>
              {field.options?.map((option) => (
                <Select.Option key={option.value} value={option.value}>
                  {option.text}
                </Select.Option>
              ))}
            </Select>
          );
        }
      case 'dateRange':
        return <DatePicker.RangePicker />;
    }
  };
  useEffect(() => {
    if (field.type === 'select' && field.isAsync && field.getOptions) {
      field.getOptions().then((res) => {
        setOptions(res.content);
      });
    }
  }, []);
  return (
    <Form.Item name={field.code} label={field.label}>
      {renderField(field)}
    </Form.Item>
  );
};
export default FieldItem;
