import { translations } from '@locales/i18n';
import { Button, Form, Input, Modal } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
interface IProps {
  text: string | number | undefined;
  label: string;
  onSave?: (...args: any[]) => void;
  rules?: any[];
}
const ChangeableInput = (props: IProps) => {
  const { text, onSave, rules = [], label } = props;
  const {
    buttons: { Edit, Update },
  } = translations;
  const [visible, setVisible] = useState(false);
  const { t } = useTranslation();
  return (
    <>
      <span>{text}</span>
      <Button type="link" onClick={() => setVisible(true)}>
        {t(Edit)}
      </Button>
      <Modal visible={visible} title={label + t(Update)} onCancel={() => setVisible(false)}>
        <Form>
          <Form.Item name="editValue" rules={rules} label={label}>
            <Input />
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
};

export default ChangeableInput;
