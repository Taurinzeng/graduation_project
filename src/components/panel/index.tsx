import { translations } from '@locales/i18n';
import React, { ReactElement } from 'react';
import { useTranslation } from 'react-i18next';
import { IconfontWidgetOperate } from '@components/iconfont';
import './index.less';

interface IProps {
  title: React.ReactNode;
  moreUrl?: string;
  children?: ReactElement;
}

export default (props: IProps) => {
  const { title, children, moreUrl } = props;
  const { t } = useTranslation();
  const {
    buttons: { More },
  } = translations;
  return (
    <section className="module-panel">
      <div className="header">
        <span className="title">{title}</span>
        {moreUrl && (
          <a className="more" href={moreUrl}>
            {t(More)}
            <IconfontWidgetOperate type={'icon-more'} />
          </a>
        )}
      </div>
      {children}
    </section>
  );
};
