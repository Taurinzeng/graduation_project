import React from 'react';
import { Row, Col } from 'antd';
import Style from './index.module.less';
interface ItemType {
  count: number;
  title: string;
}
interface IProps {
  items: ItemType[][];
  total?: ItemType[];
}
const CountBar = (props: IProps) => {
  const { total, items } = props;
  return (
    <div className={Style.count_bar}>
      <div className={Style.inner}>
        <div className={Style.left}>
          {total?.map((unit: ItemType) => (
            <div className={Style.totalUnit}>
              <span className={Style.title}>{unit.title}</span>
              <br />
              <span className={Style.count}>{unit.count}</span>
            </div>
          ))}
        </div>

        <div className={Style.right}>
          {items.map((lineItem: ItemType[]) => (
            <Row>
              {lineItem.map((item: ItemType) => {
                const colSpan =
                  Math.floor(24 / lineItem.length) > 4 ? undefined : Math.floor(24 / lineItem.length);

                return (
                  <Col span={colSpan} className={`${colSpan ? '' : Style.noSpan}`}>
                    <span className={Style.title}>{item.title}</span>
                    <br />
                    <span className={Style.count}>{item.count}</span>
                  </Col>
                );
              })}
            </Row>
          ))}
        </div>
      </div>
    </div>
  );
};
export default CountBar;
