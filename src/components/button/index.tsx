import React from "react";
import "./index.less";
const FooterButtons = (props: {btns: React.ReactNode[]}) => {
  const {btns} = props;
  return <div className={'btns'}>
    {btns.map((every: React.ReactNode, index: number) => {
      return every;
    })}
    </div>
}

export default FooterButtons;