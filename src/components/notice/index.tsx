import React from 'react';
import Style from './index.module.less';

const Notice = (props: any) => {
  return <div className={Style.notice}>{props.children}</div>;
};

export default Notice;
