/**
 * 富文本组件
 */
import React from 'react';
import Quill from 'quill';
import resizeImage from 'quill-image-resize-module';
import ReactQuill from 'react-quill';
import 'react-quill/dist/quill.snow.css';
Quill.register('modules/resizeImage ', resizeImage);

interface RichEditorProps {
  editorId?: string;
  value?: string;
  onChange?: (...args: any[]) => void;
  width?: string;
  height?: string;
}
class RichEditor extends React.Component<RichEditorProps> {
  quill = {} as any;
  constructor(props: RichEditorProps) {
    super(props);
  }
  handleImageIconClick = () => {
    const input: any = document.querySelector('#editorUploadImage');
    input.click();
  };
  handleUploadImage = (v: any) => {
    const file = v.target.files[0];
    //TODO处理上传逻辑；

    //上传后将地址放入编辑器中
    const editor = this.quill.getEditor();
    const addImageRange = editor.getSelection();
    const newRange = 0 + (addImageRange !== null ? addImageRange.index : 0);
    editor.insertEmbed(newRange, 'image', 'https://vkceyugu.cdn.bspapp.com/VKCEYUGU-566db12e-1d3c-4aea-99af-eabf8a1d35c9/0f2dea25-47a9-4cde-9157-d7257837a525.jpg');
    editor.setSelection(1 + newRange);
  };
  componentDidMount() {
    this.quill.getEditor().container.style.height = this.props.height;
    this.quill.getEditor().container.style.width = this.props.width;
  }
  render() {
    return (
      <>
        <input id="editorUploadImage" type="file" style={{ display: 'none' }} accept="image/png, image/jpeg, image/gif" onChange={this.handleUploadImage} />
        <ReactQuill
          disabled
          value={this.props.value || ''}
          onChange={this.props.onChange}
          ref={(el: any) => {
            this.quill = el;
          }}
          modules={{
            imageResize: {
              displayStyles: {
                backgroundColor: 'black',
                border: 'none',
                color: 'white',
              },
              modules: ['Resize', 'DisplaySize', 'Toolbar'],
            },
            toolbar: {
              container: ['bold', 'italic', 'blockquote', 'code-block', 'link', 'image', { list: 'ordered' }, { list: 'bullet' }],
              handlers: {
                image: this.handleImageIconClick,
              },
            },
          }}
        />
      </>
    );
  }
}
export default RichEditor;
