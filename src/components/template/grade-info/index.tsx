/**
 * 成绩概览
 */
import React from 'react';
import { Row, Col, Table } from 'antd';
import { translations } from '@locales/i18n';
import { useTranslation } from 'react-i18next';
import TemplateComponent from '../template-component';

const GradeInfo = () => {
  const labelSpan = 4;
  const {
    project,
    table: { Supervisor },
  } = translations;
  const { t } = useTranslation();
  const columns = [
    {
      title: 'Grade item',
      dataIndex: 'gradeItem',
    },
    {
      title: 'Calculated',
      dataIndex: 'Calculated',
    },
    {
      title: 'Range',
      dataIndex: 'Range',
    },
    {
      title: 'Grade',
      dataIndex: 'grade',
      render: () => {
        return (
          <>
            <div className="dataUnit">a</div>
            <div className="dataUnit">b</div>
            <div className="dataUnit">c</div>
          </>
        );
      },
    },
    {
      title: 'Examiner',
      dataIndex: 'examiner',
      render: () => {
        return (
          <>
            <div className="dataUnit">a</div>
            <div className="dataUnit">b</div>
            <div className="dataUnit">c</div>
          </>
        );
      },
    },
  ];
  return (
    <TemplateComponent>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(project.ProjectTitle)}
        </Col>
        <Col className="value">xxx</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(project.SelectedStudents)}
        </Col>
        <Col className="value" span={labelSpan}>
          xxx
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(Supervisor)}
        </Col>
        <Col className="value">xxx</Col>
      </Row>
      <Table bordered pagination={false} columns={columns} dataSource={[{ gradeItem: 'Final Grage', Calculated: '50%', Range: '0-100', grade: [] }]} />
    </TemplateComponent>
  );
};

export default GradeInfo;
