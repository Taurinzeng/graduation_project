/**
 * 初期检查表（校、院级）（单双学位同用）
 */
import React from 'react';
import { Row, Col } from 'antd';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import TemplateComponent from '../template-component';

interface IProps {
  type: 'd' | 's';
  tableId: string;
}
const PreliminarySchoolTable = (props: IProps) => {
  const labelSpan = 4;
  const valueSpan = 3;
  const { type, tableId } = props;
  const { t } = useTranslation();
  const { schoolTitle, student, commonInfo, detail, project, title } = translations;
  return (
    <TemplateComponent>
      <div className="title">{t(schoolTitle)}</div>
      <div className="subTitle">2021{t(type === 'd' ? title.PRTitleD : title.PRTitleS)}</div>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.Academy)}
        </Col>
        <Col className="value" span={8}>
          格拉斯学院
        </Col>
        <Col className="label" span={labelSpan}>
          {t(detail.FillDate)}
        </Col>

        <Col className="value" span={8}>
          2020-20-20
        </Col>
      </Row>
      <div className="blank">{t(detail.StuFill)}</div>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(student.name)}
        </Col>
        <Col className="value" span={6}>
          xx
        </Col>
        <Col className="label" span={labelSpan}>
          {t(student.UESTICID)}
        </Col>
        <Col className="value" span={6}>
          333223
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.ProjectFrom)}
        </Col>
        <Col className="value" span={6}>
          生产
        </Col>
        <Col className="label" span={labelSpan}>
          {t(detail.Supervisor)}
        </Col>
        <Col className="value" span={6}>
          xxx
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(project.ProjectTitle)}
        </Col>
        <Col className="value">理论研究</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.Position)}
        </Col>
        <Col className="value">xxxx</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.TimeRange)}
        </Col>
        <Col className="value">2020年7月12日 至 2021年7月23日</Col>
      </Row>
      <div className="blank">{t(detail.TeacherFill)}</div>

      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.ProjectSize)}
        </Col>
        <Col className="value" span={8}>
          爆满
        </Col>
        <Col className="label" span={labelSpan}>
          {t(detail.Knowle)}
        </Col>
        <Col className="value" span={8}>
          适中
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.ProjectDiffcult)}
        </Col>
        <Col className="value" span={8}>
          适中
        </Col>
        <Col className="label" span={labelSpan}>
          {t(detail.ProjectValue)}
        </Col>
        <Col className="value" span={8}>
          价值一般
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.CheckSuggestion)}
        </Col>
      </Row>
      <Row className="item">
        <Col className="value">完善的非常好，优秀</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.CurrentGrade)}
        </Col>
        <Col className="value">优秀</Col>
      </Row>
    </TemplateComponent>
  );
};

export default PreliminarySchoolTable;
