/**
 * 任务书
 */
import React from 'react';
import { Row, Col } from 'antd';
import Style from './index.module.less';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import TemplateComponent from '../template-component';

const Task = () => {
  const labelSpan = 4;
  const valueSpan = 8;
  const { t } = useTranslation();
  const { detail, title, student, commonInfo, schoolTitle } = translations;
  return (
    <TemplateComponent>
      <div className="title">{t(schoolTitle)}</div>
      <div className="subTitle">2021{t(title.TaskTitle)}</div>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.CreateUnit)}
        </Col>
        <Col className="value" span={valueSpan}>
          电子科技大学格拉斯学院
        </Col>
        <Col className="label" span={labelSpan}>
          {t(detail.Auditor)}
        </Col>
        <Col className="value" span={valueSpan}>
          林鹤
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.ProjectAndSubTitle)}
        </Col>
        <Col className="value">苹果产品在当前环境下的竞争优势分析</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.ProjectFrom)}
        </Col>
        <Col className="value">苹果产品在当前环境下的竞争优势分析</Col>
      </Row>
      <Row className="item">
        <Col className="label">{t(detail.MainTask)}</Col>
      </Row>
      <Row className="item">
        <Col className="value">
          监管的目的是控制风险，虽然监管当局逐步意识到风险监管对金融业发展的重要性，信托公司也在逐步控制信托资金的投放风险，但现行的政策主要是合规性监管，对风险监管缺乏主动性。监管部门应当制定一定的措施，能对风险进行实时监控，当风险达到一定水平时进行预警，把风险控制在合理范围内，减少危机的发生。对于风险性较高的金融行业，其发展首先要做到稳健，因此控制风险极其重要，我国的信托监管应尽快实现由传统的以合规性监管为主到风险监管的转变
        </Col>
      </Row>
      <Row className="item">
        <Col className="label">{t(detail.ExpectAndGoals)}</Col>
      </Row>
      <Row className="item">
        <Col className="value">
          监管的目的是控制风险，虽然监管当局逐步意识到风险监管对金融业发展的重要性，信托公司也在逐步控制信托资金的投放风险，但现行的政策主要是合规性监管，对风险监管缺乏主动性。监管部门应当制定一定的措施，能对风险进行实时监控，当风险达到一定水平时进行预警，把风险控制在合理范围内，减少危机的发生。对于风险性较高的金融行业，其发展首先要做到稳健，因此控制风险极其重要，我国的信托监管应尽快实现由传统的以合规性监管为主到风险监管的转变
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.GoalsOutcome)}
        </Col>
        <Col className="value">苹果产品在当前环境下的竞争优势分析</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.TimeRange)}
        </Col>
        <Col className="value">2020年7月13日 至 2021年4月13日</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(student.name)}
        </Col>
        <Col className="value" span={valueSpan}>
          林鹤
        </Col>
        <Col className="label" span={labelSpan}>
          {t(detail.Academy)}
        </Col>
        <Col className="value" span={valueSpan}>
          苹果产品在当前环境下的竞争优势分析
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.Major)}
        </Col>
        <Col className="value" span={valueSpan}>
          林鹤
        </Col>
        <Col className="label" span={labelSpan}>
          {t(student.No)}
        </Col>
        <Col className="value" span={valueSpan}>
          苹果产品在当前环境下的竞争优势分析
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.GuidenceUnit)}
        </Col>
        <Col className="value" span={valueSpan}>
          电子科技大学格拉斯学院
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.GuidenceInfo)}
        </Col>
        <Col className="value">林鹤 教授</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.Position)}
        </Col>
        <Col className="value">电子科技大学格拉斯学院</Col>
      </Row>
    </TemplateComponent>
  );
};

export default Task;
