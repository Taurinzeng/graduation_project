/**
 * 双学位教师评审成绩信息（中英公用）
 */
import { Col, Table } from 'antd';
import Row from 'rc-table/lib/Footer/Row';
import React from 'react';
import TemplateComponent from '../template-component';
interface IProps {
  gradeId: string;
}
const TeacherGradeInfo = (props: IProps) => {
  const { gradeId } = props;
  const columns: any = [
    {
      title: (
        <div>
          <div>UoG Grade Range</div>
          <div>(UESTC %age)</div>
        </div>
      ),
      align: 'center',
      width: 150,
      dataIndex: 'gradeRange',
      render: (text: string, row: any, index: number) => {
        if (index < 7) {
          return text;
        } else {
          return {
            children: text,
            props: {
              colSpan: 2,
            },
          };
        }
      },
    },
    {
      title: <div>Descriptor</div>,
      align: 'center',
      dataIndex: 'descriptor',
      render: (text: string, row: any, index: number) => {
        if (index < 7) {
          return text;
        } else {
          return {
            children: text,
            props: {
              colSpan: 0,
            },
          };
        }
      },
    },
    {
      title: (
        <div>
          <div>Writing</div>
          <div>(Weighting = 1)</div>
        </div>
      ),
      align: 'center',
      dataIndex: 'Writing',
    },
    {
      title: (
        <div>
          <div>Presentation & Figures</div>
          <div>(Weighting = 1)</div>
        </div>
      ),
      align: 'center',
      dataIndex: 'presentationFigures',
    },
    {
      title: (
        <div>
          <div>Organisation & Structure</div>
          <div>(Weighting = 1)</div>
        </div>
      ),
      align: 'center',
      dataIndex: 'organisationStructure',
    },
    {
      title: (
        <div>
          <div>Literature Survey</div>
          <div>(Weighting = 2)</div>
        </div>
      ),
      align: 'center',
      dataIndex: 'literatureSurvey',
    },
    {
      title: (
        <div>
          <div>Technical Content & Quality of Analysis </div>
          <div>(Weighting = 2)</div>
        </div>
      ),
      align: 'center',
      dataIndex: 'analysis',
    },
  ];
  return (
    <TemplateComponent>
      <div className="title">Assessment Matrix for Final Reports of Individual Projects (worth 50%)</div>
      <Row className="item">
        <Col span={4} className="label">
          Student Name
        </Col>
        <Col span={4} className="value">
          xx
        </Col>
        <Col span={4} className="label">
          Student GUID:{' '}
        </Col>
        <Col span={4} className="value">
          xxxx
        </Col>
        <Col span={4} className="label">
          Examiner Name:{' '}
        </Col>
        <Col span={4} className="value">
          xxxx
        </Col>
      </Row>
      <Table bordered pagination={false} dataSource={tableData} columns={columns} />
      <Row className="item" style={{ marginTop: 20 }}>
        <Col className="label">Writing</Col>
      </Row>
      <Row className="item">
        <Col className="value">
          Government believes that the latter taxes and duties reflect the additional costs for the entire country. Nonetheless, Government requires increasing fund in order to
          invest in particular area where there aren’t any consumers upon which the essential taxes can be subjected to tax like: defense, law and order, overseas aid and also the
          expenditure required for operating the Government and Parliament.
        </Col>
      </Row>
      <Row className="item">
        <Col className="label">Organisation& Structure</Col>
      </Row>
      <Row className="item">
        <Col className="value">
          Government believes that the latter taxes and duties reflect the additional costs for the entire country. Nonetheless, Government requires increasing fund in order to
          invest in particular area where there aren’t any consumers upon which the essential taxes can be subjected to tax like: defense, law and order, overseas aid and also the
          expenditure required for operating the Government and Parliament.
        </Col>
      </Row>
      <Row className="item">
        <Col className="label">Literature Survey</Col>
      </Row>
      <Row className="item">
        <Col className="value">
          Government believes that the latter taxes and duties reflect the additional costs for the entire country. Nonetheless, Government requires increasing fund in order to
          invest in particular area where there aren’t any consumers upon which the essential taxes can be subjected to tax like: defense, law and order, overseas aid and also the
          expenditure required for operating the Government and Parliament.
        </Col>
      </Row>
      <Row className="item">
        <Col className="label">Technical Content & Quality of Analysis</Col>
      </Row>
      <Row className="item">
        <Col className="value">
          Government believes that the latter taxes and duties reflect the additional costs for the entire country. Nonetheless, Government requires increasing fund in order to
          invest in particular area where there aren’t any consumers upon which the essential taxes can be subjected to tax like: defense, law and order, overseas aid and also the
          expenditure required for operating the Government and Parliament.
        </Col>
      </Row>
    </TemplateComponent>
  );
};
const tableData = [
  {
    gradeRange: 'A1 (97-100), A2 (94-96), A3 (91-93), A4 (88-90), A5 (85-87)',
    descriptor: 'Excellent',
    Writing: 'Exceptionally clear, precise and concise English. Excellent spelling&grammar, few typos.',
    presentationFigures: 'Professional standard of presentation. All illustrations are well formatted and presented.',
    organisationStructure: 'Structure is entirely correct with all sections correctly placed. Reading contents gives clear overview.',
    literatureSurvey: 'Exemplary range of references used and discussed in great depth, indicating comprehensive background reading.',
    analysis: 'Well informed and authoritative discussion and a comprehensive analysis of a significantly complex technical problem.',
  },
  {
    gradeRange: 'B1 (81-84), B2 (78-80), B3 (75-77)',
    descriptor: 'Very Good',
    Writing: 'Clear and well written, easy to understand, and mostly free of errors.',
    presentationFigures: 'A clear and consistent presentation style making it easy to read. Most of the figures are clear and well presented.',
    organisationStructure: 'A well organised report with all sections logically placed enhancing understanding of work.',
    literatureSurvey: 'An appropriate range of relevant references used and discussed suggesting substantial background reading.',
    analysis: 'Clear and reasoned arguments backed up with a significant analysis indicating a very good grasp of a difficult technical problem.',
  },
  {
    gradeRange: 'C1 (71-74), C2 (68-70), C3 (65-67)',
    descriptor: 'Good',
    Writing: 'Most of the text is clear and easily understood. There are some issues with grammar and spelling.',
    presentationFigures: 'There are some minor flaws in the presentation and the clarity of the figures, but overall a well presented report.',
    organisationStructure: 'A report which is sufficiently well organised to make reading the report easy.',
    literatureSurvey: 'Sufficient references used and discussed to indicate a good level of background reading.',
    analysis: 'Arguments presented are of a reasonable technical level, supported by a good quality analysis, and have been well considered and clearly stated.',
  },
  {
    gradeRange: 'D1 (63-64), D2 (61-62), D3 (60)',
    descriptor: 'Satisfactory',
    Writing: 'The text can be understood, but some elements are not entirely clear. A sizeable volume of errors is noticeable.',
    presentationFigures: 'A number of basic errors present – inconsistent use of styles, margins etc. Figures are satisfactory.',
    organisationStructure: 'There may be some issues with the structure, but these do not detract from overall quality.',
    literatureSurvey: 'Perhaps just enough references used and discussed to suggest some background reading was undertaken. Too many “www” references.',
    analysis: 'The arguments presented are of reasonable technical depth, supported by some analysis and show a satisfactory understanding.',
  },
  {
    gradeRange: 'E1 (55-59), E2 (50-54), E3 (45-49)',
    descriptor: 'Weak',
    Writing: 'Hard to understand much of the text. Significant spelling errors and grammatical flaws.',
    presentationFigures: 'Significant flaws in the presentation detracting from the overall impression of the report. Flawed figures, e.g. badly drawn and untidy,',
    organisationStructure: 'There are flaws in the way the report is structured which damages the overall quality of the report.',
    literatureSurvey: 'Too few relevant references used and discussed and possibly an over reliance on www sources indicating insufficient background work.',
    analysis: 'Only limited critical discussion of the technical problem studied. Little analysis or a low level of analysis. Suggests limited understanding of problem.',
  },
  {
    gradeRange: 'F1 (40-44), F2 (35-39), F3 (30-34)',
    descriptor: 'Poor',
    Writing: 'The volume and nature of the grammatical errors, combined with poor writing makes this report difficult to read.',
    presentationFigures: 'Unacceptable presentation: untidy and inconsistent use of styles. Figures are messy and unclear.',
    organisationStructure: 'Serious flaws in structure which makes it difficult to read and understand the report.',
    literatureSurvey: 'Only a few references used and discussed and majority are irrelevant. Little evidence of background reading.',
    analysis: 'Very little evidence of critical discussion of technical work or results. Superficial understanding of problem. Minimal analysis included.',
  },
  {
    gradeRange: 'G1 (22-29), G2 (15-21), H (0-14)',
    descriptor: 'G: Very PoorH: No Attainment',
    Writing: 'Unintelligible. Impossible to read due to exceptionally poor use of English.',
    presentationFigures: 'A messy report, e.g. no evidence of any effective effort on the quality of the presentation. Report is hard to follow due to unclear figures.',
    organisationStructure: 'No discernable structure. Illogical placement of sections. Impossible to follow argument.',
    literatureSurvey: 'Very few (or no) references used or discussed.  No evidence of any background reading.',
    analysis: 'The lack of quality of the technical argument suggests that the student has very little understanding of the problem. No analysis.',
  },
  {
    gradeRange: 'Grade Awarded',
    Writing: 2,
    presentationFigures: 1,
    organisationStructure: 3,
    literatureSurvey: 4,
    analysis: 5,
  },
];
export default TeacherGradeInfo;
