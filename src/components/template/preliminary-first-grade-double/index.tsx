/**
 * 双学位一导初期报告评分表
 */
import React from 'react';
import { Row, Col } from 'antd';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import TemplateComponent from '../template-component';

const PreliminaryFirstGradeDouble = () => {
  const labelSpan = 4;
  const valueSpan = 4;
  const { t } = useTranslation();
  const { schoolTitle, title, student, appraisal, detail, project, unit } = translations;
  return (
    <TemplateComponent>
      <div className="title">Assessment sheet for Independent Project</div>
      <Row className="item">
        <Col className="label" span={4}>
          Student Name
        </Col>
        <Col className="value" span={4}>
          wangmian
        </Col>
        <Col className="label" span={4}>
          Student GUID
        </Col>
        <Col className="value" span={4}>
          02033943
        </Col>
        <Col className="label" span={4}>
          Examiner Name
        </Col>
        <Col className="value" span={4}>
          Line
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={12}>
          Questions
        </Col>
        <Col className="label" span={3}>
          YES
        </Col>
        <Col className="label" span={3}>
          NO
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={12}>
          Has the student attained the considerable understanding of the project technical background?
        </Col>
        <Col className="value" span={3}>
          Y
        </Col>
        <Col className="value" span={3}>
          N
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={12}>
          Is the student aware of the technical details related to the targeted problem?
        </Col>
        <Col className="value" span={3}>
          Y
        </Col>
        <Col className="value" span={3}>
          N
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={12}>
          Has the student done sufficient literature review?
        </Col>
        <Col className="value" span={3}>
          Y
        </Col>
        <Col className="value" span={3}>
          N
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={12}>
          Has the student taken the initiative towards solving the problem?
        </Col>
        <Col className="value" span={3}>
          Y
        </Col>
        <Col className="value" span={3}>
          N
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={12}>
          Is the revised proposed work plan by the student feasible?
        </Col>
        <Col className="value" span={3}>
          Y
        </Col>
        <Col className="value" span={3}>
          N
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={12}>
          Grade
        </Col>
        <Col className="label">5</Col>
      </Row>
      <Row className="item">
        <Col span={18}>
          {
            // @ts-ignore
            <table border="1">
              <tr>
                <td>Score</td>
                <td>0</td>
                <td>1</td>
                <td>2</td>
                <td>3</td>
                <td>4</td>
                <td>5</td>
              </tr>
              <tr>
                <td>Grade</td>
                <td>H</td>
                <td>F3</td>
                <td>D3</td>
                <td>B3</td>
                <td>A2</td>
                <td>A1</td>
              </tr>
            </table>
          }
        </Col>
      </Row>
      <Row className="item">
        <Col className="label">COMMENTS</Col>
      </Row>
      <Row className="item">
        <Col className="value">
          Government believes that the latter taxes and duties reflect the additional costs for the entire country. Nonetheless, Government requires increasing fund in order to
          invest in particular area where there aren’t any consumers upon which the essential taxes can be subjected to tax like: defense, law and order, overseas aid and also the
          expenditure required for operating the Government and Parliament.
        </Col>
      </Row>
    </TemplateComponent>
  );
};

export default PreliminaryFirstGradeDouble;
