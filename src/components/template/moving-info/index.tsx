/**
 * 流转表格
 */
import { translations } from '@locales/i18n';
import { Table } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import Style from './index.module.less';
const MovingInfo = () => {
  const { t } = useTranslation();
  const { project, table } = translations;
  const columns = [
    {
      title: t(table.FlowNode),
      dataIndex: 'flowNode',
    },
    {
      title: t(table.Operator),
      dataIndex: 'flowNode',
    },
    {
      title: t(table.StartTime),
      dataIndex: 'flowNode',
    },
    {
      title: t(table.EndTime),
      dataIndex: 'flowNode',
    },
    {
      title: t(table.Suggestion),
      dataIndex: 'flowNode',
    },
    {
      title: t(table.TaskLast),
      dataIndex: 'flowNode',
    },
  ];
  return (
    <div className={Style.movingInfo}>
      <div className={Style.title}>{t(project.MovingInfo)}</div>
      <Table columns={columns} pagination={false} />
    </div>
  );
};

export default MovingInfo;
