/**
 * 单学位一导初期报告评分表
 */
import React from 'react';
import { Row, Col, Form, Radio, Select, Button } from 'antd';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import TemplateComponent from '../template-component';
import RichEditor from '@components/rich-editor';
import FooterButtons from '@components/button';

const PreliminaryGradeSingleEdit = () => {
  const { t } = useTranslation();
  const {
    schoolTitle,
    title,
    student,
    appraisal,
    detail,
    project,
    buttons: { Submit },
  } = translations;
  return (
    <>
      <TemplateComponent>
        <div className="title">{t(schoolTitle)}</div>
        <div className="subTitle">2021{t(title.PGradeTitle)}</div>
        <Row className="item">
          <Col className="label" span={3}>
            {t(student.name)}
          </Col>
          <Col className="value" span={4}>
            王冕
          </Col>
          <Col className="label" span={3}>
            {t(student.UESTICID)}
          </Col>
          <Col className="value" span={4}>
            4343434
          </Col>
          <Col className="label" span={3}>
            {t(detail.Supervisor)}
          </Col>
          <Col className="value" span={4}>
            林和
          </Col>
        </Row>
        <Row className="item">
          <Col className="label" span={16}>
            {t(appraisal.Standards)}
          </Col>
          <Col className="label" span={3}>
            {t(appraisal.Yes)}
          </Col>
          <Col className="label" span={3}>
            {t(appraisal.No)}
          </Col>
        </Row>
        <Row className="item">
          <Col className="label" span={16}>
            {t(appraisal.DescriptProject)}
          </Col>
          <Col className="label" span={8}>
            <Form.Item wrapperCol={{ span: 17 }}>
              <Radio.Group buttonStyle="solid" style={{ width: '100%' }}>
                <Radio.Button value="1" style={{ width: '40%', textAlign: 'center' }}>
                  {t(appraisal.Yes)}
                </Radio.Button>
                <Radio.Button value="0" style={{ width: '40%', textAlign: 'center', marginLeft: '10%' }}>
                  {t(appraisal.No)}
                </Radio.Button>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>
        <Row className="item">
          <Col className="label" span={16}>
            {t(appraisal.ProjectTaskAndTarget)}
          </Col>
          <Col className="label" span={8}>
            <Form.Item wrapperCol={{ span: 17 }}>
              <Radio.Group buttonStyle="solid" style={{ width: '100%' }}>
                <Radio.Button value="1" style={{ width: '40%', textAlign: 'center' }}>
                  {t(appraisal.Yes)}
                </Radio.Button>
                <Radio.Button value="0" style={{ width: '40%', textAlign: 'center', marginLeft: '10%' }}>
                  {t(appraisal.No)}
                </Radio.Button>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>
        <Row className="item">
          <Col className="label" span={16}>
            {t(appraisal.ProjectTaskDetail)}
          </Col>
          <Col className="label" span={8}>
            <Form.Item wrapperCol={{ span: 17 }}>
              <Radio.Group buttonStyle="solid" style={{ width: '100%' }}>
                <Radio.Button value="1" style={{ width: '40%', textAlign: 'center' }}>
                  {t(appraisal.Yes)}
                </Radio.Button>
                <Radio.Button value="0" style={{ width: '40%', textAlign: 'center', marginLeft: '10%' }}>
                  {t(appraisal.No)}
                </Radio.Button>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>
        <Row className="item">
          <Col className="label" span={16}>
            {t(appraisal.TechDescription)}
          </Col>
          <Col className="label" span={8}>
            <Form.Item wrapperCol={{ span: 17 }}>
              <Radio.Group buttonStyle="solid" style={{ width: '100%' }}>
                <Radio.Button value="1" style={{ width: '40%', textAlign: 'center' }}>
                  {t(appraisal.Yes)}
                </Radio.Button>
                <Radio.Button value="0" style={{ width: '40%', textAlign: 'center', marginLeft: '10%' }}>
                  {t(appraisal.No)}
                </Radio.Button>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>
        <Row className="item" style={{ paddingBottom: 25, borderBottom: '1px solid rgba(0,0,0,0.1)' }}>
          <Col className="label" span={16}>
            {t(appraisal.WorkDetail)}
          </Col>
          <Col className="label" span={8}>
            <Form.Item wrapperCol={{ span: 17 }}>
              <Radio.Group buttonStyle="solid" style={{ width: '100%' }}>
                <Radio.Button value="1" style={{ width: '40%', textAlign: 'center' }}>
                  {t(appraisal.Yes)}
                </Radio.Button>
                <Radio.Button value="0" style={{ width: '40%', textAlign: 'center', marginLeft: '10%' }}>
                  {t(appraisal.No)}
                </Radio.Button>
              </Radio.Group>
            </Form.Item>
          </Col>
        </Row>
        <Row className="item">
          <Col className="label" span={16}>
            {t(appraisal.Total)}
          </Col>
          <Form.Item style={{ marginBottom: 0, width: 150 }}>
            <Select
              options={[
                { label: 1, value: 1 },
                { label: 2, value: 2 },
                { label: 3, value: 3 },
                { label: 4, value: 4 },
                { label: 5, value: 5 },
              ]}
            />
          </Form.Item>
        </Row>
        <Row className="item">
          <Col className="label">{t(project.SupervisorSuggestion)}</Col>
        </Row>
        <Row className="item">
          <Col span={22}>
            <Form.Item>
              <RichEditor height="200px" />
            </Form.Item>
          </Col>
        </Row>
      </TemplateComponent>
      <FooterButtons
        btns={[
          <Button key={'time'} size={'large'} type="primary">
            {t(Submit)}
          </Button>,
        ]}
      />
    </>
  );
};

export default PreliminaryGradeSingleEdit;
