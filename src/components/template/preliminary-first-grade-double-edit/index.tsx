/**
 * 双学位一导初期报告评分表
 */
import React from 'react';
import { Row, Col, Form, Radio, Button } from 'antd';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import TemplateComponent from '../template-component';
import RichEditor from '@components/rich-editor';
import FooterButtons from '@components/button';

const PreliminaryGradeDoubleEdit = () => {
  const labelSpan = 4;
  const valueSpan = 4;
  const { t } = useTranslation();
  const { schoolTitle, title, student, appraisal, detail, project, unit } = translations;
  return (
    <>
      <TemplateComponent>
        <div className="title">Assessment sheet for Independent Project</div>
        <Row className="item">
          <Col className="label" span={4}>
            Student Name
          </Col>
          <Col className="value" span={4}>
            wangmian
          </Col>
          <Col className="label" span={4}>
            Student GUID
          </Col>
          <Col className="value" span={4}>
            02033943
          </Col>
          <Col className="label" span={4}>
            Examiner Name
          </Col>
          <Col className="value" span={4}>
            Line
          </Col>
        </Row>
        <Form>
          <Row className="item">
            <Col className="label" span={16}>
              Questions
            </Col>
            <Col className="label" span={3}>
              YES
            </Col>
            <Col className="label" span={3}>
              NO
            </Col>
          </Row>
          <Row className="item">
            <Col className="label" span={16}>
              Has the student attained the considerable understanding of the project technical background?
            </Col>
            <Col className="label" span={8}>
              <Form.Item wrapperCol={{ span: 17 }}>
                <Radio.Group buttonStyle="solid" style={{ width: '100%' }}>
                  <Radio.Button value="1" style={{ width: '40%', textAlign: 'center' }}>
                    Y
                  </Radio.Button>
                  <Radio.Button value="0" style={{ width: '40%', textAlign: 'center', marginLeft: '10%' }}>
                    N
                  </Radio.Button>
                </Radio.Group>
              </Form.Item>
            </Col>
          </Row>
          <Row className="item">
            <Col className="label" span={16}>
              Is the student aware of the technical details related to the targeted problem?
            </Col>
            <Col className="label" span={8}>
              <Form.Item wrapperCol={{ span: 17 }}>
                <Radio.Group buttonStyle="solid" style={{ width: '100%' }}>
                  <Radio.Button value="1" style={{ width: '40%', textAlign: 'center' }}>
                    Y
                  </Radio.Button>
                  <Radio.Button value="0" style={{ width: '40%', textAlign: 'center', marginLeft: '10%' }}>
                    N
                  </Radio.Button>
                </Radio.Group>
              </Form.Item>
            </Col>
          </Row>
          <Row className="item">
            <Col className="label" span={16}>
              Has the student done sufficient literature review?
            </Col>
            <Col className="label" span={8}>
              <Form.Item wrapperCol={{ span: 17 }}>
                <Radio.Group buttonStyle="solid" style={{ width: '100%' }}>
                  <Radio.Button value="1" style={{ width: '40%', textAlign: 'center' }}>
                    Y
                  </Radio.Button>
                  <Radio.Button value="0" style={{ width: '40%', textAlign: 'center', marginLeft: '10%' }}>
                    N
                  </Radio.Button>
                </Radio.Group>
              </Form.Item>
            </Col>
          </Row>
          <Row className="item">
            <Col className="label" span={16}>
              Has the student taken the initiative towards solving the problem?
            </Col>
            <Col className="label" span={8}>
              <Form.Item wrapperCol={{ span: 17 }}>
                <Radio.Group buttonStyle="solid" style={{ width: '100%' }}>
                  <Radio.Button value="1" style={{ width: '40%', textAlign: 'center' }}>
                    Y
                  </Radio.Button>
                  <Radio.Button value="0" style={{ width: '40%', textAlign: 'center', marginLeft: '10%' }}>
                    N
                  </Radio.Button>
                </Radio.Group>
              </Form.Item>
            </Col>
          </Row>
          <Row className="item">
            <Col className="label" span={16}>
              Is the revised proposed work plan by the student feasible?
            </Col>
            <Col className="label" span={8}>
              <Form.Item wrapperCol={{ span: 17 }}>
                <Radio.Group buttonStyle="solid" style={{ width: '100%' }}>
                  <Radio.Button value="1" style={{ width: '40%', textAlign: 'center' }}>
                    Y
                  </Radio.Button>
                  <Radio.Button value="0" style={{ width: '40%', textAlign: 'center', marginLeft: '10%' }}>
                    N
                  </Radio.Button>
                </Radio.Group>
              </Form.Item>
            </Col>
          </Row>
          <Row className="item">
            <Col className="label" span={4}>
              Grade
            </Col>
            <Col className="label" span={20}>
              <Form.Item wrapperCol={{ span: 24 }} style={{ marginBottom: 0 }}>
                <Radio.Group buttonStyle="solid" style={{ width: '100%' }}>
                  <Radio.Button value="0" style={{ width: '12%', textAlign: 'center' }}>
                    H
                  </Radio.Button>
                  <Radio.Button value="1" style={{ width: '12%', textAlign: 'center', marginLeft: '4%' }}>
                    F3
                  </Radio.Button>
                  <Radio.Button value="2" style={{ width: '12%', textAlign: 'center', marginLeft: '4%' }}>
                    D3
                  </Radio.Button>
                  <Radio.Button value="3" style={{ width: '12%', textAlign: 'center', marginLeft: '4%' }}>
                    B3
                  </Radio.Button>
                  <Radio.Button value="4" style={{ width: '12%', textAlign: 'center', marginLeft: '4%' }}>
                    A2
                  </Radio.Button>
                  <Radio.Button value="5" style={{ width: '12%', textAlign: 'center', marginLeft: '4%' }}>
                    A1
                  </Radio.Button>
                </Radio.Group>
              </Form.Item>
            </Col>
          </Row>
          <Row className="item">
            <Col span={18}>
              {
                // @ts-ignore
                <table border="1">
                  <tr>
                    <td>Score</td>
                    <td>0</td>
                    <td>1</td>
                    <td>2</td>
                    <td>3</td>
                    <td>4</td>
                    <td>5</td>
                  </tr>
                  <tr>
                    <td>Grade</td>
                    <td>H</td>
                    <td>F3</td>
                    <td>D3</td>
                    <td>B3</td>
                    <td>A2</td>
                    <td>A1</td>
                  </tr>
                </table>
              }
            </Col>
          </Row>
          <Row className="item">
            <Col className="label">COMMENTS</Col>
          </Row>
          <Row className="item">
            <Col span={24}>
              <Form.Item>
                <RichEditor height="200px" />
              </Form.Item>
            </Col>
          </Row>
        </Form>
      </TemplateComponent>
      <FooterButtons
        btns={[
          <Button key={'time'} size={'large'} type="primary">
            Submit
          </Button>,
        ]}
      />
    </>
  );
};

export default PreliminaryGradeDoubleEdit;
