/**
 * 中期检查表(校、院级)（单双学位公用）
 */
import React from 'react';
import { Row, Col } from 'antd';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import TemplateComponent from '../template-component';

interface IProps {
  type: 'd' | 's';
  tableId: string;
}
const InterimSchoolTable = (props: IProps) => {
  const labelSpan = 4;
  const valueSpan = 8;
  const { type, tableId } = props;
  const { t } = useTranslation();
  const { schoolTitle, student, commonInfo, detail, project, title } = translations;
  return (
    <TemplateComponent>
      <div className="title">{t(schoolTitle)}</div>
      <div className="subTitle">2021{t(type === 'd' ? title.IRTitleD : title.IRTitleS)}</div>
      <Row className="item">
        <Col className="label" span={4}>
          {t(detail.Academy)}
        </Col>
        <Col className="value" span={8}>
          王冕
        </Col>
        <Col className="label" span={4}>
          {t(detail.FillDate)}
        </Col>
        <Col className="value" span={8}>
          格拉斯学院
        </Col>
      </Row>
      <div className="blank">{t(detail.StuFill)}</div>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(student.name)}
        </Col>
        <Col className="value" span={valueSpan}>
          xx
        </Col>
        <Col className="label" span={labelSpan}>
          {t(student.UESTICID)}
        </Col>
        <Col className="value" span={valueSpan}>
          333223
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.ProjectFrom)}
        </Col>
        <Col className="value" span={valueSpan}>
          生产
        </Col>
        <Col className="label" span={labelSpan}>
          {t(detail.Supervisor)}
        </Col>
        <Col className="value" span={valueSpan}>
          xxx
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(project.ProjectTitle)}
        </Col>
        <Col className="value">理论研究</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.Position)}
        </Col>
        <Col className="value">xxxx</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.TimeRange)}
        </Col>
        <Col className="value">2020年7月12日 至 2021年7月23日</Col>
      </Row>
      <div className="blank">{t(detail.TeacherFill)}</div>

      <Row className="item">
        <Col className="label">{t(detail.ProjectCore)}</Col>
      </Row>
      <Row className="item">
        <Col className="value">
          完善的监管制度是有效监管的前提。信托业的监管应坚持以政府监管为主导，社会监管以及行业自律为辅助的一套完善监管体系。可以采取以下方式:完善信托立法，加大法律法规的监管力度;对信托产品的流通运行建立完善的流程和机制，促进集中交易市场的建立;发挥行业协会的监督作用，同时建立信用评级制度，加强行业自律;积极推进分类监管评级方案的实施，建立与国际接轨的评级体系完善的监管制度是有效监管的前提。信托业的监管应坚持
        </Col>
      </Row>
      <Row className="item">
        <Col className="label">{t(detail.ProjectProgress)}</Col>
      </Row>
      <Row className="item">
        <Col className="value">
          完善的监管制度是有效监管的前提。信托业的监管应坚持以政府监管为主导，社会监管以及行业自律为辅助的一套完善监管体系。可以采取以下方式:完善信托立法，加大法律法规的监管力度;对信托产品的流通运行建立完善的流程和机制，促进集中交易市场的建立;发挥行业协会的监督作用，同时建立信用评级制度，加强行业自律;积极推进分类监管评级方案的实施，建立与国际接轨的评级体系完善的监管制度是有效监管的前提。信托业的监管应坚持
        </Col>
      </Row>
      <Row className="item">
        <Col className="label">{t(detail.Diffcult)}</Col>
      </Row>
      <Row className="item">
        <Col className="value">
          完善的监管制度是有效监管的前提。信托业的监管应坚持以政府监管为主导，社会监管以及行业自律为辅助的一套完善监管体系。可以采取以下方式:完善信托立法，加大法律法规的监管力度;对信托产品的流通运行建立完善的流程和机制，促进集中交易市场的建立;发挥行业协会的监督作用，同时建立信用评级制度，加强行业自律;积极推进分类监管评级方案的实施，建立与国际接轨的评级体系完善的监管制度是有效监管的前提。信托业的监管应坚持
        </Col>
      </Row>
      <Row className="item">
        <Col className="label">{t(detail.SolutionSuggestion)}</Col>
      </Row>
      <Row className="item">
        <Col className="value">
          完善的监管制度是有效监管的前提。信托业的监管应坚持以政府监管为主导，社会监管以及行业自律为辅助的一套完善监管体系。可以采取以下方式:完善信托立法，加大法律法规的监管力度;对信托产品的流通运行建立完善的流程和机制，促进集中交易市场的建立;发挥行业协会的监督作用，同时建立信用评级制度，加强行业自律;积极推进分类监管评级方案的实施，建立与国际接轨的评级体系完善的监管制度是有效监管的前提。信托业的监管应坚持
        </Col>
      </Row>

      <Row className="item">
        <Col className="label">{t(detail.CurrentGrade)}</Col>
      </Row>
      <Row className="item">
        <Col className="value">优秀</Col>
      </Row>
    </TemplateComponent>
  );
};

export default InterimSchoolTable;
