/**
 * 审核表单
 */
import React, { useImperativeHandle } from 'react';
import { Form, Input, Radio } from 'antd';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import { useForm } from 'antd/lib/form/Form';
import Style from './index.module.less';
const AuditForm = React.forwardRef((props, ref) => {
  const { t } = useTranslation();
  const { enumT, audit } = translations;
  const [form] = useForm();
  const getFormData = () => {
    return form.getFieldsValue();
  };
  useImperativeHandle(ref, () => ({ getFormData }), []);
  return (
    <div className={Style.auditForm}>
      <Form form={form}>
        <Form.Item label={t(audit.AuditResult)} name="result">
          <Radio.Group>
            <Radio value="1">{t(enumT.Pass)}</Radio>
            <Radio value="0">{t(enumT.Nopass)}</Radio>
          </Radio.Group>
        </Form.Item>
        <Form.Item label={t(audit.AuditAdvice)} name="advice">
          <Input.TextArea />
        </Form.Item>
      </Form>
    </div>
  );
});

export default AuditForm;
