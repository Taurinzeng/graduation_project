/**
 * 预分配申请表
 */
import React from 'react';
import { Row, Col } from 'antd';
import { translations } from '@locales/i18n';
import { useTranslation } from 'react-i18next';
import TemplateComponent from '../template-component';

const PreAllotApply = () => {
  const labelSpan = 4;
  const valueSpan = 4;
  const { preAllotTable } = translations;
  const { t } = useTranslation();
  return (
    <TemplateComponent>
      <div className="title">{t(preAllotTable.Title)}</div>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          申请人
          <br />
          (中文)
        </Col>
        <Col className="value" span={labelSpan}>
          xxx
        </Col>

        <Col className="label" span={labelSpan}>
          UESTIC ID
        </Col>
        <Col className="value" span={labelSpan}>
          格拉斯学院
        </Col>

        <Col className="label" span={labelSpan}>
          Applicant
          <br />
          （Pinyin）
        </Col>
        <Col className="value" span={labelSpan}>
          信息与计算科学
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          联系电话
          <br />
          Contact Number
        </Col>
        <Col className="value" span={labelSpan}>
          02033943
        </Col>

        <Col className="label" span={labelSpan}>
          电子邮件
          <br />
          Email
        </Col>
        <Col className="value" span={labelSpan}>
          xxx
        </Col>

        <Col className="label" span={labelSpan}>
          GUID
        </Col>
        <Col className="value" span={labelSpan}>
          2324
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          申请毕设题目
          <br />
          FYP Title
        </Col>
        <Col className="value">苹果产品在当前环境下的竞争优势分析</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          指导教师
          <br />
          Supervisor
        </Col>
        <Col className="value" span={labelSpan}>
          xxxx
        </Col>
        <Col className="label" span={5}>
          指导教师联系方式
          <br />
          Supervisor's Contact
        </Col>
        <Col className="value" span={labelSpan}>
          11111111111
        </Col>
      </Row>

      <Row className="item">
        <Col className="label">
          题目简介
          <br />
          Project Description
        </Col>
      </Row>
      <Row className="item">
        <Col className="value">
          监管的目的是控制风险，虽然监管当局逐步意识到风险监管对金融业发展的重要性，信托公司也在逐步控制信托资金的投放风险，但现行的政策主要是合规性监管，对风险监管缺乏主动性。监管部门应当制定一定的措施，能对风险进行实时监控，当风险达到一定水平时进行预警，把风险控制在合理范围内，减少危机的发生。对于风险性较高的金融行业，其发展首先要做到稳健，因此控制风险极其重要，我国的信托监管应尽快实现由传统的以合规性监管为主到风险监管的转变
        </Col>
      </Row>
      <Row className="item">
        <Col className="label">
          申请理由
          <br />
          Application Reasons
        </Col>
      </Row>
      <Row className="item">
        <Col className="value">
          监管的目的是控制风险，虽然监管当局逐步意识到风险监管对金融业发展的重要性，信托公司也在逐步控制信托资金的投放风险，但现行的政策主要是合规性监管，对风险监管缺乏主动性。监管部门应当制定一定的措施，能对风险进行实时监控，当风险达到一定水平时进行预警，把风险控制在合理范围内，减少危机的发生。对于风险性较高的金融行业，其发展首先要做到稳健，因此控制风险极其重要，我国的信托监管应尽快实现由传统的以合规性监管为主到风险监管的转变
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          申请人签字：
          <br />
          Signature of Applicant
        </Col>
        <Col className="value" span={6}>
          苹果产品在当
        </Col>
        <Col className="label" span={labelSpan}>
          指导教师签字：
          <br />
          Signature of Supervisor
        </Col>
        <Col className="value">苹果产品在当前环境下的竞争优势分析</Col>
      </Row>
      <Row className="item">
        <Col className="label">
          专家审核意见
          <br />
          Comments of Expert
        </Col>
      </Row>
      <Row className="item">
        <Col className="value">
          监管的目的是控制风险，虽然监管当局逐步意识到风险监管对金融业发展的重要性，信托公司也在逐步控制信托资金的投放风险，但现行的政策主要是合规性监管，对风险监管缺乏主动性。监管部门应当制定一定的措施，能对风险进行实时监控，当风险达到一定水平时进行预警，把风险控制在合理范围内，减少危机的发生。对于风险性较高的金融行业，其发展首先要做到稳健，因此控制风险极其重要，我国的信托监管应尽快实现由传统的以合规性监管为主到风险监管的转变
        </Col>
      </Row>
      <Row className={'item' + ' ' + 'right'}>
        <Col className="label">
          审核人： <br />
          Reviewer
        </Col>
      </Row>
      <Row className={'item' + ' ' + 'right'}>
        <Col className="label">
          日期： <br />
          Date
        </Col>
      </Row>
      <Row className="item">
        <Col className="label">
          学院审批意见
          <br />
          Comments of Glasgow College, UESTC
        </Col>
      </Row>
      <Row className="item">
        <Col className="value">
          监管的目的是控制风险，虽然监管当局逐步意识到风险监管对金融业发展的重要性，信托公司也在逐步控制信托资金的投放风险，但现行的政策主要是合规性监管，对风险监管缺乏主动性。监管部门应当制定一定的措施，能对风险进行实时监控，当风险达到一定水平时进行预警，把风险控制在合理范围内，减少危机的发生。对于风险性较高的金融行业，其发展首先要做到稳健，因此控制风险极其重要，我国的信托监管应尽快实现由传统的以合规性监管为主到风险监管的转变
        </Col>
      </Row>
      <Row className={'item' + ' ' + 'right'}>
        <Col className="label">
          审核人： <br />
          Reviewer
        </Col>
      </Row>
      <Row className={'item' + ' ' + 'right'}>
        <Col className="label">
          日期： <br />
          Date
        </Col>
      </Row>
    </TemplateComponent>
  );
};

export default PreAllotApply;
