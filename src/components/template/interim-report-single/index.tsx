/**
 * 单学位学生中期报告
 */
import React from 'react';
import { Row, Col } from 'antd';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import TemplateComponent from '../template-component';

const InterimReportSingle = () => {
  const labelSpan = 3;
  const valueSpan = 3;
  const { t } = useTranslation();
  const { schoolTitle, title, student, commonInfo, detail, project } = translations;
  return (
    <TemplateComponent>
      <div className="title">{t(schoolTitle)}</div>
      <div className="subTitle">2021{t(title.IRTitle)}</div>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(student.name)}
        </Col>
        <Col className="value" span={valueSpan}>
          王冕
        </Col>
        <Col className="label" span={2}>
          {t(student.UESTICID)}
        </Col>
        <Col className="value" span={valueSpan}>
          0215456
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.Supervisor)}
        </Col>
        <Col className="value" span={valueSpan}>
          王冕
        </Col>
        <Col className="label" span={2}>
          {t(detail.Position)}
        </Col>
        <Col className="value" span={valueSpan}>
          0215456
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.ProjectTitle)}
        </Col>
        <Col className="value">苹果产品在当前环境下的竞争优势分析</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.IntegrityStatement)}
        </Col>
        <Col className="value">苹果产品在当前环境下的竞争优势分析</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.ProjectProgress)}
        </Col>
        <Col className="value">
          完善的监管制度是有效监管的前提。信托业的监管应坚持以政府监管为主导，社会监管以及行业自律为辅助的一套完善监管体系。可以采取以下方式:完善信托立法，加大法律法规的监管力度;对信托产品的流通运行建立完善的流程和机制，促进集中交易市场的建立;发挥行业协会的监督作用，同时建立信用评级制度，加强行业自律;积极推进分类监管评级方案的实施，建立与国际接轨的评级体系完善的监管制度是有效监管的前提。信托业的监管应坚持以政府监管为主导，社会监管以及行业自律为辅助的一套完善监管体系。可以采取以下方式:完善信托立法，加大法律法规的监管力度;对信托产品的流通运行建立完善的流程和机制，促进集中交易市场的建立;发挥行业协会的监督作用，同时建立信用评级制度，加强行业自律;积极推进分类监管评级方案的实施，建立与国际接轨的评级体系完善的监管制度是有效监管的前提。信托业的监管应坚持以政府监管为主导，社会监管以及行业自律为辅助的一套完善监管体系。可以采取以下方式:完善信托立法，加大法律法规的监管力度;对信托产品的流通运行建立完善的流程和机制，促进集中交易市场的建立;发挥行业协会的监督作用，同时建立信用评级制度，加强行业自律;积极推进分类监管评级方案的实施，建立与国际接轨的评级体系
        </Col>
      </Row>
      <Row className="item">
        <Col className="label">{t(detail.StageSuccess)}</Col>
      </Row>
      <Row className="item">
        <Col className="value">
          完善的监管制度是有效监管的前提。信托业的监管应坚持以政府监管为主导，社会监管以及行业自律为辅助的一套完善监管体系。可以采取以下方式:完善信托立法，加大法律法规的监管力度;对信托产品的流通运行建立完善的流程和机制，促进集中交易市场的建立;发挥行业协会的监督作用，同时建立信用评级制度，加强行业自律;积极推进分类监管评级方案的实施，建立与国际接轨的评级体系完善的监管制度是有效监管的前提。信托业的监管应坚持以政府监管为主导，社会监管以及行业自律为辅助的一套完善监管体系。可以采取以下方式:完善信托立法，加大法律法规的监管力度;对信托产品的流通运行建立完善的流程和机制，促进集中交易市场的建立;发挥行业协会的监督作用，同时建立信用评级制度，加强行业自律;积极推进分类监管评级方案的实施，建立与国际接轨的评级体系完善的监管制度是有效监管的前提。信托业的监管应坚持以政府监管为主导，社会监管以及行业自律为辅助的一套完善监管体系。可以采取以下方式:完善信托立法，加大法律法规的监管力度;对信托产品的流通运行建立完善的流程和机制，促进集中交易市场的建立;发挥行业协会的监督作用，同时建立信用评级制度，加强行业自律;积极推进分类监管评级方案的实施，建立与国际接轨的评级体系
        </Col>
      </Row>
      <Row className="item">
        <Col className="label">{t(detail.Problems)}</Col>
      </Row>
      <Row className="item">
        <Col className="value">完善的非常好，优秀</Col>
      </Row>
      <Row className="item">
        <Col className="label">{t(detail.NextPlan)}</Col>
      </Row>
      <Row className="item">
        <Col className="value">完善的非常好，优秀</Col>
      </Row>
      <Row className="item">
        <Col className="label">{t(detail.CheckSuggestion)}</Col>
      </Row>
      <Row className="item">
        <Col className="value">完善的非常好，优秀</Col>
      </Row>
    </TemplateComponent>
  );
};

export default InterimReportSingle;
