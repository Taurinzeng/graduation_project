/**
 * 双学位二导初期报告评分表
 */
import React from 'react';
import { Row, Col } from 'antd';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import TemplateComponent from '../template-component';

const PreliminarySecondGradeDouble = () => {
  const labelSpan = 3;
  const valueSpan = 3;
  const { t } = useTranslation();
  const { schoolTitle, title, student, appraisal, detail, project, unit } = translations;
  return (
    <TemplateComponent>
      <div className="title">Feedback from Second Supervisors</div>
      <Row className="item">
        <Col className="label" span={8}>
          Student Name
        </Col>
        <Col className="value">wangmian</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={8}>
          Student Matriculation Number
        </Col>
        <Col className="value">0215545</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={8}>
          Name of Second Supervisor
        </Col>
        <Col className="value">Linhe</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={12}>
          Assessment criteria
        </Col>
        <Col className="label" span={3}>
          YES
        </Col>
        <Col className="label" span={3}>
          NO
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={12}>
          Was the report satisfactory?
        </Col>
        <Col className="value" span={3}>
          Y
        </Col>
        <Col className="value" span={3}>
          N
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={12}>
          Is project suitable for an BEng project?
        </Col>
        <Col className="value" span={3}>
          Y
        </Col>
        <Col className="value" span={3}>
          N
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={12}>
          Is the project plan feasible?
        </Col>
        <Col className="value" span={3}>
          Y
        </Col>
        <Col className="value" span={3}>
          N
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={12}>
          Would you like to give any suggestions/recommendations?
        </Col>
        <Col className="value" span={3}>
          Y
        </Col>
        <Col className="value" span={3}>
          N
        </Col>
      </Row>

      <Row className="item">
        <Col className="label">COMMENTS</Col>
      </Row>
      <Row className="item">
        <Col className="value">
          Government believes that the latter taxes and duties reflect the additional costs for the entire country. Nonetheless, Government requires increasing fund in order to
          invest in particular area where there aren’t any consumers upon which the essential taxes can be subjected to tax like: defense, law and order, overseas aid and also the
          expenditure required for operating the Government and Parliament.
        </Col>
      </Row>
    </TemplateComponent>
  );
};

export default PreliminarySecondGradeDouble;
