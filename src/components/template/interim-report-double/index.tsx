/**
 * 双学位学生中期报告
 */
import React from 'react';
import { Row, Col } from 'antd';
import TemplateComponent from '../template-component';

const InterimReportDouble = () => {
  const labelSpan = 12;
  const valueSpan = 12;
  return (
    <TemplateComponent>
      <div className="title">Project Specifications and Interim Report on UESTC4006P(BEng) Final Year Project</div>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          Student Name
        </Col>
        <Col className="value" span={valueSpan}>
          Wangmian
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          Student Matriculation Number
        </Col>
        <Col className="value">2018929282U</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          UESTC Student Number
        </Col>
        <Col className="value">UESTC4006P</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          Degree programme
        </Col>
        <Col className="value">Communications Engineering</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          Academic year
        </Col>
        <Col className="value">2020</Col>
      </Row>
      <Row></Row>
      <Row className="item">
        <Col className="label" span={valueSpan}>
          Placement Company(if appropriate)
        </Col>
        <Col className="value">Alipay Network Technology Co., Ltd.</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={valueSpan}>
          Working Title of Project
        </Col>
        <Col className="value">Smart Home Design</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={valueSpan}>
          Name of First Supervisor
        </Col>
        <Col className="value">Linhe</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={valueSpan}>
          Name of Second Supervisor
        </Col>
        <Col className="value">Dr.wang</Col>
      </Row>

      <Row className="item">
        <Col className="label" span={valueSpan}>
          Declaration of Originality and Submission Information
        </Col>
      </Row>
      <Row className="item">
        <Col className="value">
          I affirm that this submission is all my own work in accordance with the University of Glasgow Regulations and the School of Engineering requirements
        </Col>
      </Row>
      <Row className="item">
        <Col className="value">Signed </Col>
      </Row>
      <Row className="item">
        <Col className="notice">Note: Your report should be NO more than 6 pages in length and include the below subject headings and incorporated within this document </Col>
      </Row>
      <Row className="item">
        <Col className="label">Work done so far including thorough literature review</Col>
      </Row>
      <Row className="item">
        <Col className="content">
          Tax treaties are bilateral treaties under international tax law to prevent fiscal evasion and to eliminate double juridical taxation through negotiated sharing of tax
          revenues based on economic and revenue interests. As already mentioned earlier, rights of legislation and enforcement are bestowed on sovereign states. However, the
          international tax rules are contained in tax treaties. The main importance of a tax treaty is that it helps to restraint double taxation as far as possible and this can
          be achieved by setting the limits on scope of domestic law and by promoting the harmonization of the international tax rules through the adoption of income tax treaties
          that follow the same general pattern (Arnold & McIntyre, 1995). Generally tax treaty overrides domestic law and it governs disputes between states and not taxpayers.
          However, it has its limitations to the fact that
        </Col>
      </Row>
      <Row className="item">
        <Col className="label">Conclusions from initial work</Col>
      </Row>
      <Row className="item">
        <Col className="content">
          nternational law on treaties and it also contains its interpretation. It is binding on signatories and non-signatories as “part of the law of the land” and all courts
          must apply the VCLT to ensure compliance with international obligations. But the most important fact is that tax treaties are governed by VCLT and not domestic law.
        </Col>
      </Row>
      <Row className="item">
        <Col className="label">Work to be done</Col>
      </Row>
      <Row className="item">
        <Col className="content">Function and Purpose of Taxation in Modern Economy in UK</Col>
      </Row>
      <Row className="item">
        <Col className="label">Revised Gantt Chart</Col>
      </Row>
      <Row className="item">
        <Col className="content">
          international law on treaties and it also contains its interpretation. It is binding on signatories and non-signatories as “part of the law of the land” and all courts
          must apply the VCLT to ensure compliance with international obligations. But the most important fact is that tax treaties are governed by VCLT and not domestic law.
        </Col>
      </Row>
      <Row className="item">
        <Col className="label">Deadlines for submission of this report</Col>
      </Row>
      <Row className="item">
        <Col className="content">
          international law on treaties and it also contains its interpretation. It is binding on signatories and non-signatories as “part of the law of the land” and all courts
          must apply the VCLT to ensure compliance with international obligations. But the most important fact is that tax treaties are governed by VCLT and not domestic law.
        </Col>
      </Row>
    </TemplateComponent>
  );
};

export default InterimReportDouble;
