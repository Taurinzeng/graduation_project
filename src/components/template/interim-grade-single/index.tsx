/**
 * 单学位一导中期报告评分表
 */
import React from 'react';
import { Row, Col } from 'antd';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import TemplateComponent from '../template-component';

const InterimGradeSingle = () => {
  const labelSpan = 3;
  const valueSpan = 3;
  const { t } = useTranslation();
  const { schoolTitle, title, student, appraisal, detail, project, unit } = translations;
  return (
    <TemplateComponent>
      <div className="title">{t(schoolTitle)}</div>
      <div className="subTitle">2021{t(title.IGradeTitle)}</div>
      <Row className="item">
        <Col className="label" span={3}>
          {t(student.name)}
        </Col>
        <Col className="value" span={4}>
          王冕
        </Col>
        <Col className="label" span={3}>
          {t(student.UESTICID)}
        </Col>
        <Col className="value" span={4}>
          4343434
        </Col>
        <Col className="label" span={3}>
          {t(detail.Supervisor)}
        </Col>
        <Col className="value" span={4}>
          林和
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={12}>
          {t(appraisal.Standards)}
        </Col>
        <Col className="label" span={3}>
          {t(appraisal.Yes)}
        </Col>
        <Col className="label" span={3}>
          {t(appraisal.No)}
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={12}>
          {t(appraisal.UnderstandTech)}
        </Col>
        <Col className="value" span={3}>
          是
        </Col>
        <Col className="value" span={3}>
          否
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={12}>
          {t(appraisal.UnderstandTechDetail)}
        </Col>
        <Col className="value" span={3}>
          是
        </Col>
        <Col className="value" span={3}>
          否
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={12}>
          {t(appraisal.EnoughDescription)}
        </Col>
        <Col className="value" span={3}>
          是
        </Col>
        <Col className="value" span={3}>
          否
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={12}>
          {t(appraisal.AttitudeSolveProblem)}
        </Col>
        <Col className="value" span={3}>
          是
        </Col>
        <Col className="value" span={3}>
          否
        </Col>
      </Row>
      <Row className="item" style={{ paddingBottom: 25, borderBottom: '1px solid rgba(0,0,0,0.1)' }}>
        <Col className="label" span={12}>
          {t(appraisal.StickThePlan)}
        </Col>
        <Col className="value" span={3}>
          是
        </Col>
        <Col className="value" span={3}>
          否
        </Col>
      </Row>
      <Row className="item">
        <span>{t(appraisal.Total)}</span>
        <span>4{t(unit.point)}</span>
      </Row>
      <Row className="item">
        <Col className="label">{t(project.SupervisorSuggestion)}</Col>
      </Row>
      <Row className="item">
        <Col className="value">完善的非常好，优秀</Col>
      </Row>
    </TemplateComponent>
  );
};

export default InterimGradeSingle;
