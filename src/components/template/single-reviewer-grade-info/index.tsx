/**
 * 单学位学生开题报告
 */
import React from 'react';
import { Row, Col } from 'antd';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import TemplateComponent from '../template-component';

const SingleReviewerGradeInfo = () => {
  const labelSpan = 4;
  const valueSpan = 4;
  const { t } = useTranslation();
  const {
    schoolTitle,
    title,
    student,
    commonInfo,
    detail,
    project,
    unit: { point },
  } = translations;
  return (
    <TemplateComponent>
      <div className="title">{t(schoolTitle)}</div>
      <div className="subTitle">2021{t(title.ProjectGrade)}</div>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(student.name)}
        </Col>
        <Col className="value" span={valueSpan}>
          王冕
        </Col>
        <Col className="label" span={labelSpan}>
          {t(student.UESTICID)}
        </Col>
        <Col className="value" span={valueSpan}>
          02033943
        </Col>
        <Col className="label" span={2}>
          {t(commonInfo.DMajor)}
        </Col>
        <Col className="value" span={4}>
          信息与计算科学
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(project.ProjectTitle)}
        </Col>
        <Col className="value">苹果产品在当前环境下的竞争优势分析</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(detail.Supervisor)}
        </Col>
        <Col className="value" span={valueSpan}>
          王冕
        </Col>
        <Col className="label" span={2}>
          {t(commonInfo.DDepartment)}
        </Col>
        <Col className="value" span={valueSpan}>
          格拉斯学院
        </Col>
      </Row>
      <Row className="item">
        <Col className="label">{t(detail.GradeInfo1)}</Col>
      </Row>
      <Row className="item">
        <Col className="value">
          完善的监管制度是有效监管的前提。信托业的监管应坚持以政府监管为主导，社会监管以及行业自律为辅助的一套完善监管体系。可以采取以下方式:完善信托立法，加大法律法规的监管力度;对信托产品的流通运行建立完善的流程和机制，促进集中交易市场的建立;发挥行业协会的监督作用，同时建立信用评级制
        </Col>
      </Row>
      <Row className="item">
        <Col className="label">{t(detail.GradeInfo2)}</Col>
      </Row>
      <Row className="item">
        <Col className="value">
          完善的监管制度是有效监管的前提。信托业的监管应坚持以政府监管为主导，社会监管以及行业自律为辅助的一套完善监管体系。可以采取以下方式:完善信托立法，加大法律法规的监管力度;对信托产品的流通运行建立完善的流程和机制，促进集中交易市场的建立;发挥行业协会的监督作用，同时建立信用评级制
        </Col>
      </Row>
      <Row className="item">
        <Col className="label">{t(detail.GradeInfo3)}</Col>
      </Row>
      <Row className="item">
        <Col className="value">
          完善的监管制度是有效监管的前提。信托业的监管应坚持以政府监管为主导，社会监管以及行业自律为辅助的一套完善监管体系。可以采取以下方式:完善信托立法，加大法律法规的监管力度;对信托产品的流通运行建立完善的流程和机制，促进集中交易市场的建立;发挥行业协会的监督作用，同时建立信用评级制
        </Col>
      </Row>
      <Row className="item">
        <Col className="label">{t(detail.ProjectGrade)}</Col>
      </Row>
      <Row className="item">
        <Col className="value">
          {t(detail.Process)}: 45{t(point)}
        </Col>
      </Row>
    </TemplateComponent>
  );
};

export default SingleReviewerGradeInfo;
