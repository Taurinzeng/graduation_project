/**
 * 题目信息表
 */
import React from 'react';
import { Row, Col } from 'antd';
import Style from './index.module.less';
import TemplateComponent from '../template-component';

const ProjectInfo = () => {
  const labelSpan = 4;
  const valueSpan = 8;
  return (
    <TemplateComponent>
      <div className="title">Glasgow College UESTC Final Year Project</div>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          Project Title
        </Col>
        <Col className="value">苹果产品在当前环境下的竞争优势分析</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          Programme
        </Col>
        <Col className="value">苹果产品在当前环境下的竞争优势分析</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          Source of topic
        </Col>
        <Col className="value">苹果产品在当前环境下的竞争优势分析</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          Project Type
        </Col>
        <Col className="value">苹果产品在当前环境下的竞争优势分析</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          Subject Area
        </Col>
        <Col className="value">苹果产品在当前环境下的竞争优势分析</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          Supervisor
        </Col>
        <Col className="value" span={valueSpan}>
          林鹤
        </Col>
        <Col className="label" span={labelSpan}>
          Email
        </Col>
        <Col className="value" span={valueSpan}>
          苹果产品在当前环境下的竞争优势分析
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          University
        </Col>
        <Col className="value" span={valueSpan}>
          林鹤
        </Col>
        <Col className="label" span={labelSpan}>
          School
        </Col>
        <Col className="value" span={valueSpan}>
          苹果产品在当前环境下的竞争优势分析
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          Academic Title
        </Col>
        <Col className="value" span={valueSpan}>
          林鹤
        </Col>
        <Col className="label" span={labelSpan}>
          Office Location
        </Col>
        <Col className="value" span={valueSpan}>
          苹果产品在当前环境下的竞争优势分析
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={6}>
          Supervisor’s Webpage
        </Col>
        <Col className="value">苹果产品在当前环境下的竞争优势分析</Col>
      </Row>
      <Row className="item">
        <Col className="label">Project Description</Col>
      </Row>
      <Row className="item">
        <Col className="value">
          监管的目的是控制风险，虽然监管当局逐步意识到风险监管对金融业发展的重要性，信托公司也在逐步控制信托资金的投放风险，但现行的政策主要是合规性监管，对风险监管缺乏主动性。监管部门应当制定一定的措施，能对风险进行实时监控，当风险达到一定水平时进行预警，把风险控制在合理范围内，减少危机的发生。对于风险性较高的金融行业，其发展首先要做到稳健，因此控制风险极其重要，我国的信托监管应尽快实现由传统的以合规性监管为主到风险监管的转变
        </Col>
      </Row>
      <Row className="item">
        <Col className="label">Prerequisite Skills</Col>
      </Row>
      <Row className="item">
        <Col className="value">
          监管的目的是控制风险，虽然监管当局逐步意识到风险监管对金融业发展的重要性，信托公司也在逐步控制信托资金的投放风险，但现行的政策主要是合规性监管，对风险监管缺乏主动性。监管部门应当制定一定的措施，能对风险进行实时监控，当风险达到一定水平时进行预警，把风险控制在合理范围内，减少危机的发生。对于风险性较高的金融行业，其发展首先要做到稳健，因此控制风险极其重要，我国的信托监管应尽快实现由传统的以合规性监管为主到风险监管的转变
        </Col>
      </Row>
    </TemplateComponent>
  );
};

export default ProjectInfo;
