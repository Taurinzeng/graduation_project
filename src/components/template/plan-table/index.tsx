/**
 * 计划表
 */
import { translations } from '@locales/i18n';
import React from 'react';
import { useTranslation } from 'react-i18next';
import TemplateComponent from '../template-component';

const PlanTable = () => {
  const { t } = useTranslation();
  const { detail, student, schoolTitle, title } = translations;
  return (
    <TemplateComponent>
      <div className="title">{t(schoolTitle)}</div>
      <div className="subTitle">2021{t(title.PlanTitle)}</div>
      <div className="info">
        <span>{t(detail.Academy)}:理学院</span>
        <span>{t(detail.FillDate)}:2020年3月20日</span>
      </div>
      {
        //@ts-ignore
        <table border="1">
          <tr>
            <th>{t(student.name)}</th>
            <td colSpan={1}>xxxxxx</td>
            <th rowSpan={2}>{t(detail.ProjectTitle)}</th>
            <td rowSpan={2} colSpan={2}>
              xxxxxxxxxx
            </td>
          </tr>
          <tr>
            <th>{t(student.No)}</th>
            <td colSpan={1}>xxxxxxxxx</td>
          </tr>
          <tr>
            <th>{t(detail.WeekTime)}</th>
            <th>{t(detail.JobContent)}</th>
            <th>{t(detail.FinishStatus)}</th>
            <th>{t(detail.SuprvisorSign)}</th>
            <th>{t(detail.Notice)}</th>
          </tr>
        </table>
      }
    </TemplateComponent>
  );
};

export default PlanTable;
