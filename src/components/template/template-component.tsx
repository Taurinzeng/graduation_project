import React from 'react';
import './template.less';
const TemplateComponent = (props: any) => {
  return <section className="template-component">{props.children}</section>;
};

export default TemplateComponent;
