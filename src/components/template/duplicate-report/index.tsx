/**
 * 论文查重报告
 */
import React from 'react';
import { Row, Col } from 'antd';
import Style from './index.module.less';
import { translations } from '@locales/i18n';
import { useTranslation } from 'react-i18next';
import TemplateComponent from '../template-component';

const DuplicateReport = () => {
  const labelSpan = 4;
  const valueSpan = 4;
  const {
    schoolTitle,
    project,
    student,
    table: { Supervisor },
    title,
  } = translations;
  const { t } = useTranslation();
  return (
    <TemplateComponent>
      <div className="title">{t(schoolTitle)}</div>
      <div className="subTitle">2021{t(title.duplicateReport)}</div>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(student.name)}
        </Col>
        <Col className="value" span={labelSpan}>
          xxx
        </Col>

        <Col className="label" span={labelSpan}>
          {t(student.UESTICID)}
        </Col>
        <Col className="value" span={labelSpan}>
          格拉斯学院
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(Supervisor)}
        </Col>
        <Col className="value" span={labelSpan}>
          xxx
        </Col>

        <Col className="label" span={labelSpan}>
          {t(project.Position)}
        </Col>
        <Col className="value" span={labelSpan}>
          格拉斯学院
        </Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(project.ProjectTitle)}
        </Col>
        <Col className="value">xxx</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(project.Language)}
        </Col>
        <Col className="value">xxx</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(project.Attachment)}
        </Col>
        <Col className="value">xxx</Col>
      </Row>
      <Row className="item">
        <Col className="label" span={labelSpan}>
          {t(project.DuplicateCheckPic)}
        </Col>
      </Row>
      <Row className="item">
        <Col className="value" span={labelSpan}>
          xxx
        </Col>
      </Row>
    </TemplateComponent>
  );
};

export default DuplicateReport;
