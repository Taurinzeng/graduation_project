import React from 'react';
import './index.less';
interface IProps {
  title: string;
}
export default (props: IProps) => {
  const { title } = props;
  return (
    <div className="page-title-component">
      <span>{title}</span>
    </div>
  );
};
