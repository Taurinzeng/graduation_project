import { createFromIconfontCN } from '@ant-design/icons';
import React from 'react';
import './index.less';

export { IconfontWidget, IconfontWidgetOperate };

const IconfontWidget = createFromIconfontCN({
  scriptUrl: '//at.alicdn.com/t/font_2579953_x1wlrx3aks.js', // 在 iconfont.cn 上生成
});
const IconfontWidgetOperate = (props: any) => {
  const { type, style, className } = props;
  return <IconfontWidget type={type}
                         className={`icon-font ${className || ''}`}
                         style={style || {}}/>;
};