import { translations } from '@locales/i18n';
import { Input, Select, Modal, Tree, Button, Tag } from 'antd';
import { DataNode } from 'antd/lib/tree';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { RightOutlined, CloseCircleOutlined } from '@ant-design/icons';
import Style from './index.module.less';
import { queryPersonByRoleId, queryRoles } from '@service/content/message';
import { ResponseType } from '@modal/reducer/modal';

interface IProps {
  value?: any;
  onChange?: (...args: any[]) => void;
  disabled?: boolean;
}
const PersonSelect = (props: IProps) => {
  const { value, onChange, disabled } = props;
  const [visible, setVisible] = useState(false);
  const [treeData, setTreeData] = useState<any>([]);
  const [selectedKeys, setSelectedKeys] = useState<any>([]);
  const [leftSelectedData, setLeftSelectedData] = useState<any>();
  const [rightSelectedData, setRightSelectedData] = useState<any>([]);
  const { t } = useTranslation();
  const { placeholder } = translations;
  const handleTextClick = () => {
    if (!disabled) {
      setVisible(true);
    }
  };
  const handleCancel = () => {
    setVisible(false);
  };
  const handleOk = () => {
    onChange && onChange(rightSelectedData.length === 0 ? undefined : rightSelectedData.map((per: any) => ({ id: per.key, roleName: per.title })));
    handleCancel();
  };
  const handleSelect = (selectedKeys: any[], e: any) => {
    const { selectedNodes } = e;
    setLeftSelectedData(selectedNodes);
    setSelectedKeys(selectedKeys);
  };
  const handleMoveRight = () => {
    leftSelectedData.forEach((data: any) => {
      let existData = false;
      for (let i = 0; i < rightSelectedData.length; i++) {
        if (rightSelectedData[i].key === data.key) {
          existData = true;
          break;
        }
      }
      if (!existData) {
        rightSelectedData.push(data);
      }
    });
    setRightSelectedData([...rightSelectedData]);
  };
  const handleRightRemove = (index: number) => {
    const data = [...rightSelectedData];
    data.splice(index, 1);
    setRightSelectedData(data);
  };

  function updateTreeData(list: DataNode[], key: React.Key, children: DataNode[]): DataNode[] {
    return list.map((node) => {
      if (node.key === key) {
        return {
          ...node,
          children,
        };
      }
      if (node.children) {
        return {
          ...node,
          children: updateTreeData(node.children, key, children),
        };
      }
      return node;
    });
  }
  function onLoadData({ key, children }: any) {
    return new Promise<void>((resolve) => {
      if (children) {
        resolve();
        return;
      }
      queryPersonByRoleId({ id: key }).then((res: ResponseType) => {
        setTreeData((origin: any) =>
          updateTreeData(
            origin,
            key,
            res.data?.map((per: any) => ({ title: per.name, key: per.id, isLeaf: true })),
          ),
        );
        resolve();
      });
    });
  }
  const querySendRoles = () => {
    queryRoles().then((res: ResponseType) => {
      setTreeData(res.data?.map((per: any) => ({ title: per.roleName, key: per.id })));
    });
  };
  useEffect(() => {
    querySendRoles();
  }, []);
  useEffect(() => {
    if (visible) {
      setLeftSelectedData([]);
      setSelectedKeys([]);
    }
  }, [visible]);
  useEffect(() => {
    if (value) {
      if (Array.isArray(value)) {
        setRightSelectedData(value.map((per: any) => ({ title: per.roleName, key: per.id })));
      } else {
        throw new Error('value is not an array');
      }
    } else {
      setRightSelectedData([]);
    }
  }, [value]);
  return (
    <>
      <div className={Style.inputText} onClick={handleTextClick}>
        {value &&
          value.map((sourceValue: any, index: number) => (
            <Tag className={Style.tag} key={sourceValue.id} closable onClose={() => handleRightRemove(index)}>
              {sourceValue.roleName}
            </Tag>
          ))}
      </div>
      <Modal visible={visible} title={t(placeholder.SelectDepOrMember)} onCancel={handleCancel} onOk={handleOk} width={800} centered>
        <div className={Style.personSelect}>
          <div className={Style.left}>
            <Input.Search />
            <div className={Style.tree}>
              <Tree selectedKeys={selectedKeys} loadData={onLoadData} treeData={treeData} multiple onSelect={handleSelect} />
            </div>
          </div>
          <div className={Style.middle}>
            <Button icon={<RightOutlined />} onClick={handleMoveRight} disabled={!leftSelectedData || leftSelectedData.length === 0} />
          </div>
          <div className={Style.right}>
            <div className={Style.title}>
              <span>已选择 {rightSelectedData.length}</span>
              <a onClick={() => setRightSelectedData([])}>清空</a>
            </div>
            <div className={Style.dataContent}>
              {rightSelectedData &&
                rightSelectedData.map((tempData: any, index: number) => (
                  <div key={tempData.key} className={Style.dataUnit}>
                    <span>{tempData.title}</span>
                    <CloseCircleOutlined className={Style.deleteButton} onClick={() => handleRightRemove(index)} />
                  </div>
                ))}
            </div>
          </div>
        </div>
      </Modal>
    </>
  );
};

export default PersonSelect;
