import { Upload, UploadProps } from 'antd';
import React, { ReactElement } from 'react';

export interface FileUploadProps extends UploadProps{
  showFileList?: boolean;
  onChange?: (...args: any[]) => void;
  draggerable?: boolean;
  children?: ReactElement;
}
const FileUpload = (props: FileUploadProps) => {
  const {showFileList, draggerable, onChange} = props;
  const UploadComponent = draggerable ? Upload.Dragger : Upload;
  const uploadProps = {
    ...props,
    // action: '',
    showUploadList: !!showFileList,
    onChange: (info: any) => {
      console.log(info);
      const {file, fileList} = info;
      if (file.status === 'done') {
        onChange && onChange({fileName: file.name, url: file.response?.url})
      }
    }
  }
  return (
    <UploadComponent {...uploadProps}>
      {props.children}
    </UploadComponent>
  )
}

export default FileUpload;