import home from './home.json';
import work from './work.json';
import todo from './todo.json';
import guidance from './guidance.json';
import selectManagement from './select-management.json';
import taskAndPlan from './task-and-plan.json';
import paperAudit from './paper-audit.json';
import expense from './expense.json';
import procurement from './procurement.json';
import overview from './overview.json';
import log from './log.json';
import en from './en.json';
export default {
  home,
  work,
  todo,
  guidance,
  log,
  overview,
  selectManagement,
  taskAndPlan,
  paperAudit,
  expense,
  procurement,
  ...en,
};
