import app from './app.json';
import home from './home.json';
import work from './work.json';
import todo from './todo.json';
import guidance from './guidance.json';
import expertManagement from './expert-management.json';
import auditManagement from './audit-management.json';
import selectManagement from './select-management.json';
import taskAndPlan from './task-and-plan.json';
import paperAudit from './paper-audit.json';
import expense from './expense.json';
import procurement from './procurement.json';
import duplicateCheck from './duplicate-check.json';

import bannerManagement from './banner-management.json';
import linksManagement from './links-management.json';
import newsManagement from './news-management.json';
import profile from './profile.json';
import overview from './overview.json';
import log from './log.json';
import cn from './cn.json';
export default {
  app,
  home,
  work,
  todo,
  guidance,
  expertManagement,
  bannerManagement,
  linksManagement,
  newsManagement,
  log,
  overview,
  auditManagement,
  selectManagement,
  taskAndPlan,
  paperAudit,
  expense,
  procurement,
  duplicateCheck,
  ...cn,
};
