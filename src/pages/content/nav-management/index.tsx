import TableTemplate from '@components/table-template';
import { translations } from '@locales/i18n';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import OperateNav from './operate-nav';
import { PlusCircleFilled } from '@ant-design/icons';
import './index.less';
import { Button, Divider, message, Popconfirm } from 'antd';
import { deleteNav, queryNavList } from '@service/content/nav';

export default () => {
  const { t } = useTranslation();
  const { table, buttons } = translations;
  const [showModal, setShowModal] = useState(false);
  const [currentData, setCurrentData] = useState({});
  const [refresh, setRefresh] = useState<any>();
  const columns = [
    {
      title: t(table.ID),
      dataIndex: 'no',
      render: (text: any, record: any, index: number) => {
        return index + 1;
      },
    },
    {
      title: t(table.NavName),
      dataIndex: 'menuName',
    },
    {
      title: t(table.ParentType),
      dataIndex: 'parentName',
    },
    {
      title: t(table.Sort),
      dataIndex: 'forProfessional',
    },
    {
      title: t(table.CreateTime),
      dataIndex: 'createTime',
    },
    {
      title: t(table.NavCreator),
      dataIndex: 'createName',
    },
    {
      title: t(table.NavStatus),
      dataIndex: 'menuStatusName',
    },
    {
      title: t(table.Operation),
      dataIndex: 'id',
      render: (id: number, record: any) => {
        return (
          <>
            <a onClick={() => handleEdit(record)}>{t(buttons.Edit)}</a>
            <Divider type="vertical" />
            <Popconfirm title="确认删除该数据？" onConfirm={() => handleDelete(id)}>
              <a>{t(buttons.Delete)}</a>
            </Popconfirm>
          </>
        );
      },
    },
  ];
  const handleEdit = (record: any) => {
    setShowModal(true);
    setCurrentData(record);
  };
  const handleDelete = (id: number) => {
    deleteNav({ id }).then(() => {
      message.success('操作成功');
      changeRefresh(true);
    });
  };
  const changeRefresh = (isCurrent?: boolean) => {
    if (!refresh) {
      setRefresh({ count: 1, isCurrent });
    } else {
      setRefresh({ count: refresh.count, isCurrent });
    }
  };
  const handleOk = () => {
    setShowModal(false);
    setCurrentData({});
    changeRefresh();
  };
  const handleClose = () => {
    setShowModal(false);
    setCurrentData({});
  };

  return (
    <section className="nav-management-page">
      <TableTemplate
        columns={columns}
        queryAjax={queryNavList}
        rowKey={(record: any) => record.id}
        refresh={refresh}
        topButtons={[
          <Button type="primary" icon={<PlusCircleFilled />} onClick={() => setShowModal(true)}>
            {t(buttons.AddNav)}
          </Button>,
        ]}
      />
      <OperateNav visible={showModal} onClose={handleClose} onOk={handleOk} data={currentData} />
    </section>
  );
};
