import { Button, Checkbox, Form, Input, InputNumber, message, Modal, Select } from 'antd';
import React, { useEffect, useState } from 'react';
import { translations } from '@locales/i18n';
import { useTranslation } from 'react-i18next';
import { useForm } from 'antd/lib/form/Form';
import { addNav, editNav } from '@service/content/nav';

interface IProps {
  visible: boolean;
  onClose: () => void;
  onOk: () => void;
  data?: any;
}

export default (props: IProps) => {
  const { visible, onClose, onOk, data } = props;
  const [form] = useForm();
  const { t } = useTranslation();
  const { buttons, contentMList, placeholder } = translations;
  const handleSubmit = () => {
    form.validateFields().then((values: any) => {
      if (!values.menuName || !values.menuEnName) {
        message.error(t(contentMList.EnOrZnRequire));
        return;
      }
      const operateAjax = data.id ? editNav : addNav;
      operateAjax({ ...values, id: data.id }).then(() => {
        message.success('操作成功');
        onOk && onOk();
      });
    });
  };
  useEffect(() => {
    if (visible) {
      form.setFieldsValue({ ...data });
    } else {
      form.resetFields();
    }
  }, [data, visible]);
  return (
    <Modal width={600} visible={visible} title={t(buttons.AddNav)} cancelText={t(buttons.Cancel)} okText={t(buttons.Confirm)} onOk={handleSubmit} onCancel={onClose}>
      <Form size={'large'} colon={false} layout={'vertical'} form={form}>
        <Form.Item name="menuName" label={t(contentMList.NavNameCn)}>
          <Input />
        </Form.Item>

        <Form.Item name="menuEnName" label={t(contentMList.NavNameEn)}>
          <Input />
        </Form.Item>
        <Form.Item name="parentId" label={t(contentMList.ParentType)}>
          <Select options={[{ label: 'Item1', value: '1' }]} placeholder={t(placeholder.Choice)} />
        </Form.Item>
        <Form.Item name="sortWeight" label={t(contentMList.Weight)}>
          <InputNumber min={1} max={100} placeholder={t(placeholder.SortNumber)} style={{ width: '100%' }} />
        </Form.Item>
        <Form.Item name="toUrl" label={t(contentMList.linkUrl)}>
          <Input placeholder={t(placeholder.UrlOther)} />
        </Form.Item>
      </Form>
    </Modal>
  );
};
