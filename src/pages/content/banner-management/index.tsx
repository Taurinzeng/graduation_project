import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import React, { useState } from 'react';
import './index.less';
import Notice from '@components/notice';
import { useTranslation } from 'react-i18next';
import { deleteBanner, queryBannerList } from '@service/content/banner';
import { Button, Divider, message, Popconfirm } from 'antd';
import { PlusCircleFilled } from '@ant-design/icons';
import { ResponseType } from '@modal/reducer/modal';
import OperateBanner from './operate-banner';

export default () => {
  const { t } = useTranslation();
  const { notice, table, buttons } = translations;
  const [showModal, setShowModal] = useState(false);
  const [currentData, setCurrentData] = useState({});
  const [refresh, setRefresh] = useState<any>();
  const columns = [
    {
      title: t(table.ID),
      dataIndex: 'no',
      render: (text: any, record: any, index: number) => {
        return index + 1;
      },
    },
    {
      title: t(table.BannerTitle),
      dataIndex: 'title',
    },
    {
      title: t(table.BannerImg),
      dataIndex: 'imagePath',
    },
    {
      title: t(table.Url),
      dataIndex: 'redirectUrl',
    },
    {
      title: t(table.Sort),
      dataIndex: 'sortWeight',
    },
    {
      title: t(table.Status),
      dataIndex: 'status',
    },
    {
      title: t(table.UpdateTime),
      dataIndex: 'updateTime',
    },

    {
      title: t(table.Operation),
      dataIndex: 'id',
      render: (id: any, record: any) => {
        return (
          <>
            <a
              onClick={() => {
                handleEdit(record);
              }}>
              {t(buttons.Edit)}
            </a>
            <Divider type="vertical" />
            <Popconfirm title="确认删除该数据？" onConfirm={() => handleDelete(id)}>
              <a>{t(buttons.Delete)}</a>
            </Popconfirm>
          </>
        );
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(table.Status),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '启用',
          value: '1',
        },
        {
          text: '停用',
          value: '2',
        },
      ],
    },
    {
      label: t(table.CreateTime),
      code: 'teacherName',
      type: 'dateRange',
    },
  ];
  const handleEdit = (record: any) => {
    setShowModal(true);
    setCurrentData(record);
  };
  const handleDelete = (id: number) => {
    deleteBanner({ id }).then((res: ResponseType) => {
      message.success(res.msg);
      changeRefresh(true);
    });
  };
  const changeRefresh = (isCurrent?: boolean) => {
    if (!refresh) {
      setRefresh({ count: 1, isCurrent });
    } else {
      setRefresh({ count: refresh.count, isCurrent });
    }
  };
  const handleOk = () => {
    setShowModal(false);
    setCurrentData({});
    changeRefresh();
  };
  const handleClose = () => {
    setShowModal(false);
    setCurrentData({});
  };

  return (
    <section className="banner-management-page">
      <Notice>
        1、{t(notice.NoticeBanner1)}；<br />
        2、{t(notice.NoticeBanner2)}；
      </Notice>
      <TableTemplate
        columns={columns}
        queryAjax={queryBannerList}
        rowKey={(record: any) => record.id}
        refresh={refresh}
        filterFields={filterFields}
        topButtons={[
          <Button type="primary" icon={<PlusCircleFilled />} onClick={() => setShowModal(true)}>
            {t(buttons.AddBanner)}
          </Button>,
        ]}
      />
      <OperateBanner visible={showModal} onClose={handleClose} onOk={handleOk} data={currentData} />
    </section>
  );
};
