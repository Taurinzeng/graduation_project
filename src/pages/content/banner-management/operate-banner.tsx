import { Button, Checkbox, Form, Input, InputNumber, message, Modal } from 'antd';
import React, { useEffect, useState } from 'react';

import { translations } from '@locales/i18n';
import { useTranslation } from 'react-i18next';
import { useForm } from 'antd/lib/form/Form';
import { addBanner, editBanner } from '@service/content/banner';
import { ResponseType } from '@modal/reducer/modal';

interface IProps {
  visible: boolean;
  onClose: () => void;
  onOk: () => void;
  data?: any;
}

const OperateBanner = (props: IProps) => {
  const { visible, onClose, onOk, data } = props;
  const { t } = useTranslation();
  const { buttons, contentMList, placeholder } = translations;
  const [form] = useForm();
  const handleSubmit = () => {
    form.validateFields().then((values: any) => {
      const operateAjax = data.id ? editBanner : addBanner;
      operateAjax({ ...values, id: data.id }).then((res: ResponseType) => {
        message.success(res.msg);
        onOk && onOk();
      });
    });
  };
  useEffect(() => {
    if (visible) {
      form.setFieldsValue({ ...data, site: data?.site?.split(',') || [] });
    } else {
      form.resetFields();
    }
  }, [data, visible]);
  return (
    <Modal width={600} visible={visible} title={t(buttons.AddBanner)} cancelText={t(buttons.Cancel)} okText={t(buttons.Confirm)} onOk={handleSubmit} onCancel={onClose}>
      <Form form={form} size={'large'} colon={false} layout={'vertical'}>
        <Form.Item name="title" label={t(contentMList.Title)} required>
          <Input placeholder={t(placeholder.Max20Digits)} />
        </Form.Item>
        <Form.Item name="websiteType" label={t(contentMList.SendSite)}>
          <Checkbox.Group
            options={[
              { label: '中文站', value: '1' },
              { label: '英文站', value: '2' },
            ]}
          />
        </Form.Item>
        <Form.Item name="redirectEnUrl" label={t(contentMList.linkUrlEn)}>
          <Input placeholder={t(placeholder.Url)} />
        </Form.Item>
        <Form.Item name="redirectUrl" label={t(contentMList.linkUrlCn)}>
          <Input />
        </Form.Item>
        <Form.Item name="fid" label={t(contentMList.Banner)} required></Form.Item>
        <Form.Item name="sortWeight" label={t(contentMList.Weight)} required>
          <InputNumber min={1} max={100} placeholder={t(placeholder.SortNumber)} style={{ width: '100%' }} />
        </Form.Item>
      </Form>
    </Modal>
  );
};
export default OperateBanner;
