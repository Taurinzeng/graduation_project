import { Checkbox, Form, Input, InputNumber, message, Modal } from 'antd';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import { useForm } from 'antd/lib/form/Form';
import { addLink, editLink } from '@service/content/banner';
import { ResponseType } from '@modal/reducer/modal';

interface IProps {
  visible: boolean;
  onClose: () => void;
  onOk: () => void;
  data?: any;
}
const OperateLink = (props: IProps) => {
  const { visible, onClose, onOk, data } = props;
  const { t } = useTranslation();
  const { buttons, contentMList, placeholder } = translations;
  const [form] = useForm();
  const handleSubmit = () => {
    form.validateFields().then((values: any) => {
      const operateAjax = data.id ? editLink : addLink;
      operateAjax({ ...values, id: data.id }).then((res: ResponseType) => {
        message.success(res.msg);
        onOk && onOk();
      });
    });
  };
  useEffect(() => {
    if (visible) {
      form.setFieldsValue({ ...data, site: data?.site?.split(',') || [] });
    } else {
      form.resetFields();
    }
  }, [data, visible]);
  return (
    <Modal width={600} visible={visible} title={t(buttons.CreateLinks)} cancelText={t(buttons.Cancel)} okText={t(buttons.Confirm)} onOk={handleSubmit} onCancel={onClose}>
      <Form form={form} size={'large'} colon={false} layout={'vertical'}>
        <Form.Item name="title" label={t(contentMList.SiteName)} required>
          <Input placeholder={t(placeholder.Max20Digits)} />
        </Form.Item>

        <Form.Item name="redirectUrl" label={t(contentMList.linkUrl)} required>
          <Input placeholder={t(placeholder.UrlOther)} />
        </Form.Item>
        <Form.Item name="sortWeight" label={t(contentMList.Weight)} required>
          <InputNumber min={1} max={100} placeholder={t(placeholder.SortNumber)} style={{ width: '100%' }} />
        </Form.Item>
        <Form.Item name="site" label={t(contentMList.SendSite)}>
          <Checkbox.Group
            options={[
              { label: '中文站', value: '1' },
              { label: '英文站', value: '2' },
            ]}
          />
        </Form.Item>
      </Form>
    </Modal>
  );
};
export default OperateLink;
