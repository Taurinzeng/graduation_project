import React, { useEffect, useState } from 'react';
import { Button, Checkbox, Col, Form, Input, InputNumber, message, Row, Select } from 'antd';
import RichEditor from '@components/rich-editor';
import './operate.less';
import FooterButtons from '@components/button';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import TextArea from 'antd/lib/input/TextArea';
import { useForm } from 'antd/lib/form/Form';
import { addContent, editContent, getContentDetail, getNavigationList } from '@service/content/content';
import { ResponseType } from '@modal/reducer/modal';
import { useDispatch } from 'react-redux';
import { closePage } from '@modal/reducer/work';
import { useHistory, useParams } from 'react-router-dom';

interface IProps {}

export default (props: IProps) => {
  const {} = props;
  const { t } = useTranslation();
  const [form] = useForm();
  const { id } = useParams<{ id: string }>();
  const { contentMList, buttons, placeholder, table } = translations;
  const [sourceList, setSourceList] = useState([]);
  const dispatch = useDispatch();
  const history = useHistory();
  const handleSubmit = () => {
    form.validateFields().then((values: any) => {
      const operateAjax = id ? editContent : addContent;
      operateAjax({
        ...values,
        id,
      }).then((res: ResponseType) => {
        if (res.code === 200) {
          message.success('操作成功');
          dispatch(closePage({ path: location.pathname.replace('/work/', ''), history }));
        }
      });
    });
  };
  useEffect(() => {
    getContentDetail({ id }).then((res: ResponseType) => {
      form.setFieldsValue({
        ...res.data,
      });
    });
    getNavigationList().then((res: ResponseType) => {
      if (res.code === 200) {
        setSourceList(res.data.list || []);
      }
    });
  }, []);
  return (
    <>
      <section className="content-management-info-page">
        <Form size={'large'} colon={false} layout={'vertical'} form={form}>
          <Row gutter={24}>
            <Col span={12}>
              <Form.Item name="title" label={t(contentMList.Title)} required>
                <Input />
              </Form.Item>
            </Col>
            <Col span={1} />
            <Col span={11}>
              <Form.Item name="navigationId" label={t(contentMList.PublishMenu)} required>
                <Select options={sourceList.map((source: any) => ({ label: source.menuName, value: source.id }))} placeholder={t(placeholder.Choice)} />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item name="author" label={t(contentMList.PublishAuthor)} tooltip="What do you want others to call you?">
                <Input placeholder={t(placeholder.Max20DigitsOr)} />
              </Form.Item>
            </Col>
            <Col span={1} />
            <Col span={11}>
              <Form.Item name="websiteType" label={t(contentMList.SendSite)} tooltip="What do you want others to call you?">
                <Checkbox.Group
                  options={[
                    { label: '中文站', value: 'cn' },
                    { label: '英文站', value: 'en' },
                  ]}
                />
              </Form.Item>
            </Col>
            <Col span={12}>
              <Form.Item name="sortWeight" label={t(contentMList.Weight)} tooltip="What do you want others to call you?">
                <InputNumber min={1} max={100} placeholder={t(placeholder.RuleNumber)} style={{ width: '100%' }} />
              </Form.Item>
            </Col>
            <Col span={1} />
          </Row>
          <Row>
            <Col span={12}>
              <Form.Item name="summary" label={t(table.Abstract)} required>
                <TextArea />
              </Form.Item>
            </Col>
          </Row>
          <Form.Item name="content" label={t(contentMList.Detail)} tooltip="What do you want others to call you?">
            <RichEditor height="200px" />
          </Form.Item>
          <Form.Item name="name" label={t(contentMList.UploadFile)}></Form.Item>
        </Form>
      </section>
      <FooterButtons
        btns={[
          <Button size={'large'} key={'submit'} type="primary" onClick={handleSubmit}>
            {t(buttons.SaveAndSubmit)}
          </Button>,
        ]}
      />
    </>
  );
};
