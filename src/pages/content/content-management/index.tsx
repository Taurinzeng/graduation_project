import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { PlusCircleFilled } from '@ant-design/icons';

import { Button, Divider } from 'antd';
import { useHistory } from 'react-router';
import { queryContentList } from '@service/content/content';
import { Link } from 'react-router-dom';

export default () => {
  const history = useHistory();
  const { t } = useTranslation();
  const { table, buttons } = translations;
  const columns: any[] = [
    {
      title: t(table.ID),
      dataIndex: 'no',
      render: (text: any, record: any, index: number) => {
        return index + 1;
      },
    },
    {
      title: t(table.ContentTitle),
      dataIndex: 'title',
    },
    {
      title: t(table.Abstract),
      dataIndex: 'summary',
    },
    {
      title: t(table.Author),
      dataIndex: 'author',
    },
    {
      title: t(table.PublishMenu),
      dataIndex: 'navigationId',
    },
    {
      title: t(table.SiteType),
      dataIndex: 'adviser',
    },
    {
      title: t(table.Status),
      dataIndex: 'status',
    },
    {
      title: t(table.UpdateTime),
      dataIndex: 'updateTime',
    },
    
    {
      title: t(table.Operation),
      dataIndex: 'operation',
      fixed: 'right',
      render: (text: any, record: any) => {
        return (
          <>
            <Link to={`/work/content_manage/content_info_manage/edit/${record.id}`}>{t(buttons.Edit)}</Link>
            <Divider type="vertical"/>
            <Link to={`/work/content_manage/content_info_manage/detail/${record.id}`}>{t(buttons.View)}</Link>
          </>
        )
      }
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(table.Status),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '启用',
          value: '1',
        },
        {
          text: '停用',
          value: '2',
        },
      ],
    },
    {
      label: t(table.Type),
      code: 'type',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '启用',
          value: '1',
        },
        {
          text: '停用',
          value: '2',
        },
      ],
    },
    {
      label: t(table.CreateTime),
      code: 'teacherName',
      type: 'dateRange',
    },
  ];
  return (
    <section className="content-management-page">
      <TableTemplate
        columns={columns}
        queryAjax={queryContentList}
        rowKey={(record: any) => record.id}
        filterFields={filterFields}
        topButtons={[<Button type="primary" icon={<PlusCircleFilled/>} onClick={() => {
          history.push(`/work/content_manage/content_info_manage/create`);
        }}>
          {t(buttons.Add)}
        </Button>]}
      />
    </section>
  );
};
