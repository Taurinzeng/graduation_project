import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { queryMessageList } from '@service/content/message';
import { Button, Divider } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import { useHistory } from 'react-router';
import { Link } from 'react-router-dom';

export default () => {
  const { t } = useTranslation();
  const { table, buttons } = translations;
  const history = useHistory();
  const columns = [
    {
      title: t(table.ID),
      dataIndex: 'no',
      render: (text: any, record: any, index: number) => {
        return index + 1;
      },
    },
    {
      title: t(table.Title),
      dataIndex: 'title',
      
    },
    {
      title: t(table.SendUser),
      dataIndex: 'useName',
    },
    {
      title: t(table.NavCreator),
      dataIndex: 'createUsername',
    },
    {
      title: t(table.Status),
      dataIndex: 'statusName',
    },
    {
      title: t(table.SendTime),
      dataIndex: 'sendTime',
    },
    {
      title: t(table.CreateTime),
      dataIndex: 'createTime',
    },
    {
      title: t(table.Operation),
      dataIndex: 'operation',
      render: (text: any, record: any) => {
        return (
          <>
            <Link to={`/work/content_manage/message_manage/edit/${record.id}`}>{t(buttons.Edit)}</Link>
            <Divider type="vertical" />
            <Link to={`/work/content_manage/message_manage/detail/${record.id}`}>{t(buttons.View)}</Link>
          </>
        );
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(table.Type),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '启用',
          value: '1',
        },
        {
          text: '停用',
          value: '2',
        },
      ],
    },
    {
      label: t(table.Status),
      code: 'status',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '已发送',
          value: '1',
        },
        {
          text: '待发送',
          value: '2',
        },
      ],
    },

    {
      label: t(table.CreateTime),
      code: 'teacherName',
      type: 'dateRange',
    },
  ];
  return (
    <section className="news-management-page">
      <TableTemplate
        columns={columns}
        queryAjax={queryMessageList}
        rowKey={(record: any) => record.id}
        filterFields={filterFields}
        topButtons={[
          <Button
            type="primary"
            onClick={() => {
              history.push(`/work/content_manage/message_manage/create`);
            }}>
            {t(buttons.AddNew)}
          </Button>,
        ]}
      />
    </section>
  );
};
