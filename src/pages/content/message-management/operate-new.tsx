import React, { useEffect, useState } from 'react';
import { Button, Checkbox, Col, DatePicker, Form, Input, message, Row, TreeSelect } from 'antd';
import RichEditor from '@components/rich-editor';
import FooterButtons from '@components/button';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import { useHistory, useParams } from 'react-router-dom';
import { FileUpload } from '@components/attachment';
import { useForm } from 'antd/lib/form/Form';
import './operate.less';
import { ResponseType } from '@modal/reducer/modal';
import Modal from 'antd/lib/modal/Modal';
import { addMessage, editMessage, getMessageDetail } from '@service/content/message';
import { closePage } from '@modal/reducer/work';
import { useDispatch } from 'react-redux';
import PersonSelect from '@components/person-select';

interface IProps {}

export default (props: IProps) => {
  const {} = props;
  const { type, id } = useParams<{ type?: string; id?: string }>();
  const [form] = useForm();
  const [showTimeModal, setShowTimeModal] = useState(false);
  const [sendDate, setSendDate] = useState('');
  const dispatch = useDispatch();
  const history = useHistory();
  const { t } = useTranslation();
  const { table, buttons, contentMList, other } = translations;
  const formItemLayout = {
    wrapperCol: {
      span: 12,
    },
  };
  const handleSubmit = (status: number) => {
    form.validateFields().then((values: any) => {
      const params = {
        ...values,
        status,
        sendWay: values.sendWay?.join(','),
      };
      if (status === 2) {
        params.sendTime = sendDate;
      }
      const operateAjax = id ? editMessage : addMessage;
      operateAjax(params).then((res: ResponseType) => {
        if (res.code === 200) {
          message.success(res.msg);
          dispatch(closePage({ path: location.pathname.replace('/work/', ''), history }));
          setShowTimeModal(false);
        }
      });
    });
  };
  useEffect(() => {
    if (id) {
      getMessageDetail({ id }).then((res: ResponseType) => {
        form.setFieldsValue({
          ...res.data,
          sendWay: res.data?.sendWay.split(','),
        });
      });
    }
  }, [id]);
  return (
    <>
      <section className="news-management-info-page">
        <Form size={'large'} colon={false} layout={'vertical'} {...formItemLayout} form={form}>
          <Form.Item name="roles" label={t(table.SendUser)} rules={[{ required: true, message: t(table.SendUser) + t(other.required) }]}>
            <PersonSelect />
          </Form.Item>
          <Form.Item name="title" label={t(table.Title)} tooltip="What do you want others to call you?" rules={[{ required: true, message: t(table.Title) + t(other.required) }]}>
            <Input />
          </Form.Item>
          <Form.Item name="content" label={t(contentMList.NoticeContent)} tooltip="What do you want others to call you?">
            <Input.TextArea rows={5} />
          </Form.Item>
          <Form.Item name="files" label={t(contentMList.UploadFile)} tooltip="What do you want others to call you?">
            <FileUpload draggerable>
              <div>将文件放在此处或点击上传。</div>
            </FileUpload>
          </Form.Item>
          <Form.Item name="sendWay" label={t(contentMList.SendType)}>
            <Checkbox.Group
              options={[
                { label: '系统公告', value: '1' },
                { label: '邮件', value: '2' },
                { label: '手机短信', value: '3' },
              ]}
            />
          </Form.Item>
        </Form>
      </section>
      <FooterButtons
        btns={[
          <Button size={'large'} key={'submit'} type="primary" onClick={() => handleSubmit(1)}>
            {t(buttons.SendNow)}
          </Button>,
          <Button key={'time'} size={'large'} onClick={() => setShowTimeModal(true)}>
            {t(buttons.TimingTransmission)}
          </Button>,
          <Button key={'save'} size={'large'} onClick={() => handleSubmit(3)}>
            {t(buttons.SaveDraft)}
          </Button>,
        ]}
      />
      <Modal visible={showTimeModal} onCancel={() => setShowTimeModal(false)} onOk={() => handleSubmit(2)} title={t(contentMList.Timedrelease)}>
        <Form.Item label={t(contentMList.SetPublishingtime)}>
          <DatePicker showTime onChange={(v: any) => setSendDate(v)} />
        </Form.Item>
      </Modal>
    </>
  );
};
