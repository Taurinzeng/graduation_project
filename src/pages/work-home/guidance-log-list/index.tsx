import PageTitle from '@components/page-title';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import React from 'react';
import { useTranslation } from 'react-i18next';

export default () => {
  const { t } = useTranslation();
  const { table } = translations;
  const columns = [
    {
      title: t(translations.guidance.courseName),
      dataIndex: 'courseName',
    },
    {
      title: t(translations.guidance.courseType),
      dataIndex: 'courseType',
    },
    {
      title: t(translations.guidance.researchField),
      dataIndex: 'researchField',
    },
    {
      title: t(translations.guidance.source),
      dataIndex: 'source',
    },
    {
      title: t(translations.guidance.forProfessional),
      dataIndex: 'forProfessional',
    },
    {
      title: t(translations.guidance.adviser),
      dataIndex: 'adviser',
    },
    {
      title: t(translations.guidance.operation),
      dataIndex: 'operation',
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(translations.guidance.courseName),
      code: 'courseName',
      type: 'input',
    },
  ];
  return (
    <section className="todo-page">
      <TableTemplate columns={columns} queryAjax={() => Promise.resolve()} rowKey={(record: any) => record.id} filterFields={filterFields} />
    </section>
  );
};
