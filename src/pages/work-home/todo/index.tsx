import CountBar from '@components/count-bar';
import PageTitle from '@components/page-title';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import React from 'react';
import { useTranslation } from 'react-i18next';
import './index.less';

export default () => {
  const { t } = useTranslation();
  const columns = [
    {
      title: t(translations.todo.no),
      dataIndex: 'no',
      render: (text: any, record: any, index: number) => {
        return index + 1;
      },
    },
    {
      title: t(translations.todo.eventName),
      dataIndex: 'eventName',
    },
    {
      title: t(translations.todo.acceptTime),
      dataIndex: 'acceptTime',
    },
    {
      title: t(translations.todo.finishedTime),
      dataIndex: 'finishedTime',
    },
    {
      title: t(translations.todo.status),
      dataIndex: 'statusName',
    },
    {
      title: t(translations.todo.operation),
      dataInde: 'operation',
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(translations.todo.status),
      code: 'status',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '待处理',
          value: '1',
        },
        {
          text: '已完成',
          value: '2',
        },
      ],
    },
  ];
  return (
    <section className="todo-page">
      <CountBar
        items={[
          [
            {
              title: `${t(translations.todo.todoCount)}`,
              count: 888,
            },
          ],
        ]}
      />
      <TableTemplate
        columns={columns}
        queryAjax={() => Promise.resolve()}
        rowKey={(record: any) => record.id}
        filterFields={filterFields}
      />
    </section>
  );
};
