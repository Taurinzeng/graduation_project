import React from 'react';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import './index.less';
export default () => {
  const { t } = useTranslation();
  const { overview } = translations;
  return (
    <section className="overview-page">
      <div className="welcome">{t(overview.welcomeMaster)}</div>
      <div className="panel-line">
        <div className="project-view">
          <div className="title">{t(overview.projectView)}</div>
          <div>{t(overview.projectName)}: xxxx</div>
          <div>{t(overview.startAndEndTime)}: 2020.01.02 - 2022.01.01</div>
          <div>{t(overview.graduateStuCount)}: 999</div>
          <div>{t(overview.teacherCount)}: 88</div>
          <div>{t(overview.professorCount)}: 8</div>
        </div>
        <div className="project-schedule">
          <div>{t(overview.projectSchedule)}</div>
        </div>
      </div>
    </section>
  );
};
