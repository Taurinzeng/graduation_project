import RichEditor from '@components/rich-editor';
import { Button, Form } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import React, { useEffect } from 'react';
import {FileUpload} from '@components/attachment';
const Test = () => {
  const [form] = useForm();
  const handleSubmit = (value: any) => {
    console.log(value);
  };
  useEffect(() => {
    setTimeout(() => {
      form.setFieldsValue({
        test: '<p>sdfsdf<img src="https://vkceyugu.cdn.bspapp.com/VKCEYUGU-566db12e-1d3c-4aea-99af-eabf8a1d35c9/0f2dea25-47a9-4cde-9157-d7257837a525.jpg" width="172" style="cursor: nwse-resize;"></p>',
      });
    });
  }, []);
  return (
    <div>
      <FileUpload>
        <Button>上传</Button>
      </FileUpload>
      <Form form={form} onFinish={handleSubmit}>
        <Form.Item name="test">
          <RichEditor />
        </Form.Item>
        <Form.Item>
          <Button htmlType="submit">提交</Button>
        </Form.Item>
      </Form>
    </div>
  );
};
export default Test;
