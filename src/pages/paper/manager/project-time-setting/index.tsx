import React, { useEffect, useState } from 'react';
import { translations } from '@locales/i18n';
import PageTitle from '@components/page-title';
import { useTranslation } from 'react-i18next';
import TimeConfig from './time-config';
import TimeConfigMutiple from './time-config-mutiple';
import { Form } from 'antd';

import Style from './index.module.less';
import { useForm } from 'antd/lib/form/Form';
import { getProjectConfig } from '@service/paper/project';
import { ResponseType } from '@modal/reducer/modal';

export default () => {
  const { t } = useTranslation();
  const { ProjectSetting } = translations;
  const [configData, setConfigData] = useState<any>({});
  const [form] = useForm();
  useEffect(() => {
    getProjectConfig().then((res: ResponseType) => {
      setConfigData(res.data || {});
    });
  }, []);
  return (
    <section className={Style.project_time_setting}>
      <Form form={form}>
        <TimeConfig configData={configData} form={form} title={t(ProjectSetting.S1)} messageT={t(ProjectSetting.S1M)} code={['paperStartTime', 'paperEndTime']} />
        <TimeConfigMutiple configData={configData} form={form} title={t(ProjectSetting.S2)} messageT={t(ProjectSetting.S2M)} code="1" />
        <TimeConfigMutiple configData={configData} form={form} title={t(ProjectSetting.S3)} messageT={t(ProjectSetting.S3M)} code="2" />
        <TimeConfig configData={configData} form={form} title={t(ProjectSetting.S4)} messageT={t(ProjectSetting.S4M)} code={['preDistributeStartTime', 'preDistributeEndTime']} />
        <TimeConfigMutiple configData={configData} form={form} title={t(ProjectSetting.S5)} messageT={t(ProjectSetting.S5M)} code="3" />
        <TimeConfigMutiple configData={configData} form={form} title={t(ProjectSetting.S6)} messageT={t(ProjectSetting.S6M)} code="4" />
        <TimeConfigMutiple configData={configData} form={form} title={t(ProjectSetting.S7)} messageT={t(ProjectSetting.S7M)} code="5" />
        <TimeConfig configData={configData} form={form} title={t(ProjectSetting.S8)} messageT={t(ProjectSetting.S8M)} code={['distSecondStartTime', 'distSecondEndTime']} />
        <TimeConfig configData={configData} form={form} title={t(ProjectSetting.S9)} messageT={t(ProjectSetting.S9M)} code={['prestageTaskStartTime', 'prestageTaskEndTime']} />
        <TimeConfig
          form={form}
          configData={configData}
          title={t(ProjectSetting.S10)}
          messageT={t(ProjectSetting.S10M)}
          switchCode="prestageReportFlag"
          code={['prestageReportStartTime', 'prestageReportEndTime']}
        />
        <TimeConfig
          configData={configData}
          form={form}
          title={t(ProjectSetting.S11)}
          messageT={t(ProjectSetting.S11M)}
          code={['prestageReportscoreStartTime', 'prestageReportscoreEndTime']}
        />
        <TimeConfig configData={configData} form={form} title={t(ProjectSetting.S12)} messageT={t(ProjectSetting.S12M)} code={['prestageCheckStartTime', 'prestageCheckEndTime']} />
        <TimeConfig
          form={form}
          configData={configData}
          title={t(ProjectSetting.S13)}
          messageT={t(ProjectSetting.S13M)}
          switchCode="mestageReportFlag"
          code={['mestageReportStartTime', 'mestageReportEndTime']}
        />
        <TimeConfig
          configData={configData}
          form={form}
          title={t(ProjectSetting.S14)}
          messageT={t(ProjectSetting.S14M)}
          code={['mestageReportscoreStartTime', 'mestageReportscoreEndTime']}
        />
        <TimeConfig configData={configData} form={form} title={t(ProjectSetting.S15)} messageT={t(ProjectSetting.S15M)} code={['mestageCheckStartTime', 'mestageCheckEndTime']} />
        <TimeConfig
          form={form}
          configData={configData}
          title={t(ProjectSetting.S16)}
          messageT={t(ProjectSetting.S16M)}
          switchCode="paperSubmitFlag"
          code={['paperSubmitStartTime', 'paperSubmitEndTime']}
        />
        <TimeConfig
          form={form}
          configData={configData}
          title={t(ProjectSetting.S17)}
          messageT={t(ProjectSetting.S17M)}
          switchCode="paperCheckdupFlag"
          code={['paperCheckdupStartTime', 'paperCheckdupEndTime']}
        />
        <TimeConfig configData={configData} form={form} title={t(ProjectSetting.S18)} messageT={t(ProjectSetting.S18M)} code={['paperPsStartTime', 'paperPsEndTime']} />
        <TimeConfig configData={configData} form={form} title={t(ProjectSetting.S19)} messageT={t(ProjectSetting.S19M)} code={['scoreInputStartTime', 'scoreInputEndTime']} />
        <TimeConfig configData={configData} form={form} title={t(ProjectSetting.S20)} messageT={t(ProjectSetting.S21M)} code={['resultConfirmStartTime', 'resultConfirmEndTime']} />
      </Form>
    </section>
  );
};
