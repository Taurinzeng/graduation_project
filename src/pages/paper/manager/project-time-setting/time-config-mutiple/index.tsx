import { DatePicker, Form, FormProps, Switch } from 'antd';
import React, { useEffect, useState } from 'react';
import { MinusCircleOutlined, PlusCircleOutlined } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';
import Style from './index.module.less';
import { translations } from '@locales/i18n';
import moment from 'moment';

interface IProps {
  title: string;
  messageT: string;
  code: string;
  configData: any;
  hasSwitch?: boolean;
  form?: any;
}
export default (props: IProps) => {
  const { title, messageT, code, hasSwitch, form, configData = {} } = props;
  const {
    unit,
    table: { StartTime, EndTime },
    ProjectSetting: { overlay },
  } = translations;
  const { t } = useTranslation();
  const handleChange = () => {
    console.log(form?.getFieldsValue(code));
  };
  useEffect(() => {
    const data = configData.prjTimeSetting || [];
    let value = data.filter((each: any) => each.type === code);
    value = value.map((each: any) => [each.startTime > 0 ? moment(each.startTime) : moment(-each.startTime), each.endTime > 0 ? moment(each.endTime) : moment(-each.endTime)]);
    form.setFieldsValue({ [code]: value });
  }, [configData]);
  return (
    <div className={Style.timeConfigUnit}>
      <div className={Style.title}>
        <span>{title}</span>
        {hasSwitch && (
          <span className={Style.switchTitle}>
            {t(overlay)}&nbsp;&nbsp;
            <Switch />
          </span>
        )}
      </div>
      <div className={Style.message}>{messageT}</div>
      <Form.List name={code}>
        {(fields, { add, remove }, { errors }) => {
          if (fields.length === 0) {
            add();
          }
          return (
            <>
              {fields.map((field, index) => (
                <div className={Style.dateUnit} key={field.key}>
                  <Form.Item label={`${t(unit.the)}${index + 1}${t(unit.time)}`} name={[index]}>
                    <DatePicker.RangePicker onChange={handleChange} />
                  </Form.Item>
                  {index === fields.length - 1 && <PlusCircleOutlined className={Style.addButtonIcon} onClick={() => add()} />}

                  {index > 0 ? (
                    <MinusCircleOutlined
                      className={Style.deleteButtonIcon}
                      onClick={() => {
                        remove(field.name);
                      }}
                    />
                  ) : null}
                </div>
              ))}
            </>
          );
        }}
      </Form.List>
    </div>
  );
};
