import { DatePicker, Form, FormProps, Switch } from 'antd';
import React, { useEffect, useState } from 'react';
import { MinusCircleOutlined, PlusCircleOutlined } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';
import Style from './index.module.less';
import { translations } from '@locales/i18n';
import { saveProjectConfig } from '@service/paper/project';
import { ResponseType } from '@modal/reducer/modal';
import { message } from 'antd';
import moment from 'moment';

interface IProps {
  title: string;
  messageT: string;
  configData: any;
  code: string[];
  switchCode?: string;
  form?: any;
  listField?: boolean;
}
export default (props: IProps) => {
  const { title, messageT, code, form, switchCode, configData } = props;
  const {
    table: { StartTime, EndTime },
    ProjectSetting: { overlay },
  } = translations;
  const { t } = useTranslation();
  const handleDateChange = () => {
    const val = form.getFieldsValue(code);
    if (val[code[0]] && val[code[1]]) {
      saveProjectConfig({
        [code[0]]: val[code[0]]?.valueOf(),
        [code[1]]: val[code[1]]?.valueOf(),
      }).then((res: ResponseType) => {
        message.success(res.msg);
      });
    }
  };
  const handleSwitchChange = (v: any) => {
    saveProjectConfig({
      [switchCode!]: v ? '1' : '0',
    }).then((res: ResponseType) => {
      message.success(res.msg);
    });
  };
  useEffect(() => {
    const initData = {
      [code[0]]: configData[code[0]] >= 0 ? moment(configData[code[0]]) : moment(-configData[code[0]]),
      [code[1]]: configData[code[1]] >= 0 ? moment(configData[code[1]]) : moment(-configData[code[1]]),
    };
    if (switchCode) {
      initData[switchCode] = configData[switchCode];
    }
    form.setFieldsValue(initData);
  }, [configData]);
  return (
    <div className={Style.timeConfigUnit}>
      <div className={Style.title}>
        <span>{title}</span>
        {switchCode && (
          <span className={Style.switchTitle}>
            {t(overlay)}&nbsp;&nbsp;
            <Form.Item style={{ margin: 0 }} name={switchCode}>
              <Switch onChange={handleSwitchChange} />
            </Form.Item>
          </span>
        )}
      </div>
      <div className={Style.message}>{messageT}</div>

      <div className={Style.dateUnit}>
        <Form.Item name={code[0]}>
          <DatePicker onChange={handleDateChange} placeholder={t(StartTime)} style={{ width: 200, height: 40, textAlign: 'center' }} onOk={() => {}} />
        </Form.Item>
        <span className={Style.to}>至</span>
        <Form.Item name={code[1]}>
          <DatePicker onChange={handleDateChange} placeholder={t(EndTime)} style={{ width: 200, height: 40, textAlign: 'center' }} onOk={() => {}} />
        </Form.Item>
      </div>
    </div>
  );
};
