import PageTitle from '@components/page-title';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import './index.less';
export default () => {
  const { t } = useTranslation();
  const { procurement } = translations;
  const columns = [
    {
      title: t(procurement.no),
      dataIndex: 'no',
      render: (text: any, record: any, index: number) => {
        return index + 1;
      },
    },
    {
      title: t(procurement.procurementId),
      dataIndex: 'stuName',
    },
    {
      title: t(procurement.brandName),
      dataIndex: 'UESTCID',
    },
    {
      title: t(procurement.recommendSupplier),
      dataIndex: 'UESTCID',
    },
    {
      title: t(procurement.num),
      dataIndex: 'researchField',
    },
    {
      title: t(procurement.unitPrice),
      dataIndex: 'researchField',
    },
    {
      title: t(procurement.totalPrice),
      dataIndex: 'source',
    },
    {
      title: t(procurement.applyName),
      dataIndex: 'adviser',
    },
    {
      title: t(procurement.applyTime),
      dataIndex: 'academy',
    },
    {
      title: t(procurement.auditStatus),
      dataIndex: 'academy',
    },
    {
      title: t(procurement.operation),
      dataIndex: 'operation',
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(procurement.auditStatus),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },

    {
      label: t(procurement.brandName),
      code: 'stuName',
      type: 'input',
    },
  ];
  return (
    <section className="procurement-management-page">
      <PageTitle title={t(procurement.procurementManagement)} />
      <div className="notice">
        <div className="notice-title">{t(procurement.notice)}</div>
        <div className="notice-content">1、{t(procurement.notice1)}；</div>
        <div className="notice-content">2、{t(procurement.notice2)}；</div>
      </div>
      <div className="expert-total">
        <div className="total-unit">
          <div className="text">{t(procurement.auditedTotal)}</div>
          <span className="number">800</span>
        </div>
        <div className="total-unit">
          <div className="text">{t(procurement.unAunditTotal)}</div>
          <span className="number">100</span>
        </div>
      </div>
      <TableTemplate
        columns={columns}
        queryAjax={() => Promise.resolve()}
        rowKey={(record: any) => record.id}
        filterFields={filterFields}
      />
    </section>
  );
};
