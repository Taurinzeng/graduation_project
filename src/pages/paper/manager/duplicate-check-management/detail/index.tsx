import AuditForm from '@components/template/audit-form';
import DuplicateReport from '@components/template/duplicate-report';
import MovingInfo from '@components/template/moving-info';
import React from 'react';
import { useParams } from 'react-router-dom';
import Style from './index.module.less';
const DuplicateManageDetail = () => {
  const { type } = useParams<{ type: string; id: string }>();
  return (
    <section className={Style.duplicateCheckDetail}>
      <DuplicateReport />
      {type === 'audit' && <AuditForm />}
      <MovingInfo />
    </section>
  );
};

export default DuplicateManageDetail;
