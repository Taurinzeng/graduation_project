import CountBar from '@components/count-bar';
import PageTitle from '@components/page-title';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { Divider, Radio } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import './index.less';
export default () => {
  const { t } = useTranslation();
  const {
    paperCheckList,
    table,
    project,
    student,
    buttons: { View, Approve },
  } = translations;
  const columns = [
    {
      title: t(project.ProjectTitle),
      dataIndex: 'stuName',
    },
    {
      title: t(student.GUID),
      dataIndex: 'UESTCID',
    },
    {
      title: t(student.name),
      dataIndex: 'UESTCID',
    },
    {
      title: t(table.FirstAdviser),
      dataIndex: 'researchField',
    },
    {
      title: t(table.SecondAdviser),
      dataIndex: 'source',
    },
    {
      title: t(table.Submittime),
      dataIndex: 'adviser',
    },
    {
      title: t(table.Status),
      dataIndex: 'academy',
    },
    {
      title: t(table.Operation),
      dataIndex: 'id',
      render: (id: string, record: any) => {
        return (
          <>
            <Link to={`/work/paper_manage/duplicate_check_manage/view/${id}`}>{t(View)}</Link>
            <Divider type="vertical" />
            <Link to={`/work/paper_manage/duplicate_check_manage/audit/${id}`}>{t(Approve)}</Link>
          </>
        );
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(table.SubmitStatus),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },
    {
      label: t(table.CheckStatus),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },

    {
      label: t(project.ProjectTitle),
      code: 'stuName',
      type: 'input',
    },
  ];
  return (
    <section className="duplicate-management-page">
      <CountBar
        items={[
          [
            {
              title: t(paperCheckList.Totalsubmitted),
              count: 100,
            },
            {
              title: t(paperCheckList.TotalSuccesses),
              count: 100,
            },
            {
              title: t(paperCheckList.TotalFailed),
              count: 100,
            },
          ],
          [
            {
              title: t(paperCheckList.Totaltobesubmitted2),
              count: 100,
            },
            {
              title: t(paperCheckList.TotalNotsubmitted),
              count: 100,
            },
          ],
        ]}
        total={[
          {
            title: t(paperCheckList.Totaltobesubmitted),
            count: 200,
          },
        ]}
      />
      <TableTemplate columns={columns} queryAjax={() => Promise.resolve(resultData)} rowKey={(record: any) => record.id} filterFields={filterFields} />
    </section>
  );
};

const resultData = {
  code: 200,
  data: {
    list: [
      {
        id: 1,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uog',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 2,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 3,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
    ],
  },
};
