import Notice from '@components/notice';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { Divider, Button, Popconfirm } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

const ArbitrationManage = () => {
  const { t } = useTranslation();
  const { table, student, project, buttons, roll } = translations;
  const columns: any = [
    {
      title: t(project.ProjectTitle),
      dataIndex: 'stuName',
    },
    {
      title: t(project.SelectedStudents),
      dataIndex: 'UESTCID',
    },
    {
      title: t(table.FirstAdviser),
      dataIndex: 'UESTCID',
    },
    {
      title: t(table.SecondAdviser),
      dataIndex: 'researchField',
    },
    {
      title: t(table.UESTCExaminerGrade),
      dataIndex: 'source',
    },
    {
      title: t(table.UOGExaminerGrade),
      dataIndex: 'adviser',
    },
    {
      title: t(table.ArbitrationExpertGrade),
      dataIndex: 'academy',
    },
    {
      title: t(table.ArbitrationExpert),
      dataIndex: 'academy',
    },
    {
      title: t(table.ArbitrationTime),
      dataIndex: 'academy',
    },
    {
      title: t(table.Status),
      dataIndex: 'academy',
    },
    {
      title: t(table.Operation),
      dataIndex: 'operation',
      fixed: 'right',
      render: (text: any, record: any, index: number) => {
        return (
          <>
            <Link to={`/work/paper_manage/arbitration_manage/detail/${index}`}>{t(buttons.View)}</Link>
          </>
        );
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(table.Status),
      code: 'status',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '中方',
          value: '1',
        },
        {
          text: '英方',
          value: '2',
        },
      ],
    },

    {
      label: t(project.ProjectTitle),
      code: 'projectTitle',
      type: 'input',
    },
  ];
  return (
    <section>
      <TableTemplate columns={columns} queryAjax={() => Promise.resolve(resultData)} rowKey={(record: any) => record.id} filterFields={filterFields} />
    </section>
  );
};
export default ArbitrationManage;
const resultData = {
  code: 200,
  data: {
    list: [
      {
        id: 1,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uog',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 2,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 3,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
    ],
  },
};
