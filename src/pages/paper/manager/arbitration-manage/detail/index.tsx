import GradeInfo from '@components/template/grade-info';
import MovingInfo from '@components/template/moving-info';
import PlanTable from '@components/template/plan-table';
import TeacherGradeInfo from '@components/template/teacher-grade-info';
import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';

interface IProps {}
const ArbitrationDetail = (props: IProps) => {
  const { arbitrationList } = translations;
  const { t } = useTranslation();
  const { id } = useParams<{ id: string }>();
  const [currentTab, setCurrentTab] = useState('1');
  return (
    <section>
      <Radio.Group onChange={(v) => setCurrentTab(v.target.value)} value={currentTab} style={{ marginTop: 20 }}>
        <Radio.Button value="1">{t(arbitrationList.GradeView)}</Radio.Button>
        <Radio.Button value="2">{t(arbitrationList.ZhGrade)}</Radio.Button>
        <Radio.Button value="3">{t(arbitrationList.EnGrade)}</Radio.Button>
        <Radio.Button value="4">{t(arbitrationList.ArbitratorGrade)}</Radio.Button>
      </Radio.Group>
      <div style={{ display: currentTab === '1' ? 'block' : 'none' }}>
        <GradeInfo />
      </div>
      <div style={{ display: currentTab === '2' ? 'block' : 'none' }}>
        <TeacherGradeInfo gradeId="" />
      </div>
      <div style={{ display: currentTab === '3' ? 'block' : 'none' }}>
        <TeacherGradeInfo gradeId="" />
      </div>
      <div style={{ display: currentTab === '4' ? 'block' : 'none' }}>
        <TeacherGradeInfo gradeId="" />
      </div>
    </section>
  );
};

export default ArbitrationDetail;
