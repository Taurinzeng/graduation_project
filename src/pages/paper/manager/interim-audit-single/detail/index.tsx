import InterimGradeSingle from '@components/template/interim-grade-single';
import InterimReportSingle from '@components/template/interim-report-single';
import InterimSchoolTable from '@components/template/Interim-school-table';
import MovingInfo from '@components/template/moving-info';
import PlanTable from '@components/template/plan-table';
import PreliminaryReport from '@components/template/preliminary-report-single';
import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';

interface IProps {}
const AllotDetail = (props: IProps) => {
  const { interimManagementList } = translations;
  const { t } = useTranslation();
  const { type } = useParams<{ type: '1' | '2' | '3' | '4'; id: string }>();
  const [currentTab, setCurrentTab] = useState('1');
  const typeMap = {
    gOne: t(interimManagementList.Tab2),
    gTwo: t(interimManagementList.Tab3),
    d: t(interimManagementList.Tab4),
    s: t(interimManagementList.Tab5),
  };
  return (
    <section className="page-detail-commoncss">
      <Radio.Group onChange={(v) => setCurrentTab(v.target.value)} value={currentTab} style={{ marginTop: 20 }}>
        <Radio.Button value="1">{t(interimManagementList.Tab1)}</Radio.Button>
        {type !== '1' ? <Radio.Button value="4">{type === '3' ? typeMap.d : typeMap.s}</Radio.Button> : <Radio.Button value="5">{typeMap.gOne}</Radio.Button>}
      </Radio.Group>
      <div style={{ display: currentTab === '1' ? 'block' : 'none' }}>
        <InterimReportSingle />
      </div>
      {type === '1' ? (
        <div style={{ display: currentTab === '5' ? 'block' : 'none' }}>
          <InterimGradeSingle />
        </div>
      ) : (
        <div>
          <InterimSchoolTable type={type === '3' ? 'd' : 's'} tableId="" />
        </div>
      )}

      <MovingInfo />
    </section>
  );
};

export default AllotDetail;
