import SingleReviewerGradeInfo from '@components/template/single-reviewer-grade-info';
import TeacherGradeInfo from '@components/template/teacher-grade-info';
import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router';

const ReviewExpertDetail = () => {
  const { type, id } = useParams<{ type: string; id: string }>();
  const [currentTab, setCurrentTab] = useState<'1' | '2'>('1');
  const { t } = useTranslation();
  const { reviewExpertList } = translations;
  return (
    <section className="page-detail-commoncss">
      {type === 'double' && (
        <Radio.Group onChange={(v) => setCurrentTab(v.target.value)} value={currentTab} style={{ marginTop: 20 }}>
          <Radio.Button value="1">{t(reviewExpertList.ZhReviewer)}</Radio.Button>
          <Radio.Button value="2">{t(reviewExpertList.EnReviewer)}</Radio.Button>
        </Radio.Group>
      )}
      {type === 'single' ? <SingleReviewerGradeInfo /> : <TeacherGradeInfo gradeId="" />}
    </section>
  );
};

export default ReviewExpertDetail;
