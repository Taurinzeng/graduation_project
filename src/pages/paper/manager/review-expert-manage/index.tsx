import Notice from '@components/notice';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { Divider, Button, Popconfirm } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Style from './index.module.less';
import ReviewSelectModal from './revies-select-modal';
const ReviewExpertManage = () => {
  const { t } = useTranslation();
  const { table, student, reviewExpertList, project, buttons, roll } = translations;
  const [showModal, setShowModal] = useState(false);
  const [selectedData, setSelectedData] = useState<any>({});
  const [refresh, setRefresh] = useState<any>({ count: 0, isCurrent: false });
  const columns = [
    {
      title: t(project.ProjectTitle),
      dataIndex: 'stuName',
    },
    {
      title: t(project.ProjectType),
      dataIndex: 'UESTCID',
    },
    {
      title: t(student.GUID),
      dataIndex: 'UESTCID',
    },
    {
      title: t(project.SelectedStudents),
      dataIndex: 'researchField',
    },
    {
      title: t(student.Degreetype),
      dataIndex: 'source',
    },
    {
      title: t(roll.FirstSupervisor),
      dataIndex: 'adviser',
    },
    {
      title: t(roll.ProjectReviewExpert),
      dataIndex: 'academy',
    },
    {
      title: t(table.Status),
      dataIndex: 'academy',
    },
    {
      title: t(table.Operation),
      dataIndex: 'operation',
      render: (text: any, record: any, index: number) => {
        return (
          <>
            <a onClick={() => handleModify({ degreeType: record.degreeType, stuId: record.id })}>{t(buttons.Modify)}</a>
            <Divider type="vertical" />
            <Link to={`/work/paper_manage/reveiw_expert_allot/detail/double/${index}`}>{t(buttons.View)}</Link>
          </>
        );
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(table.Status),
      code: 'status',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '中方',
          value: '1',
        },
        {
          text: '英方',
          value: '2',
        },
      ],
    },

    {
      label: t(project.ProjectTitle),
      code: 'projectTitle',
      type: 'input',
    },
  ];
  const handleModify = (record: any) => {
    setSelectedData(record);
    setShowModal(true);
  };
  const handleSelectOk = () => {
    setRefresh({ count: refresh.count + 1, isCurrent: true });
    setShowModal(false);
  };
  return (
    <section className={Style.reviewExpertManage}>
      <Notice>
        1、{t(reviewExpertList.Notice1)}；<br />
        2、{t(reviewExpertList.Notice2)}；
      </Notice>
      <div className={Style.timeRemain}>
        <span>{t(reviewExpertList.TimeRemain)}1 天 23 时 41 分 33 秒</span>
        <span>{t(reviewExpertList.UnAssignNumber)}</span>
      </div>
      <TableTemplate
        columns={columns}
        queryAjax={() => Promise.resolve(resultData)}
        rowKey={(record: any) => record.id}
        filterFields={filterFields}
        topButtons={[
          <Popconfirm title={t(reviewExpertList.AllotConfirmMessage)}>
            <Button type="primary">{t(buttons.AllotConfirm)}</Button>
          </Popconfirm>,
          <Popconfirm title={t(reviewExpertList.ReAllotConfirmMessage)}>
            <Button type="primary">{t(buttons.ReAllotConfirm)}</Button>
          </Popconfirm>,
          <Button>{t(buttons.Export)}</Button>,
        ]}
      />
      <ReviewSelectModal visible={showModal} onCancel={() => setShowModal(false)} degreeType={selectedData.degreeType} stuId={selectedData.stuId} onOk={handleSelectOk} />
    </section>
  );
};
export default ReviewExpertManage;
const resultData = {
  code: 200,
  data: {
    list: [
      {
        id: 1,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uog',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 2,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 3,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
    ],
  },
};
