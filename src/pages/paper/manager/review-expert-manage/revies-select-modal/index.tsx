import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { Form, Modal, Select } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

interface IProps {
  degreeType: string;
  stuId: string;
  visible: boolean;
  onCancel: () => void;
  onOk: () => void;
}
const ReviewSelectModal = (props: IProps) => {
  const { degreeType, stuId, onCancel, onOk, visible } = props;
  const { table, student, reviewExpertList, supervisor, commonInfo, roll } = translations;
  const { t } = useTranslation();
  const column = [
    {
      title: t(supervisor.JobNO),
      dataIndex: 'workNumber',
    },
    {
      title: t(supervisor.SupervisorName),
      dataIndex: 'name',
    },
    {
      title: t(supervisor.AcademicTitle),
      dataIndex: 'jobTitleName',
    },
    {
      title: t(supervisor.Type),
      dataIndex: 'typeName',
    },
    {
      title: t(supervisor.University),
      dataIndex: 'schoolName',
    },
    {
      title: t(commonInfo.Department),
      dataIndex: 'deptName',
    },
    {
      title: t(table.TotalFirstSupervisor),
      dataIndex: 'firstTeachCount',
    },
    {
      title: t(table.TotalSecondSupervisor),
      dataIndex: 'secondTeachCount',
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(table.Status),
      code: 'status',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '中方',
          value: '1',
        },
        {
          text: '英方',
          value: '2',
        },
      ],
    },
  ];
  const handleCancel = () => {
    Modal.confirm({
      title: t(reviewExpertList.CancelConfirm),
      onOk: onCancel,
    });
  };
  const handleOk = () => {
    onCancel();
  };
  return (
    <Modal visible={visible} width={900} title={t(reviewExpertList.SelectTeacher)} onCancel={handleCancel} onOk={handleOk}>
      <Form.Item label={t(reviewExpertList.SelectTeacherType)} style={{ borderBottom: '1px solid rgba(0,0,0,0.05)', paddingBottom: 10 }}>
        <Select
          style={{ width: 200 }}
          options={[
            { label: '中方', value: 'zh' },
            { label: '英方', value: 'cn' },
          ]}
        />
      </Form.Item>
      <TableTemplate
        showRowSelect
        modalTable
        columns={column}
        selectType="radio"
        queryAjax={() => Promise.resolve(resultData)}
        rowKey={(record: any) => record.id}
        filterFields={filterFields}
      />
    </Modal>
  );
};

export default ReviewSelectModal;

const resultData = {
  code: 200,
  data: {
    list: [
      {
        id: 1,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uog',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 2,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 3,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
    ],
  },
};
