import PageTitle from '@components/page-title';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

import './index.less';
export default () => {
  const { t } = useTranslation();
  const { expense } = translations;
  const columns = [
    {
      title: t(expense.no),
      dataIndex: 'no',
      render: (text: any, record: any, index: number) => {
        return index + 1;
      },
    },
    {
      title: t(expense.brandName),
      dataIndex: 'stuName',
    },
    {
      title: t(expense.projectName),
      dataIndex: 'UESTCID',
    },
    {
      title: t(expense.num),
      dataIndex: 'UESTCID',
    },
    {
      title: t(expense.unitPrice),
      dataIndex: 'researchField',
    },
    {
      title: t(expense.totalPrice),
      dataIndex: 'source',
    },
    {
      title: t(expense.applyName),
      dataIndex: 'adviser',
    },
    {
      title: t(expense.applyTime),
      dataIndex: 'academy',
    },
    {
      title: t(expense.auditStatus),
      dataIndex: 'academy',
    },
    {
      title: t(expense.operation),
      dataIndex: 'operation',
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(expense.auditStatus),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },

    {
      label: t(expense.brandName),
      code: 'stuName',
      type: 'input',
    },
  ];
  return (
    <section className="expense-management-page">
      <PageTitle title={t(expense.expenseManagement)} />
      <div className="notice">
        <div className="notice-title">{t(expense.notice)}</div>
        <div className="notice-content">1、{t(expense.notice1)}；</div>
        <div className="notice-content">2、{t(expense.notice2)}；</div>
      </div>
      <div className="expert-total">
        <div className="total-unit">
          <div className="text">{t(expense.auditedTotal)}</div>
          <span className="number">800</span>
        </div>
        <div className="total-unit">
          <div className="text">{t(expense.unAuditedTotal)}</div>
          <span className="number">100</span>
        </div>
      </div>
      <TableTemplate
        columns={columns}
        queryAjax={() => Promise.resolve()}
        rowKey={(record: any) => record.id}
        filterFields={filterFields}
      />
    </section>
  );
};
