import Notice from '@components/notice';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { Divider, Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import Style from './index.module.less';
import TeacherModal from './teacher-modal';
export default () => {
  const { t } = useTranslation();
  const { secondSupervisorList, table, project, buttons } = translations;
  const [showModal, setShowModal] = useState(false);
  const [currentTable, setCurrentTable] = useState('p');
  const columns = [
    {
      title: t(table.ProjectName),
      dataIndex: 'stuName',
    },
    {
      title: t(table.ProjectType),
      dataIndex: 'UESTCID',
    },
    {
      title: t(table.SubjectSource),
      dataIndex: 'researchField',
    },
    {
      title: t(project.Programme),
      dataIndex: 'forProfessional',
    },
    {
      title: t(secondSupervisorList.firstAdviser),
      dataIndex: 'adviser',
    },
    {
      title: t(secondSupervisorList.secondAdviser),
      dataIndex: 'adviser',
    },
    {
      title: t(secondSupervisorList.secondAdviserNumber),
      dataIndex: 'specialtyName',
    },
    {
      title: t(secondSupervisorList.secondAdviser),
      dataIndex: 'academy',
    },

    {
      title: t(table.Operation),
      dataIndex: 'questionCount',
      render: (text: any, record: any, index: number) => {
        return (
          <>
            <a onClick={() => setShowModal(true)}>{t(buttons.Distribution)}</a>
            <Divider type="vertical" />
            <Link to={`/work/paper_manage/distb_second_teacher/detail/${index}`}>{t(buttons.View)}</Link>
          </>
        );
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(table.Status),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '中方',
          value: '1',
        },
        {
          text: '英方',
          value: '2',
        },
      ],
    },

    {
      label: t(project.ProjectTitle),
      code: 'teacherName',
      type: 'input',
    },
  ];
  return (
    <section className={Style.allotSecondAdviser}>
      <Notice>
        1、{t(secondSupervisorList.Notice1)}；<br />
        2、{t(secondSupervisorList.Notice2)}；
      </Notice>
      <div className={Style.timeRemain}>
        <span>{t(secondSupervisorList.TimeRemain)}</span>
        <span className={Style.total}>{t(secondSupervisorList.TotalUnAssign)}</span>
      </div>
      <div className={Style.switchTab}>
        <Radio.Group value={currentTable} onChange={(v) => setCurrentTable(v.target.value)}>
          <Radio.Button value="p">{t(secondSupervisorList.UESTICProject)}</Radio.Button>
          <Radio.Button value="s">{t(secondSupervisorList.GUProject)}</Radio.Button>
        </Radio.Group>
      </div>
      <TableTemplate columns={columns} queryAjax={() => Promise.resolve(resultData)} rowKey={(record: any) => record.id} filterFields={filterFields} />
      <TeacherModal visible={showModal} onCancel={() => setShowModal(false)} onOk={() => setShowModal(false)} />
    </section>
  );
};
const resultData = {
  code: 200,
  data: {
    list: [
      {
        stuName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        stuName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        stuName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
    ],
  },
};
