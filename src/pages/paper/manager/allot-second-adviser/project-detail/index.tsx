import React from 'react';
import ProjectInfo from '@components/template/project-info';
import MovingInfo from '@components/template/moving-info';
const ProjectDetail = () => {
  return (
    <section>
      <ProjectInfo/>
      <MovingInfo/>
    </section>
  )
}

export default ProjectDetail;