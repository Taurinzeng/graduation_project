import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { Modal } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

interface IProps {
  visible: boolean;
  onCancel: () => void;
  onOk: () => void;
}
const TeacherModal = (props: IProps) => {
  const { t } = useTranslation();
  const [selectedKeys, setSelectedKeys] = useState<string[]>([]);
  const { visible, onCancel, onOk } = props;
  const { supervisor, supervisorList, table, buttons, commonInfo, secondSupervisorList } = translations;
  const columns = [
    {
      title: t(supervisor.JobNO),
      dataIndex: 'jobNo',
    },
    {
      title: t(supervisor.SupervisorName),
      dataIndex: 'jobNo',
    },
    {
      title: t(supervisor.AcademicTitle),
      dataIndex: 'jobNo',
    },
    {
      title: t(supervisor.University),
      dataIndex: 'jobNo',
    },
    {
      title: t(commonInfo.Department),
      dataIndex: 'jobNo',
    },
    {
      title: t(table.TotalFirstSupervisor),
      dataIndex: 'jobNo',
    },
    {
      title: t(table.TotalSecondSupervisor),
      dataIndex: 'jobNo',
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(supervisor.AcademicTitle),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '中方',
          value: '1',
        },
        {
          text: '英方',
          value: '2',
        },
      ],
    },

    {
      label: t(table.Keyword),
      code: 'teacherName',
      type: 'input',
      placeholder: `${t(supervisor.SupervisorName)}、${t(supervisor.JobNO)}`,
    },
  ];
  const handleOk = () => {};
  const handleSelectChange = (keys: string[]) => {
    setSelectedKeys(keys);
  };
  return (
    <Modal title={t(secondSupervisorList.SelectSupervisorTitle)} visible={visible} width={900} onCancel={onCancel} onOk={handleOk}>
      <TableTemplate
        modalTable
        columns={columns}
        queryAjax={() => Promise.resolve()}
        rowKey={(record: any) => record.id}
        filterFields={filterFields}
        onSelectChange={handleSelectChange}
      />
    </Modal>
  );
};

export default TeacherModal;
