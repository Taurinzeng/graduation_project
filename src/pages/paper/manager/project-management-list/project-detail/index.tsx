import React from 'react';
import ProjectInfo from '@components/template/project-info';
const ProjectDetail = () => {
  return (
    <section>
      <ProjectInfo/>
    </section>
  )
}

export default ProjectDetail;