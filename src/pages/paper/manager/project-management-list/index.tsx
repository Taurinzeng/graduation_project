import CountBar from '@components/count-bar';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { getProjectList } from '@service/paper/project';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Style from './index.module.less';
export default () => {
  const { t } = useTranslation();
  const {
    projectManagementList,
    table,
    project,
    buttons: { View },
    unit,
    other,
  } = translations;
  const columns = [
    {
      title: t(project.ProjectCode),
      dataIndex: 'id',
    },
    {
      title: t(project.SubmitTime),
      dataIndex: 'subjectZhBatch',
    },
    {
      title: t(table.ProjectName),
      dataIndex: 'projectTitle',
    },
    {
      title: t(table.ProjectType),
      dataIndex: 'projectType',
    },
    {
      title: t(table.Creator),
      dataIndex: 'forProfessional',
    },
    {
      title: t(table.Submittime),
      dataIndex: 'submitTime',
    },
    {
      title: t(table.SelectionMethod),
      dataIndex: 'subjTypeZhName',
    },
    {
      title: t(table.Status),
      dataIndex: 'stateName',
    },
    {
      title: t(table.Operation),
      dataIndex: 'operation',
      render: (text: any, record: any, index: number) => {
        return <Link to={`/work/paper_manage/sub_audit_manage/detail/${index}`}>{t(View)}</Link>;
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(table.Type),
      code: 'projectType',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },
    {
      label: t(table.Status),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },
    {
      label: t(table.Keyword),
      code: 'keyword',
      type: 'input',
      placeholder: `${t(other.Enter)}${t(table.Creator)}、${t(table.ProjectName)}`,
    },
  ];
  return (
    <section className={Style.projectList}>
      <CountBar
        items={[
          [
            {
              title: `${t(projectManagementList.TotalSubmitted)}（${t(unit.class)}）`,
              count: 888,
            },
            {
              title: `${t(projectManagementList.TotalApproved)}（${t(unit.class)}）`,
              count: 888,
            },
            {
              title: `${t(projectManagementList.TotalFailed)}（${t(unit.class)}）`,
              count: 888,
            },
            {
              title: `${t(projectManagementList.TotalNotReviewed)}（${t(unit.class)}）`,
              count: 888,
            },
          ],
        ]}
      />
      <div className={Style.timeRemain}>{t(projectManagementList.TimeRemain)}</div>
      <TableTemplate columns={columns} queryAjax={getProjectList} rowKey={(record: any) => record.id} filterFields={filterFields} />
    </section>
  );
};
