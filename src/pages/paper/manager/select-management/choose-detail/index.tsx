import { translations } from '@locales/i18n';
import React from 'react';
import { useTranslation } from 'react-i18next';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';

import Style from './index.module.less';

const ChooseDetail = () => {
  const { t } = useTranslation();
  const { project, table, student, chooseProjectList } = translations;
  const columns = [
    {
      title: t(student.GUID),
      dataIndex: 'stuName',
    },
    {
      title: t(student.name),
      dataIndex: 'UESTCID',
    },
    {
      title: t(student.Degreetype),
      dataIndex: 'UESTCID',
    },
    {
      title: t(table.Submittime),
      dataIndex: 'UESTCID',
    },

    {
      title: t(table.Status),
      dataIndex: 'forProfessional',
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(chooseProjectList.ChooseTime),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },
  ];
  return (
    <section className={Style.chooseDetail}>
      <div className={Style.projectInfo}>
        <span>{t(project.ProjectTitle)}: xxx</span>
        <span>{t(project.ProjectType)}: xxx</span>
        <span>{t(table.Creator)}: xxx</span>
      </div>
      <TableTemplate columns={columns} queryAjax={() => Promise.resolve(resultData)} rowKey={(record: any) => record.id} filterFields={filterFields} />
    </section>
  );
};

export default ChooseDetail;

const resultData = {
  success: true,
  data: {
    content: [
      {
        id: 1,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uog',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 2,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 3,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
    ],
  },
};
