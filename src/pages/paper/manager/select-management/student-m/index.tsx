import CountBar from '@components/count-bar';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Style from './index.module.less';

const StudentM = () => {
  const { t } = useTranslation();
  const {
    chooseProjectList,
    table,
    project,
    buttons: { View },
    unit,
    other,
    student,
    commonInfo,
  } = translations;
  const columns = [
    {
      title: t(student.GUID),
      dataIndex: 'stuName',
    },
    {
      title: t(student.name),
      dataIndex: 'UESTCID',
    },
    {
      title: t(student.Degreetype),
      dataIndex: 'UESTCID',
    },
    {
      title: t(commonInfo.Major),
      dataIndex: 'researchField',
    },
    {
      title: t(commonInfo.Department),
      dataIndex: 'source',
    },
    {
      title: t(table.Status),
      dataIndex: 'forProfessional',
    },
    {
      title: t(project.ProjectTitle),
      dataIndex: 'adviser',
    },
    {
      title: t(chooseProjectList.Supervisor),
      dataIndex: 'academy',
    },
    {
      title: t(chooseProjectList.SupervisorType),
      dataIndex: 'supervisorType',
    },
    {
      title: t(chooseProjectList.ChooseTime),
      dataIndex: 'academy',
    },
    {
      title: t(table.Operation),
      dataIndex: 'operation',
      render: (text: any, record: any, index: number) => {
        return <Link to={`/work/paper_manage/select_sub_manage/choose_detail/${index}`}>{t(View)}</Link>;
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(student.Degreetype),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },
    {
      label: t(table.Status),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },
    {
      label: t(chooseProjectList.ChooseTime),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },
    {
      label: t(table.Keyword),
      code: 'keyword',
      type: 'input',
      placeholder: `${t(other.Enter)}${t(table.ProjectName)}`,
    },
  ];
  return (
    <section className={Style.projectList}>
      <TableTemplate columns={columns} queryAjax={() => Promise.resolve(resultData)} rowKey={(record: any) => record.id} filterFields={filterFields} />
    </section>
  );
};
export default StudentM;
const resultData = {
  success: true,
  data: {
    content: [
      {
        id: 1,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uog',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 2,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 3,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
    ],
  },
};
