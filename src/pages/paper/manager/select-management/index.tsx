import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Notice from '@components/notice';
import CountBar from '@components/count-bar';
import Style from './index.module.less';
import ProjectM from './project-m';
import StudentM from './student-m';

export default () => {
  const { t } = useTranslation();
  const { chooseProjectList, unit } = translations;
  const [currentTable, setCurrentTable] = useState('p');
  return (
    <section className={Style.selectProjectList}>
      <Notice>
        1、{t(chooseProjectList.Notice1)}；<br />
        2、{t(chooseProjectList.Notice2)}；
      </Notice>
      <CountBar
        items={[
          [
            {
              title: `${t(chooseProjectList.TotalProject)}（${t(unit.class)}）`,
              count: 888,
            },
            {
              title: `${t(chooseProjectList.TotalSelected)}（${t(unit.class)}）`,
              count: 888,
            },
            {
              title: `${t(chooseProjectList.TotalUSETCProject)}（${t(unit.class)}）`,
              count: 888,
            },
            {
              title: `${t(chooseProjectList.TotalSingleSuccesses)}（${t(unit.person)}）`,
              count: 888,
            },
            {
              title: `${t(chooseProjectList.TotalSingleFailed)}（${t(unit.person)}）`,
              count: 888,
            },
            {
              title: `${t(chooseProjectList.TotalUSETCRemaining)}（${t(unit.person)}）`,
              count: 888,
            },
          ],
          [
            {
              title: `${t(chooseProjectList.TotalSuccesses)}（${t(unit.class)}）`,
              count: 888,
            },
            {
              title: `${t(chooseProjectList.TotalFailed)}（${t(unit.class)}）`,
              count: 888,
            },
            {
              title: `${t(chooseProjectList.TotalGUProject)}（${t(unit.class)}）`,
              count: 888,
            },
            {
              title: `${t(chooseProjectList.TotalDoubleSuccesses)}（${t(unit.person)}）`,
              count: 888,
            },
            {
              title: `${t(chooseProjectList.TotalDoubleFailed)}（${t(unit.person)}）`,
              count: 888,
            },
            {
              title: `${t(chooseProjectList.TotalGURemaining)}（${t(unit.person)}）`,
              count: 888,
            },
          ],
        ]}
      />
      <div className={Style.switchTab}>
        <Radio.Group value={currentTable} onChange={(v) => setCurrentTable(v.target.value)}>
          <Radio.Button value="p">{t(chooseProjectList.ProjectManagement)}</Radio.Button>
          <Radio.Button value="s">{t(chooseProjectList.StudentManagement)}</Radio.Button>
        </Radio.Group>
      </div>
      {currentTable === 'p' ? <ProjectM /> : <StudentM />}
    </section>
  );
};
