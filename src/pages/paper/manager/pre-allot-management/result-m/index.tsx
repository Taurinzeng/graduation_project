import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { Divider, Button, Popconfirm, Modal, Form, Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import ExportButton from './export-button';

const ResultM = () => {
  const { t } = useTranslation();
  const { table, student, commonInfo, project, buttons, preAssignList } = translations;
  const [showModal, setShowModal] = useState(false);
  const [selectedData, setSelectedData] = useState([] as string[]);
  const columns = [
    {
      title: t(student.GUID),
      dataIndex: 'stuName',
    },
    {
      title: t(student.name),
      dataIndex: 'UESTCID',
    },
    {
      title: t(student.Degreetype),
      dataIndex: 'UESTCID',
    },
    {
      title: t(commonInfo.Major),
      dataIndex: 'researchField',
    },
    {
      title: t(commonInfo.Department),
      dataIndex: 'source',
    },
    {
      title: t(project.ProjectTitle),
      dataIndex: 'adviser',
    },
    {
      title: t(table.Supervisor),
      dataIndex: 'academy',
    },
    {
      title: t(table.SupervisorType),
      dataIndex: 'supervisorType',
    },
    {
      title: t(table.GPAGrade),
      dataIndex: 'academy',
    },
    {
      title: t(table.Status),
      dataIndex: 'academy',
    },
    {
      title: t(table.Operation),
      dataIndex: 'operation',
      render: (text: any, record: any, index: number) => {
        return (
          <>
            <Link to={`/work/paper_manage/pre_allot_manage/detail/${index}`}>{t(buttons.View)}</Link>
            <Divider type="vertical" />
            <a onClick={() => handleAlertClick(record)}>{t(buttons.AlertResult)}</a>
          </>
        );
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(table.Status),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '中方',
          value: '1',
        },
        {
          text: '英方',
          value: '2',
        },
      ],
    },

    {
      label: t(student.name),
      code: 'teacherName',
      type: 'input',
    },
  ];
  const handleAlertClick = (record: any) => {
    setSelectedData(record);
    setShowModal(true);
  };
  return (
    <section>
      <TableTemplate
        columns={columns}
        queryAjax={() => Promise.resolve(resultData)}
        rowKey={(record: any) => record.id}
        filterFields={filterFields}
        topButtons={[
          <Popconfirm title={t(preAssignList.ConfirmMessage)}>
            <Button type="primary">{t(buttons.PreAllotConfirm)}</Button>
          </Popconfirm>,
          <Button>{t(buttons.Import)}</Button>,
          <Button>{t(buttons.Export)}</Button>,
          <Button>{t(buttons.Downloadtemplate)}</Button>,
        ]}
      />
      <Modal title={t(preAssignList.AlertResultM)} visible={showModal} onCancel={() => setShowModal(false)}>
        <Form.Item label={t(preAssignList.AlertLabel)}>
          <Radio.Group>
            <Radio value="1">{t(preAssignList.Pass)}</Radio>
            <Radio value="0">{t(preAssignList.Fail)}</Radio>
          </Radio.Group>
        </Form.Item>
      </Modal>
    </section>
  );
};
export default ResultM;
const resultData = {
  success: true,
  data: {
    content: [
      {
        id: 1,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uog',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 2,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 3,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
    ],
  },
};
