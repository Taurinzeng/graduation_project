import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
export default () => {
  const { t } = useTranslation();
  const {
    project,
    student,
    table,
    commonInfo,
    buttons: { View },
  } = translations;
  const columns = [
    {
      title: t(table.ProjectName),
      dataIndex: 'stuName',
    },
    {
      title: t(project.SubmitTime),
      dataIndex: 'UESTCID',
    },
    {
      title: t(table.InstructorsName),
      dataIndex: 'researchField',
    },
    {
      title: t(table.ProjectType),
      dataIndex: 'forProfessional',
    },
    {
      title: t(student.name),
      dataIndex: 'adviser',
    },
    {
      title: t(commonInfo.Major),
      dataIndex: 'specialtyName',
    },
    {
      title: t(table.Submittime),
      dataIndex: 'academy',
    },

    {
      title: t(table.Status),
      dataIndex: 'questionCount',
    },

    {
      title: t(table.Operation),
      dataIndex: 'operation',
      render: (text: any, record: any, index: number) => {
        return <Link to={`/work/paper_manage/pre_allot_manage/detail/${index}`}>{t(View)}</Link>;
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(project.SubmitTime),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '中方',
          value: '1',
        },
        {
          text: '英方',
          value: '2',
        },
      ],
    },
    {
      label: t(table.Status),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '中方',
          value: '1',
        },
        {
          text: '英方',
          value: '2',
        },
      ],
    },
    {
      label: t(project.ProjectTitle),
      code: 'teacherName',
      type: 'input',
    },
  ];
  return (
    <section>
      <TableTemplate columns={columns} queryAjax={() => Promise.resolve(resultData)} rowKey={(record: any) => record.id} filterFields={filterFields} />
    </section>
  );
};
const resultData = {
  success: true,
  data: {
    content: [
      {
        id: 1,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uog',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 2,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 3,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
    ],
  },
};
