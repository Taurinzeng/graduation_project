import PreAllotApply from '@components/template/pre-allot-apply';
import ProjectInfo from '@components/template/project-info';
import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

const AllotDetail = () => {
  const {preAssignList} = translations;
  const {t} = useTranslation();
  const [currentTab, setCurrentTab] = useState('d');
  return (
    <section>
      <Radio.Group onChange={(v) => setCurrentTab(v.target.value)} value={currentTab} style={{marginTop: 20}}>
        <Radio.Button value="d">{t(preAssignList.ProjectDetail)}</Radio.Button>
        <Radio.Button value="a">{t(preAssignList.PreAllotApply)}</Radio.Button>
      </Radio.Group>
      <div style={{display: currentTab === 'd' ? 'block' : 'none'}}>
        <ProjectInfo/>
      </div>
      <div style={{display: currentTab === 'a' ? 'block' : 'none'}}>
        <PreAllotApply/>
      </div>
    </section>
  )
}

export default AllotDetail;