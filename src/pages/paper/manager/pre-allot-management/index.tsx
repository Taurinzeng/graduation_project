import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Notice from '@components/notice';
import PreAllot from './pre-allot';
import ResultM from './result-m';
import CountBar from '@components/count-bar';
import Style from './index.module.less';

const PreAllotManagement = () => {
  const { t } = useTranslation();
  const { preAssignList, unit } = translations;
  const [currentTable, setCurrentTable] = useState('p');
  return (
    <section className={Style.preAllotManagement}>
      <Notice>
        1、{t(preAssignList.Notice1)}；<br />
        2、{t(preAssignList.Notice2)}；
      </Notice>
      <CountBar
        items={[
          [
            {
              title: `${t(preAssignList.TotalUESTICApplications)}（${t(unit.person)}）`,
              count: 188,
            },
            {
              title: `${t(preAssignList.TotalUESTICSuccesses)}（${t(unit.person)}）`,
              count: 700,
            },
            {
              title: `${t(preAssignList.TotalUESTICFailed)}（${t(unit.person)}）`,
              count: 700,
            },
          ],
          [
            {
              title: `${t(preAssignList.TotalGUApplications)}（${t(unit.person)}）`,
              count: 188,
            },
            {
              title: `${t(preAssignList.TotalGUSuccesses)}（${t(unit.person)}）`,
              count: 700,
            },
            {
              title: `${t(preAssignList.TotalGUFailed)}（${t(unit.person)}）`,
              count: 700,
            },
          ],
        ]}
        total={[
          {
            title: `${t(preAssignList.TotalApplications)}（${t(unit.person)}）`,
            count: 1000,
          },
          {
            title: `${t(preAssignList.TotalAllocable)}（${t(unit.person)}）`,
            count: 1000,
          },
        ]}
      />
      <div className={Style.switchTab}>
        <Radio.Group value={currentTable} onChange={(v) => setCurrentTable(v.target.value)}>
          <Radio.Button value="p">{t(preAssignList.ApplicationManagement)}</Radio.Button>
          <Radio.Button value="s">{t(preAssignList.ResultsManagement)}</Radio.Button>
        </Radio.Group>
      </div>
      {currentTable === 'p' ? <PreAllot /> : <ResultM />}
    </section>
  );
};
export default PreAllotManagement;
