import React from 'react';
import ProjectInfo from '@components/template/project-info';
import Style from './index.module.less';
const ProjectDetail = () => {
  return (
    <section className={Style.teacherProjectDetail}>
      <ProjectInfo />
    </section>
  );
};

export default ProjectDetail;
