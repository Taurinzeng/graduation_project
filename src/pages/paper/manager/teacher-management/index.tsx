import CountBar from '@components/count-bar';
import Notice from '@components/notice';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import ExportButton from './export-button';
import { Link } from 'react-router-dom';
import { Divider, Modal, Form, Input, Radio } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import './index.less';
import { getTeacherTotalInfo, queryTeacherList } from '@service/paper/teacherList';
import { ResponseType } from '@modal/reducer/modal';

export default () => {
  const { t } = useTranslation();
  const {
    supervisor,
    supervisorList,
    commonInfo,
    table,
    unit,
    roll,
    buttons: { View, Edit, Export },
  } = translations;
  const [showModal, setShowModal] = useState(false);
  const [totalInfo, setTotalInfo] = useState<any>({});
  const [form] = useForm();
  const columns: any = [
    {
      title: t(supervisor.SupervisorName),
      dataIndex: 'name',
    },
    {
      title: t(supervisor.JobNO),
      dataIndex: 'workNumber',
    },
    {
      title: t(supervisor.Type),
      dataIndex: 'typeName',
    },
    {
      title: t(commonInfo.Phone),
      dataIndex: 'telephone',
    },
    {
      title: t(supervisor.AcademicTitle),
      dataIndex: 'jobTitleName',
    },
    {
      title: t(supervisor.University),
      dataIndex: 'schoolName',
    },
    {
      title: t(commonInfo.Department),
      dataIndex: 'deptName',
    },

    {
      title: t(supervisorList.TotalProject),
      dataIndex: 'subjCount',
    },
    {
      title: t(table.TotalFirstSupervisor),
      dataIndex: 'firstTeachCount',
    },
    {
      title: t(table.TotalSecondSupervisor),
      dataIndex: 'secondTeachCount',
    },

    {
      title: t(table.Operation),
      dataIndex: 'operation',
      fixed: 'right',
      render: (text: any, record: any) => {
        return (
          <>
            <Link to="/work/paper_manage/teacher_manage/project_list">{t(View)}</Link>
            <Divider type="vertical" />
            <a onClick={() => handleEditClick(record)}>{t(Edit)}</a>
          </>
        );
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(supervisor.Type),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '中方',
          value: '1',
        },
        {
          text: '英方',
          value: '2',
        },
      ],
    },
    {
      label: t(supervisor.GuideType),
      code: 'teachType',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '第一指导教师',
          value: '1',
        },
        {
          text: '第二指导教师',
          value: '2',
        },
      ],
    },
    {
      label: t(supervisor.SupervisorName),
      code: 'teacherName',
      type: 'input',
    },
  ];
  const handleEditClick = (record: any) => {
    setShowModal(true);
    form.setFieldsValue(record);
  };
  const handleSubmit = () => {
    const values = form.getFieldsValue();
    console.log(values);
  };
  useEffect(() => {
    getTeacherTotalInfo().then((res: ResponseType) => {
      setTotalInfo(res.data);
    });
  }, []);
  return (
    <section className="student-management-page">
      <Notice>
        1、{t(supervisorList.Notice1)}；<br />
        2、{t(supervisorList.Notice2)}；
      </Notice>
      <CountBar
        items={[
          [
            {
              title: `${t(supervisorList.TotalNotGUStaff)}（${t(unit.person)}）`,
              count: totalInfo.chineseTeacherNumber,
            },
            {
              title: `${t(supervisorList.TotalCNFirstSupervisor)}（${t(unit.person)}）`,
              count: totalInfo.chineseFirstTeacherNumber,
            },
            {
              title: `${t(supervisorList.TotalCNSecondSupervisor)}（${t(unit.person)}）`,
              count: totalInfo.chineseSecondTeacherNumber,
            },
          ],
          [
            {
              title: `${t(supervisorList.TotalGUStaff)}（${t(unit.person)}）`,
              count: totalInfo.englishTeacherNumber,
            },
            {
              title: `${t(supervisorList.TotalGUFirstSupervisor)}（${t(unit.person)}）`,
              count: totalInfo.englishFirstTeacherNumber,
            },
            {
              title: `${t(supervisorList.TotalGUSecondSupervisor)}（${t(unit.person)}）`,
              count: totalInfo.englishSecondTeacherNumber,
            },
          ],
        ]}
        total={[
          {
            title: `${t(supervisorList.TotalProjectCreator)}（${t(unit.person)}）`,
            count: totalInfo.subjCountTeacherNumber,
          },
        ]}
      />

      <TableTemplate
        columns={columns}
        queryAjax={queryTeacherList}
        rowKey={(record: any) => record.id}
        filterFields={filterFields}
        topButtons={[<ExportButton text={t(Export)} />]}
      />

      <Modal visible={showModal} title={t(supervisorList.UpdateSupervisorType)} onOk={handleSubmit} onCancel={() => setShowModal(false)}>
        <Form form={form}>
          <Form.Item noStyle name="id" />
          <Form.Item label={t(supervisor.SupervisorName)} name="supervisorName">
            <Input />
          </Form.Item>
          <Form.Item label={t(supervisor.SupervisorName)} name="type">
            <Radio.Group>
              <Radio value="uest">{t(roll.UESTCStaff)}</Radio>
              <Radio value="uog">{t(roll.UOGStaff)}</Radio>
            </Radio.Group>
          </Form.Item>
        </Form>
      </Modal>
    </section>
  );
};
