import React from 'react';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import Notice from '@components/notice';
import CountBar from '@components/count-bar';
import { Link } from 'react-router-dom';
import Style from './index.module.less';
import { queryTeacherProject } from '@service/paper/teacherList';
interface IProps {}
const ProjectList = (props: IProps) => {
  const {} = props;
  const { t } = useTranslation();
  const {
    project,
    table,
    createProjectList,
    unit,
    roll,
    supervisor,
    supervisorList,
    buttons: { View, Edit },
  } = translations;
  const columns: any = [
    {
      title: t(project.ProjectTitle),
      dataIndex: 'projectTitle',
    },
    {
      title: t(project.ProjectType),
      dataIndex: 'projectType',
    },
    {
      title: t(project.SelectionType),
      dataIndex: 'subjTypeName',
    },
    {
      title: t(table.Submittime),
      dataIndex: 'submitTime',
    },
    {
      title: t(project.SelectedStudents),
      dataIndex: 'studentName',
    },
    {
      title: t(project.RollType),
      dataIndex: 'teacherRole',
    },
    {
      title: t(project.FlowNode),
      dataIndex: 'stateName',
    },
    {
      title: t(table.Operation),
      dataIndex: 'operation',
      fixed: 'right',
      render: (text: string, record: any, index: number) => {
        return <Link to={`/work/paper_manage/teacher_manage/project_list/detail/${index}`}>{t(View)}</Link>;
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(project.RollType),
      code: 'stuName',
      type: 'input',
    },
  ];

  return (
    <section className={Style.projectList}>
      <Notice>
        1、{t(createProjectList.Notice1)}；<br />
        2、{t(createProjectList.Notice2)}；
      </Notice>
      <CountBar
        items={[
          [
            {
              title: `${t(createProjectList.TotalProject)}（${t(unit.class)}）`,
              count: 188,
            },
            {
              title: `${t(createProjectList.TotalApproved)}（${t(unit.class)}）`,
              count: 700,
            },
            {
              title: `${t(createProjectList.TotalFailed)}（${t(unit.class)}）`,
              count: 700,
            },
            {
              title: `${t(createProjectList.TotalSelected)}（${t(unit.class)}）`,
              count: 700,
            },
          ],
        ]}
      />
      <div className={Style.teacherInfo}>
        <span>{t(supervisor.JobNO)}: sdfsdfsdf</span>
        <span>{t(supervisor.SupervisorName)}: sdfsdfsdf</span>
        <span>{t(supervisor.Type)}: sdfsdfsdf</span>
        <span />
      </div>
      <TableTemplate columns={columns} queryAjax={queryTeacherProject} rowKey={(record: any) => record.id} filterFields={filterFields} />
    </section>
  );
};
export default ProjectList;
