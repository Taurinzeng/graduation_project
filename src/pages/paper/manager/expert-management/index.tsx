import CountBar from '@components/count-bar';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { Divider, Modal, Radio, Form, Input, Popconfirm } from 'antd';
import Notice from '@components/notice';
import React, { useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';
import AddExport from './add-export';
import { useForm } from 'antd/lib/form/Form';
import './index.less';
import { getExpertTotalInfo, queryExpertList } from '@service/paper/expertList';
import { ResponseType } from '@modal/reducer/modal';

export default () => {
  const { t } = useTranslation();
  const { table, expertList, unit, supervisor, commonInfo, createProjectList, buttons, roll } = translations;
  const [currentTab, setCurrentTab] = useState('1');
  const [showModal, setShowModal] = useState(false);
  const [totalInfo, setTotalInfo] = useState<any>({});
  const [form] = useForm();
  const columns = [
    {
      title: t(supervisor.SupervisorName),
      dataIndex: 'name',
    },
    {
      title: t(supervisor.JobNO),
      dataIndex: 'workNumber',
    },
    {
      title: t(supervisor.University),
      dataIndex: 'schoolName',
    },
    {
      title: t(commonInfo.Department),
      dataIndex: 'deptName',
    },
    {
      title: t(supervisor.AcademicTitle),
      dataIndex: 'jobTitleName',
    },
    {
      title: t(supervisor.Type),
      dataIndex: 'typeName',
    },
  ];
  const newColumns: any[] = useMemo(() => {
    switch (currentTab) {
      case '1':
        columns.push({
          title: t(createProjectList.TotalApproved),
          dataIndex: 'stPassCount',
        });
        columns.push({
          title: t(createProjectList.TotalFailed),
          dataIndex: 'stNopassCount',
        });
        break;
      case '2':
        columns.push({
          title: t(expertList.lwpss),
          dataIndex: 'paperPsCount',
        });
        break;
      case '3':
        columns.push({
          title: t(expertList.cqjcs),
          dataIndex: 'preCheckCount',
        });
        columns.push({
          title: t(expertList.zqjcs),
          dataIndex: 'meCheckCount',
        });
        break;
      case '5':
        columns.push({
          title: t(expertList.sjap),
          dataIndex: 'sjap',
        });
        columns.push({
          title: t(expertList.lwdbs),
          dataIndex: 'paperAnswerCount',
        });
        break;
      case '6':
        columns.push({
          title: t(expertList.zclws),
          dataIndex: 'arbitrationPaperCount',
        });
        break;
    }
    return columns;
  }, [currentTab]);
  newColumns.push({
    title: t(table.Operation),
    dataIndex: 'operation',
    fixed: 'right',
    render: (text: any, record: any) => {
      return (
        <>
          <a onClick={() => handleEditClick(record)}>{t(buttons.Edit)}</a>
          <Divider type="vertical" />
          <Popconfirm title={t(expertList.DeleteConfirm)}>
            <a>{t(buttons.Remove)}</a>
          </Popconfirm>
        </>
      );
    },
  });
  const filterFields: FieldType[] = [
    {
      label: t(supervisor.Type),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },
    {
      label: t(supervisor.SupervisorName),
      code: 'stuName',
      type: 'input',
    },
  ];
  const handleEditClick = (record: any) => {
    setShowModal(true);
    form.setFieldsValue(record);
  };
  const handleSubmit = () => {
    const values = form.getFieldsValue();
    console.log(values);
  };
  useEffect(() => {
    getExpertTotalInfo().then((res: ResponseType) => {
      setTotalInfo(res.data);
    });
  }, []);
  return (
    <section className="student-management-page">
      <Notice>
        1、{t(expertList.Notice1)}；<br />
        2、{t(expertList.Notice2)}；
      </Notice>
      <CountBar
        items={[
          [
            {
              title: `${t(expertList.TotalAuditExpert)}（${t(unit.person)}）`,
              count: 700,
            },
            {
              title: `${t(expertList.TotalObserver)}（${t(unit.person)}）`,
              count: 700,
            },
            {
              title: `${t(expertList.TotalProjectReviewExpert)}（${t(unit.person)}）`,
              count: 700,
            },
            {
              title: `${t(expertList.TotalArbitrationExpert)}（${t(unit.person)}）`,
              count: 700,
            },
            {
              title: `${t(expertList.TotalOralPresentationExpert)}（${t(unit.person)}）`,
              count: 700,
            },
            {
              title: `${t(expertList.TotalAuthorizedExperts)}（${t(unit.person)}）`,
              count: 700,
            },
            {
              title: `${t(expertList.TotalGraduationExpert)}（${t(unit.person)}）`,
              count: 700,
            },
            {
              title: `${t(expertList.TotalAdmin)}（${t(unit.person)}）`,
              count: 700,
            },
          ],
        ]}
      />

      <div className="table-part">
        <div className="expert-switch">
          <Radio.Group onChange={(e: any) => setCurrentTab(e.target.value)} value={currentTab}>
            <Radio.Button value="1">{t(expertList.AuditExpert)}</Radio.Button>
            <Radio.Button value="2">{t(expertList.ProjectReviewExpert)}</Radio.Button>
            <Radio.Button value="3">{t(expertList.Observer)}</Radio.Button>
            <Radio.Button value="6">{t(expertList.OralPresentationExpert)}</Radio.Button>
            <Radio.Button value="5">{t(expertList.ArbitrationExpert)}</Radio.Button>
            <Radio.Button value="4">{t(expertList.GraduationExpert)}</Radio.Button>
            <Radio.Button value="7">{t(expertList.Admin)}</Radio.Button>
            <Radio.Button value="8">{t(expertList.AuthorizedExperts)}</Radio.Button>
          </Radio.Group>
        </div>
        <TableTemplate
          initSearchKey={{ type: currentTab }}
          columns={newColumns}
          queryAjax={queryExpertList}
          rowKey={(record: any) => record.id}
          filterFields={filterFields}
          topButtons={[<AddExport expertType={currentTab} />]}
        />
        <Modal visible={showModal} title={t(expertList.SelectSupervisor)} onOk={handleSubmit} onCancel={() => setShowModal(false)}>
          <Form form={form}>
            <Form.Item noStyle name="id" />
            <Form.Item label={t(supervisor.SupervisorName)} name="supervisorName">
              <Input disabled />
            </Form.Item>
            <Form.Item label={t(supervisor.SupervisorName)} name="type">
              <Radio.Group>
                <Radio value="uest">{t(roll.UESTCStaff)}</Radio>
                <Radio value="uog">{t(roll.UOGStaff)}</Radio>
              </Radio.Group>
            </Form.Item>
          </Form>
        </Modal>
      </div>
    </section>
  );
};
