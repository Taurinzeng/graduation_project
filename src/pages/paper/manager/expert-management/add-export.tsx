import { Button, Modal } from 'antd';
import React, { useState } from 'react';
import { PlusCircleFilled } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';

interface IProps {
  expertType: string;
}
export default (props: IProps) => {
  const {} = props;
  const [showModal, setShowModal] = useState(false);
  const { t } = useTranslation();
  const { expertList, supervisor, commonInfo, buttons, table } = translations;
  const columns = [
    {
      title: t(supervisor.JobNO),
      dataIndex: 'workNumber',
    },
    {
      title: t(supervisor.SupervisorName),
      dataIndex: 'name',
    },
    {
      title: t(supervisor.AcademicTitle),
      dataIndex: 'jobTitleName',
    },
    {
      title: t(commonInfo.School),
      dataIndex: 'schoolName',
    },
    {
      title: t(commonInfo.Department),
      dataIndex: 'deptName',
    },
    {
      title: t(supervisor.Type),
      dataIndex: 'typeName',
    },
    {
      title: t(supervisor.ExpertType),
      dataIndex: 'expertType',
    },
    {
      title: t(supervisor.ExpertType),
      dataIndex: 'operation',
      render: () => {
        return <a>{t(buttons.Choose)}</a>;
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(supervisor.AcademicTitle),
      code: 'stuName',
      type: 'select',
    },
    {
      label: t(supervisor.ExpertType),
      code: 'stuName',
      type: 'input',
    },
  ];
  return (
    <>
      <Button type="primary" icon={<PlusCircleFilled />} onClick={() => setShowModal(true)}>
        {t(expertList.AddExpert)}
      </Button>
      <Modal width={900} visible={showModal} title={t(expertList.SelectSupervisor)} onCancel={() => setShowModal(false)}>
        <TableTemplate modalTable columns={columns} queryAjax={() => Promise.resolve()} rowKey={(record: any) => record.id} filterFields={filterFields} />
      </Modal>
    </>
  );
};
