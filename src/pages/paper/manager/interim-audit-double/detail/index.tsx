import InterimReportDouble from '@components/template/interim-report-double';
import InterimSchoolTable from '@components/template/Interim-school-table';
import MovingInfo from '@components/template/moving-info';
import PlanTable from '@components/template/plan-table';
import PreliminaryFirstGradeDouble from '@components/template/preliminary-first-grade-double';
import PreliminaryReport from '@components/template/preliminary-report-double';
import PreliminarySecondGradeDouble from '@components/template/preliminary-second-grade-double';
import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';

interface IProps {}
const AllotDetail = (props: IProps) => {
  const { interimManagementList } = translations;
  const { t } = useTranslation();
  const { type } = useParams<{ type: '1' | '2' | '3' | '4'; id: string }>();
  const [currentTab, setCurrentTab] = useState('report');
  const typeMap = {
    gOne: t(interimManagementList.Tab2),
    gTwo: t(interimManagementList.Tab3),
    d: t(interimManagementList.Tab4),
    s: t(interimManagementList.Tab5),
  };
  return (
    <section className="page-detail-commoncss">
      <Radio.Group onChange={(v) => setCurrentTab(v.target.value)} value={currentTab} style={{ marginTop: 20 }}>
        <Radio.Button value="report">{t(interimManagementList.Tab1)}</Radio.Button>
        {type === '1' ? (
          <Radio.Button value="gOne">{typeMap.gOne}</Radio.Button>
        ) : type === '2' ? (
          <Radio.Button value="gTwo">{typeMap.gTwo}</Radio.Button>
        ) : (
          <Radio.Button value="ds">{type === '3' ? typeMap.d : typeMap.s}</Radio.Button>
        )}
      </Radio.Group>
      <div style={{ display: currentTab === 'report' ? 'block' : 'none' }}>
        <InterimReportDouble />
      </div>
      {type === '1' ? (
        <div style={{ display: currentTab === 'gOne' ? 'block' : 'none' }}>
          <PreliminaryFirstGradeDouble />
        </div>
      ) : type === '2' ? (
        <div style={{ display: currentTab === 'gTwo' ? 'block' : 'none' }}>
          <PreliminarySecondGradeDouble />
        </div>
      ) : (
        <div style={{ display: currentTab === 'ds' ? 'block' : 'none' }}>
          <InterimSchoolTable type={type === '3' ? 'd' : 's'} tableId="" />
        </div>
      )}
      <MovingInfo />
    </section>
  );
};

export default AllotDetail;
