import PageTitle from '@components/page-title';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { Divider, Radio } from 'antd';
import React from 'react';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';

import Style from './index.module.less';
import CountBar from '@components/count-bar';
export default () => {
  const { t } = useTranslation();
  const { table, student, buttons, planAndTaskList, unit } = translations;
  const columns = [
    {
      title: t(table.ProjectName),
      dataIndex: 'stuName',
    },
    {
      title: t(student.GUID),
      dataIndex: 'UESTCID',
    },
    {
      title: t(student.name),
      dataIndex: 'researchField',
    },
    {
      title: t(table.FirstAdviser),
      dataIndex: 'adviser',
    },
    {
      title: t(table.SecondAdviser),
      dataIndex: 'adviser',
    },
    {
      title: t(table.Status),
      dataIndex: 'specialtyName',
    },

    {
      title: t(table.Operation),
      dataIndex: 'questionCount',
      render: (text: any, record: any, index: number) => {
        return (
          <>
            <Link to={`/work/paper_manage/tor_audit/detail/view/${index}`}>{t(buttons.View)}</Link>
            <Divider type="vertical" />
            <Link to={`/work/paper_manage/tor_audit/detail/audit/${index}`}>{t(buttons.Distribution)}</Link>
          </>
        );
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(table.Status),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },

    {
      label: t(student.name),
      code: 'stuName',
      type: 'input',
    },
  ];
  return (
    <section className={Style.taskAndPlan}>
      <CountBar
        items={[
          [
            {
              title: `${t(planAndTaskList.Totaltobesubmitted)}`,
              count: 188,
            },
            {
              title: `${t(planAndTaskList.Totalsubmitted)}`,
              count: 188,
            },
            {
              title: `${t(planAndTaskList.TotalApproved)}`,
              count: 188,
            },
            {
              title: `${t(planAndTaskList.TotalFailed)}`,
              count: 188,
            },
          ],
        ]}
      />
      <div className={Style.timeRemain}>{t(planAndTaskList.TimeRemain)}1 天 23 时 41 分 33 秒</div>
      <TableTemplate columns={columns} queryAjax={() => Promise.resolve(resultData)} rowKey={(record: any) => record.id} filterFields={filterFields} />
    </section>
  );
};
const resultData = {
  code: 200,
  data: {
    list: [
      {
        id: 1,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uog',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 2,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 3,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
    ],
  },
};
