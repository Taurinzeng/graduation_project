import React, { useEffect, useState } from 'react';
import PageTitle from '@components/page-title';
import { translations } from '@locales/i18n';
import { Form, InputNumber, Checkbox, message, Spin } from 'antd';
import { useTranslation } from 'react-i18next';
import InputConfig from './input-config';
import SwitchConfig from './switch-config';
import './index.less';
import { getProjectConfig, saveProjectConfig } from '@service/paper/project';
import { ResponseType } from '@modal/reducer/modal';

export default () => {
  const { t } = useTranslation();
  const [loading, setLoading] = useState(false);
  const [configData, setConfigData] = useState<any>({});
  const { baseSetting, unit } = translations;
  const submitConfig = (v: any, code: string) => {
    setLoading(true);
    saveProjectConfig({ [code]: v })
      .then((res: ResponseType) => {
        setConfigData({ ...configData, [code]: v });
        message.success(res.msg);
      })
      .finally(() => {
        setLoading(false);
      });
  };
  useEffect(() => {
    getProjectConfig().then((res: ResponseType) => {
      setConfigData(res.data || {});
    });
  }, []);
  return (
    <section className="project-setting-page">
      <Spin spinning={loading}>
        <div className="config">
          <SwitchConfig value={configData.subFlag} title={t(baseSetting.S1)} label={t(baseSetting.S1M)} code="subFlag" onChange={submitConfig} />
          <SwitchConfig value={configData.teacherFlag} title={t(baseSetting.S2)} label={t(baseSetting.S2M)} code="teacherFlag" onChange={submitConfig} />
          <InputConfig
            title={t(baseSetting.S3)}
            setLoading={setLoading}
            configData={configData}
            inputData={[
              {
                inputs: [
                  {
                    label: t(baseSetting.S4),
                    inputType: 'number',
                    unit: t(unit.one),
                    code: 'maxSubNum',
                  },
                ],
                message: t(baseSetting.S3M),
              },
            ]}
          />
          <InputConfig
            title={t(baseSetting.S5)}
            configData={configData}
            inputData={[
              {
                inputs: [
                  {
                    label: t(baseSetting.S5L),
                    inputType: 'number',
                    unit: t(unit.one),
                    code: 'maxApplySecondTeacher',
                  },
                ],
                message: t(baseSetting.S5M),
              },
            ]}
          />
          <InputConfig
            title={t(baseSetting.S6)}
            configData={configData}
            inputData={[
              {
                inputs: [
                  {
                    label: t(baseSetting.S7),
                    inputType: 'number',
                    unit: t(unit.one),
                    code: 'selectSubNum',
                  },
                ],
                message: t(baseSetting.S7M),
              },
              {
                title: t(baseSetting.S8),
                inputs: [
                  {
                    label: t(baseSetting.S8L1),
                    inputType: 'number',
                    unit: t(unit.one),
                    code: 'singleDegreeCnNum',
                  },
                  {
                    label: t(baseSetting.S8L2),
                    inputType: 'number',
                    unit: t(unit.one),
                    code: 'singleDegreeEnNum',
                  },
                ],
                message: t(baseSetting.S8M),
              },
              {
                title: t(baseSetting.S10),
                inputs: [
                  {
                    label: t(baseSetting.S8L1),
                    inputType: 'number',
                    unit: t(unit.one),
                    code: 'doubleDegreeCnNum',
                  },
                  {
                    label: t(baseSetting.S8L2),
                    inputType: 'number',
                    unit: t(unit.one),
                    code: 'doubleDegreeEnNum',
                  },
                ],
                message: t(baseSetting.S8M),
              },
            ]}
          />
          <InputConfig
            title={t(baseSetting.S11)}
            configData={configData}
            inputData={[
              {
                inputs: [
                  {
                    label: t(baseSetting.S11L),
                    inputType: 'number',
                    unit: '%',
                    code: 'doubleDegreeCheckRate',
                  },
                ],
                message: t(baseSetting.S11M),
              },
              {
                title: t(baseSetting.S9),
                inputs: [
                  {
                    inputType: 'checkbox',
                    unit: '%',
                    code: 'accordType',
                    options: [
                      { value: '1', label: t(baseSetting.Averagebymajor) },
                      { value: '2', label: t(baseSetting.Averagebysupervisor) },
                      { value: '3', label: t(baseSetting.Averagebyclass) },
                      { value: '4', label: t(baseSetting.Averagebygender) },
                    ],
                  },
                ],
                message: t(baseSetting.S11M),
              },
            ]}
          />
          <InputConfig
            title={t(baseSetting.S14)}
            configData={configData}
            inputData={[
              {
                inputs: [
                  {
                    label: t(baseSetting.S14L),
                    inputType: 'number',
                    unit: '%',
                    code: 'preDistributeRate',
                  },
                ],
                message: t(baseSetting.S14Message),
              },
            ]}
          />

          <InputConfig
            title={t(baseSetting.S15)}
            configData={configData}
            inputData={[
              {
                inputs: [
                  {
                    label: t(baseSetting.S15MessageS),
                    inputType: 'number',
                    unit: '%',
                    code: 'xSingleDegreeCheckRate',
                  },
                ],
              },
              {
                inputs: [
                  {
                    label: t(baseSetting.S15MessageD),
                    inputType: 'number',
                    unit: '%',
                    code: 'xDoubleDegreeCheckRate',
                  },
                ],
                message: t(baseSetting.S15MessageC),
              },
            ]}
          />
          <InputConfig
            title={t(baseSetting.S16)}
            configData={configData}
            inputData={[
              {
                inputs: [
                  {
                    label: t(baseSetting.S16Time),
                    inputType: 'number',
                    unit: t(unit.minute),
                    code: 'answerTime',
                  },
                ],
                message: t(baseSetting.S16M),
              },
            ]}
          />
          <InputConfig
            title={t(baseSetting.S17)}
            configData={configData}
            inputData={[
              {
                inputs: [
                  {
                    label: t(baseSetting.S17D),
                    inputType: 'number',
                    unit: '%',
                    code: 'yGoodPaperRate',
                  },
                ],
              },
              {
                inputs: [
                  {
                    label: t(baseSetting.S17S),
                    inputType: 'number',
                    unit: '%',
                    code: 'xGoodPaperRate',
                  },
                ],
                message: t(baseSetting.S17M),
              },
            ]}
          />
          <InputConfig
            title={t(baseSetting.S18)}
            configData={configData}
            inputData={[
              {
                inputs: [
                  {
                    label: t(baseSetting.S18L),
                    inputType: 'number',
                    unit: t(unit.yuan),
                    code: 'firstTeacherFee',
                  },
                ],
                message: t(baseSetting.S18M),
              },
            ]}
          />
          <InputConfig
            title={t(baseSetting.S19)}
            configData={configData}
            inputData={[
              {
                inputs: [
                  {
                    label: t(baseSetting.S18L),
                    inputType: 'number',
                    unit: t(unit.yuan),
                    code: 'secondTeacherFee',
                  },
                ],
                message: t(baseSetting.FM),
              },
            ]}
          />
          <InputConfig
            title={t(baseSetting.S20)}
            configData={configData}
            inputData={[
              {
                inputs: [
                  {
                    label: t(baseSetting.S20M1),
                    inputType: 'number',
                    unit: t(unit.yuan),
                    code: 'doubleAnswerExpertFee',
                  },
                ],
              },
              {
                inputs: [
                  {
                    label: t(baseSetting.S20M2),
                    inputType: 'number',
                    unit: t(unit.yuan),
                    code: 'singleAnswerExpertFee',
                  },
                ],
                message: t(baseSetting.FM),
              },
            ]}
          />
          <InputConfig
            title={t(baseSetting.S21)}
            configData={configData}
            inputData={[
              {
                inputs: [
                  {
                    label: t(baseSetting.S21L),
                    inputType: 'number',
                    unit: t(unit.yuan),
                    code: 'auditExpertFee',
                  },
                ],
                message: t(baseSetting.FM),
              },
            ]}
          />
          <InputConfig
            title={t(baseSetting.S22)}
            configData={configData}
            inputData={[
              {
                inputs: [
                  {
                    label: t(baseSetting.S22M1),
                    inputType: 'number',
                    unit: t(unit.yuan),
                    code: 'yGoodPaperFee',
                  },
                ],
              },
              {
                inputs: [
                  {
                    label: t(baseSetting.S22M2),
                    inputType: 'number',
                    unit: t(unit.yuan),
                    code: 'xGoodPaperFee',
                  },
                ],
              },
            ]}
          />
          <InputConfig
            title={t(baseSetting.S23)}
            configData={configData}
            inputData={[
              {
                inputs: [
                  {
                    label: t(baseSetting.S23L),
                    inputType: 'number',
                    unit: t(unit.yuan),
                    code: 'subFee',
                  },
                ],
                message: t(baseSetting.S23M),
              },
            ]}
          />
          <InputConfig
            title={t(baseSetting.S24)}
            configData={configData}
            inputData={[
              {
                inputs: [
                  {
                    label: t(baseSetting.S24L1),
                    inputType: 'number',
                    unit: t(unit.yuan),
                    code: 'maxSubAmount',
                  },
                ],
                message: t(baseSetting.S24M1),
              },
              {
                inputs: [
                  {
                    label: t(baseSetting.S24L2),
                    inputType: 'number',
                    unit: t(unit.yuan),
                    code: 'maxSubmitAmount',
                  },
                ],
                message: t(baseSetting.S24M2),
              },
            ]}
          />
        </div>
      </Spin>
    </section>
  );
};
