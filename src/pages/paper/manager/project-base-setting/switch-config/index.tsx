import { Switch } from 'antd';
import React from 'react';
import './index.less';
interface IProps {
  title: string;
  label: string;
  code: string;
  isInner?: boolean;
  onChange: (...args: any[]) => void;
  value: any;
}
export default (props: IProps) => {
  const { title, label, code, isInner, onChange, value } = props;
  const handleChange = (v: any) => {
    onChange && onChange(v, code);
  };
  return (
    <div className={`switch-config ${isInner ? 'inner' : ''}`}>
      <div>
        <div className="config-title">{title}</div>
        <div className="config-label">{label}</div>
      </div>
      <Switch onChange={handleChange} checked={value} />
    </div>
  );
};
