import React, { useEffect, useState } from 'react';
import { Checkbox, Form, InputNumber, message } from 'antd';
import { useTranslation } from 'react-i18next';
import Style from './index.module.less';
import { translations } from '@locales/i18n';
import { useForm } from 'antd/lib/form/Form';
import { saveProjectConfig } from '@service/paper/project';
import { ResponseType } from '@modal/reducer/modal';
interface IProps {
  title: string;
  inputData?: any;
  children?: any;
  configData?: any;
  setLoading?: (...args: any) => void;
}
const InputUnit = (props: { inputUnit: any; setLoading: any; configData: any }) => {
  const { inputUnit, setLoading, configData } = props;
  const { t } = useTranslation();
  const { buttons } = translations;
  const [disabled, setDisabled] = useState(true);
  const [form] = useForm();
  const handleSubmit = () => {
    setLoading && setLoading(true);
    const value = form.getFieldsValue();
    saveProjectConfig({ ...value })
      .then((res: ResponseType) => {
        message.success(res.msg);
        setDisabled(true);
      })
      .finally(() => {
        setLoading && setLoading(false);
      });
  };
  useEffect(() => {
    form.setFieldsValue({ ...configData });
  }, [configData]);
  return (
    <>
      {inputUnit.title}
      <Form form={form}>
        <div className={Style.inputConfigFlex}>
          {inputUnit.inputs.map((unit: any) => {
            return (
              <>
                {unit.label}
                <Form.Item style={{ margin: 0 }} name={unit.code}>
                  {unit.inputType === 'number' ? (
                    <InputNumber style={{ width: 80, margin: '0 10px' }} disabled={disabled} min={0} />
                  ) : (
                    <Checkbox.Group disabled={disabled}>
                      {unit.options?.map((option: any) => (
                        <Checkbox value={option.value} key={option.value}>
                          {option.label}
                        </Checkbox>
                      ))}
                    </Checkbox.Group>
                  )}
                </Form.Item>
                <span className={Style.unit}>{unit.unit}</span>
              </>
            );
          })}

          {disabled ? (
            <a className={Style.editButton} onClick={() => setDisabled(false)}>
              {t(buttons.Edit)}
            </a>
          ) : (
            <>
              <a className={Style.editButton} onClick={() => setDisabled(true)}>
                {t(buttons.Cancel)}
              </a>
              <a className={Style.editButton} onClick={handleSubmit}>
                {t(buttons.Save)}
              </a>
            </>
          )}
        </div>
      </Form>
      <div className={Style.configMessage}>{inputUnit.message}</div>
    </>
  );
};
const InputConfig = (props: IProps) => {
  const { title, children, inputData = [], setLoading, configData = {} } = props;
  const { baseSetting, buttons } = translations;
  const { t } = useTranslation();
  const [disabled, setDisabled] = useState(true);
  return (
    <section className={Style.inputConfig}>
      <div className={Style.inputConfigTitle}>
        <span>{title}</span>
      </div>
      {inputData.map((inputUnit: any, index: number) => {
        return <InputUnit inputUnit={inputUnit} key={index} setLoading={setLoading} configData={configData} />;
      })}
    </section>
  );
};
export default InputConfig;
