import { Button } from 'antd';
import React from 'react';
import { ExportOutlined } from '@ant-design/icons';
interface IProps {
  text: string;
}
export default (props: IProps) => {
  const { text } = props;
  return (
    <>
      <Button icon={<ExportOutlined />}>{text}</Button>
    </>
  );
};
