import CountBar from '@components/count-bar';
import Notice from '@components/notice';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { ResponseType } from '@modal/reducer/modal';
import { getStudentTotalInfo, queryStudentList, removeStudent, updateStudent } from '@service/paper/studentList';
import { Divider, Form, Input, message, Popconfirm, Radio } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import Modal from 'antd/lib/modal/Modal';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import AddStudent from './add-student';
import ExportButton from './export-button';

import './index.less';
export default () => {
  const { t } = useTranslation();
  const {
    studentList,
    student,
    commonInfo,
    table,
    buttons: { Remove, Edit, Export },
    unit,
  } = translations;
  const [totalInfo, setTotalInfo] = useState<any>({});
  const [refresh, setRefresh] = useState<any>({ count: 1 });
  const [currentData, setCurrentData] = useState<any>();
  const [showModal, setShowModal] = useState(false);
  const [form] = useForm();
  const columns: any = [
    {
      title: t(student.name),
      dataIndex: 'name',
    },
    {
      title: t(student.UESTICID),
      dataIndex: 'userId',
    },
    {
      title: t(student.GUID),
      dataIndex: 'guid',
    },
    {
      title: t(student.Degreetype),
      dataIndex: 'degreeType',
    },
    {
      title: t(commonInfo.Phone),
      dataIndex: 'telephone',
    },
    {
      title: t(commonInfo.Email),
      dataIndex: 'email',
    },
    {
      title: t(commonInfo.Department),
      dataIndex: 'deptName',
    },
    {
      title: t(commonInfo.Major),
      dataIndex: 'professionName',
    },
    {
      title: t(table.Operation),
      dataIndex: 'id',
      fixed: 'right',
      render: (id: any, record: any) => {
        return (
          <>
            <a onClick={() => handleEdit(record)}>{t(Edit)}</a>
            <Divider type="vertical" />
            <Popconfirm title={t(studentList.RemoveContent)} onConfirm={() => handleRemove(id)}>
              <a>{t(Remove)}</a>
            </Popconfirm>
          </>
        );
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(student.Degreetype),
      code: 'degreeType',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },
    {
      label: t(commonInfo.Major),
      code: 'profession',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },
    {
      label: t(student.name),
      code: 'name',
      type: 'input',
    },
  ];
  const handleEdit = (data: any) => {
    setShowModal(true);
    form.setFieldsValue({ ...data });
  };
  const handleRemove = (id: string) => {
    removeStudent({ id }).then((res: ResponseType) => {
      message.success(res.msg);
      setRefresh({ count: refresh.count + 1, isCurrent: true });
    });
  };
  const handleOk = () => {
    const value = form.getFieldsValue();
    updateStudent({ ...value }).then((res: ResponseType) => {
      message.success(res.msg);
      setRefresh({ count: refresh.count + 1, isCurrent: true });
      handleCancel();
    });
  };
  const handleCancel = () => {
    setCurrentData({});
    setShowModal(false);
  };
  useEffect(() => {
    getStudentTotalInfo().then((res: ResponseType) => {
      setTotalInfo(res.data);
    });
  }, []);
  return (
    <section className="student-management-page">
      <Notice>
        1、{t(studentList.Notice1)}；<br />
        2、{t(studentList.Notice2)}；
      </Notice>
      <CountBar
        items={[
          [
            {
              title: `${t(studentList.TotalStudent)}（${t(unit.person)}）`,
              count: totalInfo.totalNum,
            },
            {
              title: `${t(studentList.TotalSingleDegree)}（${t(unit.person)}）`,
              count: totalInfo.singleDegreeNum,
            },
            {
              title: `${t(studentList.TotalDoubleDegree)}（${t(unit.person)}）`,
              count: totalInfo.doubleDegreeNum,
            },
          ],
        ]}
      />

      <TableTemplate
        columns={columns}
        queryAjax={queryStudentList}
        refresh={refresh}
        rowKey={(record: any) => record.id}
        filterFields={filterFields}
        topButtons={[<AddStudent onSuccess={() => setRefresh({ count: refresh.count + 1 })} />, <ExportButton text={t(Export)} />]}
      />
      <Modal title={t(studentList.ChangeStudentType)} visible={showModal} onCancel={handleCancel} onOk={handleOk}>
        <Form form={form}>
          <Form.Item noStyle name="id" />
          <Form.Item label={t(student.name)} name="name">
            <Input disabled style={{ width: 200 }} />
          </Form.Item>
          <Form.Item label={t(student.Degreetype)} name="degreeType">
            <Radio.Group>
              <Radio value="1">{t(student.SingleDegree)}</Radio>
              <Radio value="2">{t(student.DoubleDegree)}</Radio>
            </Radio.Group>
          </Form.Item>
        </Form>
      </Modal>
    </section>
  );
};
const resultData = {
  success: true,
  data: {
    content: [
      {
        stuName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        stuName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        stuName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
    ],
  },
};
