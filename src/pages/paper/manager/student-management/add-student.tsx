import { Button, message, Modal } from 'antd';
import React, { createRef, useState } from 'react';
import { PlusCircleFilled } from '@ant-design/icons';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { queryUnSelectedStudentList, selectStudent } from '@service/paper/studentList';
import { ResponseType } from '@modal/reducer/modal';

interface IProps {
  onSuccess?: () => void;
}
export default (props: IProps) => {
  const { onSuccess } = props;
  const [showModal, setShowModal] = useState(false);
  const [selectedKeys, setSelectedKeys] = useState([]);
  const { t } = useTranslation();
  const { student, studentList, commonInfo, other, table } = translations;
  const tableRef: any = createRef();
  const columns = [
    {
      title: t(student.name),
      dataIndex: 'name',
    },
    {
      title: t(student.UESTICID),
      dataIndex: 'userId',
    },
    {
      title: t(student.Academicyear),
      dataIndex: 'year',
    },
    {
      title: t(commonInfo.Major),
      dataIndex: 'professionName',
    },
    {
      title: t(commonInfo.Department),
      dataIndex: 'deptName',
    },
    {
      title: t(student.GUID),
      dataIndex: 'guid',
    },
    {
      title: t(student.Degreetype),
      dataIndex: 'degreeZhName',
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(student.name),
      code: 'stuName',
      type: 'input',
    },
  ];
  const handleOk = () => {
    const keys = tableRef?.current?.getSelectedDataKeys();
    if (keys && keys.length > 0) {
      selectStudent({ keys }).then((res: ResponseType) => {
        message.success(res.msg);
        setShowModal(false);
        setSelectedKeys([]);
        onSuccess && onSuccess();
      });
    } else {
      message.error(t(other.SelectStudent));
    }
  };
  return (
    <>
      <Button type="primary" icon={<PlusCircleFilled />} onClick={() => setShowModal(true)}>
        {t(studentList.AddStu)}
      </Button>
      <Modal
        width={900}
        visible={showModal}
        title={t(studentList.SelectStu)}
        onOk={handleOk}
        onCancel={() => {
          setSelectedKeys([]);
          setShowModal(false);
        }}>
        <TableTemplate
          selectedKeys={selectedKeys}
          modalTable
          columns={columns}
          ref={tableRef}
          queryAjax={queryUnSelectedStudentList}
          rowKey={(record: any) => record.id}
          filterFields={filterFields}
        />
      </Modal>
    </>
  );
};
