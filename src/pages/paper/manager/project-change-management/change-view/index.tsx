import AuditForm from '@components/template/audit-form';
import MovingInfo from '@components/template/moving-info';
import { translations } from '@locales/i18n';
import { Table } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import Style from './index.module.less';

interface IProps {
  modal: 'edit' | 'detail';
  projectId: string;
}
const ChangeView = (props: IProps) => {
  const { modal, projectId } = props;
  const { project, table, projectChangeList } = translations;
  const { t } = useTranslation();
  const columns = [
    {
      title: t(projectChangeList.ChangeField),
      dataIndex: 'field',
    },
    {
      title: t(projectChangeList.ContentBeforeChange),
      dataIndex: 'beforeChange',
    },
    {
      title: t(projectChangeList.ContentAfterChange),
      dataIndex: 'afterChange',
    },
  ];
  return (
    <>
      <section className={Style.changeView}>
        <div className={Style.field}>
          <span className={Style.label}>{t(project.ProjectTitle)}</span>
          <span className={Style.value}>xx</span>
        </div>
        <div className={Style.field}>
          <span className={Style.label}>{t(table.Supervisor)}</span>
          <span className={Style.value}>xx</span>
        </div>
        <div className={Style.field}>
          <span className={Style.label}>{t(project.SelectedStudents)}</span>
          <span className={Style.value}>xx</span>
        </div>
        <div className={Style.field}>
          <span className={Style.label}>{t(projectChangeList.ProjectChangeView)}</span>
        </div>
        <Table columns={columns} pagination={false} />
        <AuditForm />
      </section>
      <MovingInfo />
    </>
  );
};

export default ChangeView;
