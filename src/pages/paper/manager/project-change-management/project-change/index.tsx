import PreAllotApply from '@components/template/pre-allot-apply';
import ProjectInfo from '@components/template/project-info';
import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';
import ChangeView from '../change-view';

const AllotDetail = () => {
  const { projectChangeList } = translations;
  const { t } = useTranslation();
  const [currentTab, setCurrentTab] = useState('1');
  const { type, id } = useParams<{ type: string; id: string }>();
  return (
    <section>
      <Radio.Group
        onChange={(v) => setCurrentTab(v.target.value)}
        value={currentTab}
        style={{ marginTop: 20 }}>
        <Radio.Button value="1">{t(projectChangeList.ChangeTable)}</Radio.Button>
        <Radio.Button value="2">{t(projectChangeList.Originalassignment)}</Radio.Button>
        <Radio.Button value="3">{t(projectChangeList.ChangedProject)}</Radio.Button>
      </Radio.Group>
      <div style={{ display: currentTab === '1' ? 'block' : 'none' }}>
        <ChangeView modal="edit" projectId="11" />
      </div>
      <div style={{ display: currentTab === '2' ? 'block' : 'none' }}>
        <ProjectInfo />
      </div>
      <div style={{ display: currentTab === '3' ? 'block' : 'none' }}>
        <ProjectInfo />
      </div>
    </section>
  );
};

export default AllotDetail;
