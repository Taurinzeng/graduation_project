import PreAllotApply from '@components/template/pre-allot-apply';
import ProjectInfo from '@components/template/project-info';
import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import ChangeView from './../change-view';

const AllotDetail = () => {
  const { projectChangeList } = translations;
  const { t } = useTranslation();
  const [currentTab, setCurrentTab] = useState('p');
  return (
    <section>
      <Radio.Group
        onChange={(v) => setCurrentTab(v.target.value)}
        value={currentTab}
        style={{ marginTop: 20 }}>
        <Radio.Button value="p">{t(projectChangeList.ProjectChangeView)}</Radio.Button>
        <Radio.Button value="b">{t(projectChangeList.BeforeChange)}</Radio.Button>
        <Radio.Button value="a">{t(projectChangeList.AfterChange)}</Radio.Button>
      </Radio.Group>
      <div style={{ display: currentTab === 'p' ? 'block' : 'none' }}>
        <ChangeView modal="edit" projectId="11" />
      </div>
      <div style={{ display: currentTab === 'b' ? 'block' : 'none' }}>
        <ProjectInfo />
      </div>
      <div style={{ display: currentTab === 'a' ? 'block' : 'none' }}>
        <ProjectInfo />
      </div>
    </section>
  );
};

export default AllotDetail;
