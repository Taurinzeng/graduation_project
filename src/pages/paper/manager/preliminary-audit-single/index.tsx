import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import Style from './index.module.less';
export default () => {
  const { t } = useTranslation();
  const {
    inspectionManagementList,
    table,
    student,
    buttons: { View },
  } = translations;
  const [currentTab, setCurrentTab] = useState('1');
  const columns = [
    {
      title: t(table.ProjectName),
      dataIndex: 'stuName',
    },
    {
      title: t(student.GUID),
      dataIndex: 'UESTCID',
    },
    {
      title: t(student.name),
      dataIndex: 'UESTCID',
    },
    {
      title: t(student.Degreetype),
      dataIndex: 'UESTCID',
    },
    {
      title: t(table.FirstAdviser),
      dataIndex: 'researchField',
    },
    {
      title: t(table.Submittime),
      dataIndex: 'adviser',
    },
    {
      title: t(table.GradingTime),
      dataIndex: 'academy',
    },
    { title: t(table.Reviewer), dataIndex: 'reviewer' },
    {
      title: t(table.Status),
      dataIndex: 'academy',
    },
    {
      title: t(table.Operation),
      dataIndex: 'id',
      render: (id: string, record: any, index: number) => {
        return <Link to={`/work/paper_manage/begin_audit_manage_single/detail/${currentTab}/${index}`}>{t(View)}</Link>;
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(table.Status),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },
    {
      label: t(table.ProjectName),
      code: 'stuName',
      type: 'input',
    },
  ];
  return (
    <section className={Style.initialAuditPage}>
      <div className={Style.timeRemain}>{t(inspectionManagementList.TimeRemain)}</div>
      <div className={Style.switchTab}>
        <Radio.Group value={currentTab} onChange={(v) => setCurrentTab(v.target.value)}>
          <Radio.Button value="1">{t(inspectionManagementList.PR)}</Radio.Button>
          <Radio.Button value="3">{t(inspectionManagementList.PRDepartment)}</Radio.Button>
          <Radio.Button value="4">{t(inspectionManagementList.PRSchool)}</Radio.Button>
        </Radio.Group>
      </div>
      <TableTemplate columns={columns} queryAjax={() => Promise.resolve(resultData)} rowKey={(record: any) => record.id} filterFields={filterFields} />
    </section>
  );
};

const resultData = {
  code: 200,
  data: {
    list: [
      {
        id: 1,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uog',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 2,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 3,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
    ],
  },
};
