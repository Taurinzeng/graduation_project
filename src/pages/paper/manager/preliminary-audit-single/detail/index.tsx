import MovingInfo from '@components/template/moving-info';
import PlanTable from '@components/template/plan-table';
import PreliminaryGradeSingle from '@components/template/preliminary-grade-single';
import PreliminaryReport from '@components/template/preliminary-report-single';
import PreliminarySchoolTable from '@components/template/preliminary-school-table';
import Task from '@components/template/task';
import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';
import Style from './index.module.less';
interface IProps {}
const AllotDetail = () => {
  const { inspectionManagementList } = translations;
  const { t } = useTranslation();
  const { type } = useParams<{ type: '1' | '2' | '3' | '4'; id: string }>();
  const [currentTab, setCurrentTab] = useState<'1' | '2' | '3' | '4' | '5'>('1');
  const typeMap = {
    gOne: t(inspectionManagementList.Tab2),
    gTwo: t(inspectionManagementList.Tab3),
    task: t(inspectionManagementList.Tab4),
    plan: t(inspectionManagementList.Tab5),
    d: t(inspectionManagementList.Tab6),
    s: t(inspectionManagementList.Tab7),
  };
  return (
    <section className="page-detail-commoncss">
      <Radio.Group onChange={(v) => setCurrentTab(v.target.value)} value={currentTab} style={{ marginTop: 20 }}>
        <Radio.Button value="1">{t(inspectionManagementList.Tab1)}</Radio.Button>
        {type !== '1' ? (
          <>
            <Radio.Button value="2">{typeMap.task}</Radio.Button>
            <Radio.Button value="3">{typeMap.plan}</Radio.Button>
            <Radio.Button value="4">{type === '3' ? typeMap.d : typeMap.s}</Radio.Button>
          </>
        ) : (
          <Radio.Button value="5">{typeMap.gOne}</Radio.Button>
        )}
      </Radio.Group>
      <div style={{ display: currentTab === '1' ? 'block' : 'none' }}>
        <PreliminaryReport />
      </div>
      {type !== '1' ? (
        <>
          <div style={{ display: currentTab === '2' ? 'block' : 'none' }}>
            <Task />
          </div>
          <div style={{ display: currentTab === '3' ? 'block' : 'none' }}>
            <PlanTable />
          </div>
          <div style={{ display: currentTab === '4' ? 'block' : 'none' }}>
            <PreliminarySchoolTable type={type === '3' ? 'd' : 's'} tableId="" />
          </div>
        </>
      ) : (
        <div style={{ display: currentTab === '5' ? 'block' : 'none' }}>
          <PreliminaryGradeSingle />
        </div>
      )}

      <MovingInfo />
    </section>
  );
};

export default AllotDetail;
