import FooterButtons from '@components/button';
import RichEditor from '@components/rich-editor';
import { translations } from '@locales/i18n';
import { Button, Col, Form, Input, Radio, Row, Select } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import Style from './index.module.less';
const ProjectEdit = () => {
  const { t } = useTranslation();
  const {
    buttons: { Save, Submit },
  } = translations;
  return (
    <>
      <section className={Style.projectEdit}>
        <div className={Style.comment}>
          <div className={Style.title}>审核意见</div>
          <div className={Style.text}>
            完善的监管制度是有效监管的前提。信托业的监管应坚持以政府监管为主导，社会监管以及行业自律为辅助的一套完善监管体系。可以采取以下方式:完善信托立法，加大法律法规的监管力度;对信托产品的流通运行建立完善的流程和机制，促进集中交易市场的建立;发挥行业协会的监督作用，同时建立信用评级制度，加强行业自律;积极推进分类监管评级方案的实施，建立与国际接轨的评级体系完善的监管制度是有效监管的前提。信托业的监管应坚持以政府监管为主导，社会监管以及行业自律为辅助的一套完善监管体系。可以采取以下方式:完善信托立法，加大法律法规的监管力度;对信托产品的流通运行建立完善的流程和机制，促进集中交易市场的建立;发挥行业协会的监督作用，同时建立信用评级
          </div>
        </div>
        <div className={Style.title}>Glasgow College UESTC Final Year Project</div>
        <Form layout="vertical">
          <Form.Item label="Project Title" name="projectTitle" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Row>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Project Type" name="projectType" rules={[{ required: true }]}>
                <Select options={[]} />
              </Form.Item>
            </Col>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Subject Area" name="subjectArea" rules={[{ required: true }]}>
                <Input />
              </Form.Item>
            </Col>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Programme" name="programme" rules={[{ required: true }]}>
                <Input />
              </Form.Item>
            </Col>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Source of topic" name="sourceOfTopic" rules={[{ required: true }]}>
                <Input />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Form of expected results" name="formExpectedResults" rules={[{ required: true }]}>
                <Input />
              </Form.Item>
            </Col>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Supervisor" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Email" name="email" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="University" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
          </Row>

          <Row>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="School" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Academic Title" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Office  Location" name="officeLocation" rules={[{ required: true }]}>
                <Input />
              </Form.Item>
            </Col>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Supervisor’s Webpage" name="webpage" rules={[{ required: true }]}>
                <Input addonBefore="http://" />
              </Form.Item>
            </Col>
          </Row>

          <Form.Item label="Related topics" name="relatedTopics" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item label="Project Description" name="projectDescription" rules={[{ required: true }]}>
            <RichEditor height="200px" />
          </Form.Item>
          <div>Note: If you would like the project to be allocated to a specific student, please fill in the student name</div>
          <Row>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Is it a pre allocation prject" name="subjType" rules={[{ required: true }]}>
                <Select
                  options={[
                    { label: 'YES', value: '1' },
                    { label: 'NO', value: '2' },
                  ]}
                />
              </Form.Item>
            </Col>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="GUID" rules={[{ required: true }]}>
                <Input />
              </Form.Item>
            </Col>
          </Row>

          <Form.Item name="changeType" label="If the pre assignment fails, they are willing to assign the questions to other students" rules={[{ required: true }]}>
            <Radio.Group
              options={[
                { label: 'YES', value: '1' },
                { label: 'NO', value: '0' },
              ]}></Radio.Group>
          </Form.Item>
        </Form>
      </section>
      <FooterButtons
        btns={[
          <Button size={'large'} key={'submit'}>
            {t(Save)}
          </Button>,
          <Button key={'time'} size={'large'} type="primary">
            {t(Submit)}
          </Button>,
        ]}
      />
    </>
  );
};

export default ProjectEdit;
