import CountBar from '@components/count-bar';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { Divider } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Style from './index.module.less';

const StudentM = () => {
  const { t } = useTranslation();
  const {
    projectChangeList,
    table,
    buttons: { View, Approve, Distribution },
  } = translations;
  const columns = [
    {
      title: t(projectChangeList.OriginalProject),
      dataIndex: 'stuName',
    },
    {
      title: t(projectChangeList.CurrentProject),
      dataIndex: 'UESTCID',
    },
    {
      title: t(projectChangeList.OriginalSupervisor),
      dataIndex: 'UESTCID',
    },
    {
      title: t(projectChangeList.CurrentSupervisor),
      dataIndex: 'researchField',
    },
    {
      title: t(table.Submitter),
      dataIndex: 'source',
    },
    {
      title: t(table.Submittime),
      dataIndex: 'forProfessional',
    },
    {
      title: t(table.Status),
      dataIndex: 'adviser',
    },
    {
      title: t(table.Operation),
      dataIndex: 'operation',
      render: (text: any, record: any, index: number) => {
        return (
          <>
            <Link to={`/work/paper_manage_t/project_change_f/teacher_change/view/${index}`}>{t(View)}</Link>
            <Divider type="vertical" />
            <Link to={`/work/paper_manage_t/project_change_f/teacher_change/audit/${index}`}>{t(Approve)}</Link>
          </>
        );
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(table.Status),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },
    {
      label: t(table.Keyword),
      code: 'keyword',
      type: 'input',
      placeholder: `${t(table.Supervisor)}${t(table.ProjectName)}`,
    },
  ];
  return (
    <section className={Style.projectList}>
      <TableTemplate columns={columns} queryAjax={() => Promise.resolve(resultData)} rowKey={(record: any) => record.id} filterFields={filterFields} />
    </section>
  );
};
export default StudentM;
const resultData = {
  code: 200,
  data: {
    list: [
      {
        id: 1,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uog',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 2,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 3,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
    ],
  },
};
