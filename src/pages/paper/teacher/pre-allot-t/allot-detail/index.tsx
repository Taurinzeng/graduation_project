import AuditForm from '@components/template/audit-form';
import MovingInfo from '@components/template/moving-info';
import PreAllotApply from '@components/template/pre-allot-apply';
import ProjectInfo from '@components/template/project-info';
import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';

const AllotDetail = () => {
  const { preAssignList } = translations;
  const { t } = useTranslation();
  const [currentTab, setCurrentTab] = useState('d');
  const { type, id } = useParams<{ type: string; id: string }>();
  return (
    <section className="page-detail-commoncss">
      <Radio.Group onChange={(v) => setCurrentTab(v.target.value)} value={currentTab} style={{ marginTop: 20 }}>
        <Radio.Button value="d">{t(preAssignList.ProjectDetail)}</Radio.Button>
        <Radio.Button value="a">{t(preAssignList.PreAllotApply)}</Radio.Button>
      </Radio.Group>
      <div style={{ display: currentTab === 'd' ? 'block' : 'none' }}>
        <ProjectInfo />
        {type === 'audit' && <AuditForm />}
      </div>
      <div style={{ display: currentTab === 'a' ? 'block' : 'none' }}>
        <PreAllotApply />
      </div>
      <MovingInfo />
    </section>
  );
};

export default AllotDetail;
