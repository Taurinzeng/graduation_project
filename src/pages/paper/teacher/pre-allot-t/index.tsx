import { translations } from '@locales/i18n';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Notice from '@components/notice';
import PreAllot from './pre-allot';
import Style from './index.module.less';

const PreAllotT = () => {
  const { t } = useTranslation();
  const { preAssignList, unit } = translations;
  const [currentTable, setCurrentTable] = useState('p');
  return (
    <section className={Style.preAllotManagement}>
      <Notice>
        1、{t(preAssignList.Notice1)}；<br />
        2、{t(preAssignList.Notice2)}；
      </Notice>
      <PreAllot />
    </section>
  );
};
export default PreAllotT;
