import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { Divider, Modal, Popconfirm, Select } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
export default () => {
  const { t } = useTranslation();
  const {
    project,
    student,
    table,
    projectTList,
    preAssignList: { SubmitConfrim },
    commonInfo,
    buttons: { View, Approve, Edit, SubmitDirect },
  } = translations;
  const [showModal, setShowModal] = useState<boolean>(false);
  const columns = [
    {
      title: t(table.ProjectName),
      dataIndex: 'stuName',
    },
    {
      title: t(project.SubmitTime),
      dataIndex: 'UESTCID',
    },
    {
      title: t(table.InstructorsName),
      dataIndex: 'researchField',
    },
    {
      title: t(table.ProjectType),
      dataIndex: 'forProfessional',
    },
    {
      title: t(student.name),
      dataIndex: 'adviser',
    },
    {
      title: t(commonInfo.Major),
      dataIndex: 'specialtyName',
    },
    {
      title: t(table.Submittime),
      dataIndex: 'academy',
    },

    {
      title: t(table.Status),
      dataIndex: 'questionCount',
    },

    {
      title: t(table.Operation),
      dataIndex: 'id',
      render: (id: any, record: any, index: number) => {
        return (
          <>
            <Link to={`/work/paper_manage_t/pre_allot/view/${index}`}>{t(View)}</Link>
            <Divider type="vertical" />
            <Link to={`/work/paper_manage_t/pre_allot/audit/${index}`}>{t(Approve)}</Link>
            <Divider type="vertical" />
            <a onClick={() => setShowModal(true)}>{t(Edit)}</a>
            <Popconfirm title={t(SubmitConfrim)} onConfirm={() => handleConfrim(id)}>
              <a>{t(SubmitDirect)}</a>
            </Popconfirm>
          </>
        );
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(project.SubmitTime),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '中方',
          value: '1',
        },
        {
          text: '英方',
          value: '2',
        },
      ],
    },
    {
      label: t(table.Status),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '中方',
          value: '1',
        },
        {
          text: '英方',
          value: '2',
        },
      ],
    },
    {
      label: t(project.ProjectTitle),
      code: 'teacherName',
      type: 'input',
    },
  ];
  const handleConfrim = (id: string) => {};
  return (
    <section>
      <TableTemplate columns={columns} queryAjax={() => Promise.resolve(resultData)} rowKey={(record: any) => record.id} filterFields={filterFields} />
      <Modal title={t(projectTList.SetPreAssign)} visible={showModal} onCancel={() => setShowModal(false)}>
        <div style={{ marginBottom: 20 }}>{t(project.ProjectTitle)}: 苹果产品在当前环境下的优势</div>
        <div>{t(projectTList.SelectStudent)}</div>
        <Select onSearch={() => {}} style={{ width: 350 }} />
      </Modal>
    </section>
  );
};
const resultData = {
  code: 200,
  data: {
    list: [
      {
        id: 1,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uog',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 2,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 3,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
    ],
  },
};
