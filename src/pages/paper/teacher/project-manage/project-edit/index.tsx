import FooterButtons from '@components/button';
import RichEditor from '@components/rich-editor';
import { translations } from '@locales/i18n';
import { Button, Col, Form, Input, Radio, Row, Select } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import Style from './index.module.less';
const ProjectEdit = () => {
  const { t } = useTranslation();
  const {
    buttons: { Save, Submit },
  } = translations;
  return (
    <>
      <section className={Style.projectEdit}>
        <div className={Style.comment}>
          <div className={Style.title}>Guidelines for final year project proposals</div>
          <p>
            Staff are recommended to keep the learning outcomes mentioned in the FYP course specifications document while preparing their project proposals.
            <br />
            For the convenience of the staff, the ILOs are copied below: ‘By the end of this course students will be able to:
            <br />
            • work independently and creatively on an engineering topic;
            <br />
            • apply the underpinning science and analysis associated with their chosen project area;
            <br />
            • ensure that all activities take account of health and safety issues;
            <br />
            • use their initiative to resolve problems and uncertainties that arise during the conduct of the project, with limited intervention from the supervisor;
            <br />
            • communicate effectively with the supervisor, without relying on day-to-day direction;
            <br />
            • maintain an accurate record (log book);
            <br />
            • locate background information using all available forms of technical literature;
            <br />
            • write a substantial, well organized technical report in clear English;
            <br />
            • present the outcome of their project orally with appropriate visual aids.’
            <br />
            While most of the ILOs are met with the activities scheduled during the project duration e.g., project planning, log books, health and safety etc., the highlighted ILOs
            are the outcomes that the staff would like to pay attention to, while preparing their project proposals. The proposed descriptions should target the right level of
            knowledge and understanding expected from the undergraduate students. At the same time, the students will have the opportunity to achieve the learning outcomes upon the
            completion of the proposed projects.
          </p>
        </div>
        <div className={Style.title}>Glasgow College UESTC Final Year Project</div>
        <Form layout="vertical">
          <Form.Item label="Project Title" name="projectTitle" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Row>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Project Type" name="projectType" rules={[{ required: true }]}>
                <Select options={[]} />
              </Form.Item>
            </Col>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Subject Area" name="subjectArea" rules={[{ required: true }]}>
                <Input />
              </Form.Item>
            </Col>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Programme" name="programme" rules={[{ required: true }]}>
                <Input />
              </Form.Item>
            </Col>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Source of topic" name="sourceOfTopic" rules={[{ required: true }]}>
                <Input />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Form of expected results" name="formExpectedResults" rules={[{ required: true }]}>
                <Input />
              </Form.Item>
            </Col>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Supervisor" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Email" name="email" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="University" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
          </Row>

          <Row>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="School" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Academic Title" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Office  Location" name="officeLocation" rules={[{ required: true }]}>
                <Input />
              </Form.Item>
            </Col>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Supervisor’s Webpage" name="webpage" rules={[{ required: true }]}>
                <Input addonBefore="http://" />
              </Form.Item>
            </Col>
          </Row>

          <Form.Item label="Related topics" name="relatedTopics" rules={[{ required: true }]}>
            <Input />
          </Form.Item>
          <Form.Item label="Project Description" name="projectDescription" rules={[{ required: true }]}>
            <RichEditor height="200px" />
          </Form.Item>
          <div>Note: If you would like the project to be allocated to a specific student, please fill in the student name</div>
          <Row>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="Is it a pre allocation prject" name="subjType" rules={[{ required: true }]}>
                <Select
                  options={[
                    { label: 'YES', value: '1' },
                    { label: 'NO', value: '2' },
                  ]}
                />
              </Form.Item>
            </Col>
            <Col span={5} className={Style.itemStyle}>
              <Form.Item label="GUID" rules={[{ required: true }]}>
                <Input />
              </Form.Item>
            </Col>
          </Row>

          <Form.Item name="changeType" label="If the pre assignment fails, they are willing to assign the questions to other students" rules={[{ required: true }]}>
            <Radio.Group
              options={[
                { label: 'YES', value: '1' },
                { label: 'NO', value: '0' },
              ]}></Radio.Group>
          </Form.Item>
        </Form>
      </section>
      <FooterButtons
        btns={[
          <Button size={'large'} key={'submit'}>
            {t(Save)}
          </Button>,
          <Button key={'time'} size={'large'} type="primary">
            {t(Submit)}
          </Button>,
        ]}
      />
    </>
  );
};

export default ProjectEdit;
