import CountBar from '@components/count-bar';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { getProjectList } from '@service/paper/project';
import { Button, Divider, Form, Popconfirm, Select, Tooltip } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { InfoCircleOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import Style from './index.module.less';
import Modal from 'antd/lib/modal/Modal';
const ProjectManageT = () => {
  const { t } = useTranslation();
  const {
    projectTList,
    table,
    project,
    buttons: { View, Add, Edit, Submit, Delete, NoAuditSubmit, Copy },
    unit,
    other,
    student,
  } = translations;
  const [currentData, setCurrentData] = useState<any>({});
  const [showModal, setShowModal] = useState<boolean>(false);
  const columns: any = [
    {
      title: t(table.ProjectName),
      dataIndex: 'projectTitle',
    },
    {
      title: t(project.SubmitTime),
      dataIndex: 'subjectZhBatch',
    },

    {
      title: t(table.SelectionMethod),
      dataIndex: 'projectType',
    },
    {
      title: t(student.GUID),
      dataIndex: 'GUID',
    },
    {
      title: t(project.SelectedStudents),
      dataIndex: 'GUID',
    },
    {
      title: t(table.Submittime),
      dataIndex: 'submitTime',
    },
    {
      title: t(table.Status),
      dataIndex: 'stateName',
    },
    {
      title: t(table.Operation),
      dataIndex: 'id',
      fixed: 'right',
      render: (id: string, record: any, index: number) => {
        return (
          <>
            <Link to={`/work/paper_manage_t/project_m/view/${id}`}>{t(View)}</Link>
            <Divider type="vertical" />
            <Link to={`/work/paper_manage_t/project_m/edit/${id}`}>{t(Edit)}</Link>
            <Divider type="vertical" />
            <Popconfirm title={t(projectTList.SubmitConfirm)} onConfirm={handleSubmit}>
              <a>{t(Submit)}</a>
            </Popconfirm>
            <Divider type="vertical" />
            <Popconfirm title={t(projectTList.SubmitConfirm)} onConfirm={handleSubmit}>
              <a>{t(NoAuditSubmit)}</a>
              <Tooltip placement="top" trigger="click" title={t(projectTList.NoAuditComment)}>
                <InfoCircleOutlined style={{ marginLeft: 10, color: 'rgba(0,0,0,0.5)' }} />
              </Tooltip>
            </Popconfirm>
            <Divider type="vertical" />
            <Popconfirm title={t(projectTList.DeleteConfirm)} onConfirm={handleDelete}>
              <a>{t(Delete)}</a>
            </Popconfirm>
            <Divider type="vertical" />
            <Popconfirm title={t(projectTList.DeleteConfirm)} onConfirm={handleDelete}>
              <a>{t(Copy)}</a>
            </Popconfirm>
            <Divider type="vertical" />
            <a onClick={() => handlePreAssign({ id, title: record.projectTitle })}>{t(projectTList.ApplyPreAssign)}</a>
          </>
        );
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(table.Type),
      code: 'projectType',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },
    {
      label: t(table.Status),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },
    {
      label: t(table.Keyword),
      code: 'keyword',
      type: 'input',
      placeholder: `${t(other.Enter)}${t(table.Creator)}、${t(table.ProjectName)}`,
    },
  ];
  const handleSubmit = () => {};
  const handleDelete = () => {};
  const handlePreAssign = (currentProject: any) => {
    setCurrentData(currentProject);
    setShowModal(true);
  };
  return (
    <section className={Style.projectList}>
      <div className={Style.timeRemain}>{t(projectTList.TimeRemain)}</div>
      <TableTemplate
        columns={columns}
        queryAjax={getProjectList}
        rowKey={(record: any) => record.id}
        filterFields={filterFields}
        topButtons={[
          <Button>
            <Link to="/work/paper_manage_t/project_m/create">{t(Add)}</Link>
          </Button>,
        ]}
      />
      <Modal title={t(projectTList.SetPreAssign)} visible={showModal} onCancel={() => setShowModal(false)}>
        <div style={{ marginBottom: 20 }}>{t(project.ProjectTitle)}: 苹果产品在当前环境下的优势</div>
        <div>{t(projectTList.SelectStudent)}</div>
        <Select onSearch={() => {}} style={{ width: 350 }} />
      </Modal>
    </section>
  );
};
export default ProjectManageT;
