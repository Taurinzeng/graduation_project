import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Notice from '@components/notice';
import Style from './index.module.less';
import AvaliableProject from './avaliable-project';
import AppliedProject from './applied-project';

const ApplySecondSupervisor = () => {
  const { t } = useTranslation();
  const { applySecondSupervisor } = translations;
  const [currentTable, setCurrentTable] = useState('p');
  return (
    <section className={Style.applySecondSupervisor}>
      <Notice>
        1、{t(applySecondSupervisor.Notice1)}；<br />
        2、{t(applySecondSupervisor.Notice2)}；
      </Notice>
      <div className={Style.timeRemain}>{t(applySecondSupervisor.TimeRemain)}</div>
      <div className={Style.switchTab}>
        <Radio.Group value={currentTable} onChange={(v) => setCurrentTable(v.target.value)}>
          <Radio.Button value="p">{t(applySecondSupervisor.AvaliableProject)}</Radio.Button>
          <Radio.Button value="s">{t(applySecondSupervisor.AppliedProject)}</Radio.Button>
        </Radio.Group>
      </div>
      {currentTable === 'p' ? <AvaliableProject /> : <AppliedProject />}
    </section>
  );
};
export default ApplySecondSupervisor;
