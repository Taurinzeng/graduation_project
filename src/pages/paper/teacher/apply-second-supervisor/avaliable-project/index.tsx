import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { Divider, Popconfirm } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

const AvaliableProject = () => {
  const { t } = useTranslation();
  const {
    table,
    project,
    applySecondSupervisor,
    student,
    buttons: { Approve, View },
  } = translations;
  const columns = [
    {
      title: t(table.ProjectName),
      dataIndex: 'researchField',
    },
    {
      title: t(project.ProjectType),
      dataIndex: 'UESTCID',
    },
    {
      title: t(student.GUID),
      dataIndex: 'source',
    },
    {
      title: t(student.name),
      dataIndex: 'forProfessional',
    },
    {
      title: t(table.FirstAdviser),
      dataIndex: 'academy',
    },
    {
      title: t(table.ApplyHot),
      dataIndex: 'academy',
    },
    {
      title: t(table.Operation),
      dataIndex: 'operation',
      render: (text: any, record: any, index: number) => {
        return (
          <>
            <Popconfirm title={t(applySecondSupervisor.ApplyConfirm)} onConfirm={handleConfirm}>
              <a>{t(Approve)}</a>
            </Popconfirm>

            <Divider type="vertical" />
            <Link to={`/work/paper_manage_t/apply_second/view/${index}`}>{t(View)}</Link>
          </>
        );
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(table.Status),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },
    {
      label: t(table.Keyword),
      code: 'keyword',
      type: 'input',
      placeholder: `${t(table.Supervisor)}${t(table.ProjectName)}`,
    },
  ];
  const handleConfirm = () => {};
  return (
    <section>
      <TableTemplate columns={columns} queryAjax={() => Promise.resolve(resultData)} rowKey={(record: any) => record.id} filterFields={filterFields} />
    </section>
  );
};
export default AvaliableProject;
const resultData = {
  code: 200,
  data: {
    list: [
      {
        id: 1,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uog',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 2,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 3,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
    ],
  },
};
