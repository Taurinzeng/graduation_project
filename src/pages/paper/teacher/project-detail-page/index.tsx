import AuditForm from '@components/template/audit-form';
import MovingInfo from '@components/template/moving-info';
import ProjectInfo from '@components/template/project-info';
import React, { useState } from 'react';
import { useParams } from 'react-router-dom';
const ProjectDetailPage = () => {
  // type: audit(含有审核表单和流转信息)， view: 只含有题目信息
  const { type, id } = useParams<{ type: string; id: string }>();

  return (
    <section className="page-detail-commoncss">
      <ProjectInfo />
      {type === 'audit' && <AuditForm />}
      {type !== 'view' && <MovingInfo />}
    </section>
  );
};

export default ProjectDetailPage;
