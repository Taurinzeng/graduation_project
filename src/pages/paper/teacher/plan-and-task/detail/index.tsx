import AuditForm from '@components/template/audit-form';
import MovingInfo from '@components/template/moving-info';
import PlanTable from '@components/template/plan-table';
import Task from '@components/template/task';
import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

interface IProps {
  type: 'detail' | 'audit';
}
const AllotDetail = (props: IProps) => {
  const { planAndTaskList } = translations;
  const { t } = useTranslation();
  const { type } = props;
  const [currentTab, setCurrentTab] = useState('1');
  return (
    <section className="page-detail-commoncss">
      <Radio.Group onChange={(v) => setCurrentTab(v.target.value)} value={currentTab} style={{ marginTop: 20 }}>
        <Radio.Button value="1">{t(planAndTaskList.Task)}</Radio.Button>
        <Radio.Button value="2">{t(planAndTaskList.PlanTable)}</Radio.Button>
      </Radio.Group>
      <div style={{ display: currentTab === '1' ? 'block' : 'none' }}>
        <Task />
      </div>
      <div style={{ display: currentTab === '2' ? 'block' : 'none' }}>
        <PlanTable />
      </div>
      <MovingInfo />
    </section>
  );
};

export default AllotDetail;
