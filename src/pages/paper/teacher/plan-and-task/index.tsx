import { translations } from '@locales/i18n';
import { Divider, Popconfirm, Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Notice from '@components/notice';
import Style from './index.module.less';
import { FieldType } from '@components/table-template/filter-form/model';
import { Link } from 'react-router-dom';
import TableTemplate from '@components/table-template';

const PlanAndTask = () => {
  const { t } = useTranslation();
  const {
    table,
    project,
    planAndTaskList,
    student,
    buttons: { Submit, View, Edit, Fill },
  } = translations;
  const columns = [
    {
      title: t(table.ProjectName),
      dataIndex: 'stuName',
    },
    {
      title: t(student.GUID),
      dataIndex: 'UESTCID',
    },
    {
      title: t(student.name),
      dataIndex: 'researchField',
    },
    {
      title: t(table.FirstAdviser),
      dataIndex: 'adviser',
    },
    {
      title: t(table.Submittime),
      dataIndex: 'adviser',
    },
    {
      title: t(table.Status),
      dataIndex: 'specialtyName',
    },
    {
      title: t(table.Operation),
      dataIndex: 'operation',
      render: (text: any, record: any, index: number) => {
        return (
          <>
            <Popconfirm title={t(planAndTaskList.SubmitConfirm)} onConfirm={handleConfirm}>
              <a>{t(Submit)}</a>
            </Popconfirm>
            <Divider type="vertical" />
            <Link to={`/work/paper_manage_t/task_and_plan/view/${index}`}>{t(View)}</Link>
            <Divider type="vertical" />
            <Link to={`/work/paper_manage_t/task_and_plan/Edit/${index}`}>{t(Edit)}</Link>
            <Divider type="vertical" />
            <Link to={`/work/paper_manage_t/task_and_plan/Edit/${index}`}>{t(Fill)}</Link>
          </>
        );
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(table.Status),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },
    {
      label: t(table.Keyword),
      code: 'keyword',
      type: 'input',
      placeholder: `${t(table.Supervisor)}${t(table.ProjectName)}`,
    },
  ];
  const handleConfirm = () => {};
  return (
    <section className={Style.applySecondSupervisor}>
      <div className={Style.timeRemain}>{t(planAndTaskList.TimeRemain)}</div>
      <TableTemplate columns={columns} queryAjax={() => Promise.resolve(resultData)} rowKey={(record: any) => record.id} filterFields={filterFields} />
    </section>
  );
};
export default PlanAndTask;

const resultData = {
  code: 200,
  data: {
    list: [
      {
        id: 1,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uog',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 2,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 3,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
    ],
  },
};
