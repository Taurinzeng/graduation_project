import React, { useState } from 'react';
import FooterButtons from '@components/button';
import Notice from '@components/notice';
import RichEditor from '@components/rich-editor';
import { translations } from '@locales/i18n';
import { Button, Col, Form, Input, Row, Select } from 'antd';
import { useTranslation } from 'react-i18next';
import { EditableProTable } from '@ant-design/pro-table';
import Style from './../index.module.less';

const PlanEdit = () => {
  const {
    planAndTaskList,
    title,
    schoolTitle,
    project,
    student,
    detail,
    supervisor,
    buttons: { Save, Submit },
  } = translations;
  const { t } = useTranslation();
  const [dataSource, setDataSource] = useState<any[]>();

  const columns: any = [
    {
      title: '学期',
      dataIndex: 'decs',
    },
    {
      title: '周次',
      dataIndex: 'decs',
    },
    {
      title: '主要工作内容',
      dataIndex: 'title',
      formItemProps: {
        rules: [
          {
            required: true,
            whitespace: true,
            message: '此项是必填项',
          },
        ],
      },
    },
    {
      title: '完成情况',
      dataIndex: 'title',
      valueType: 'input',
    },
    {
      title: '指导老师确认',
      dataIndex: 'title',
      valueType: 'input',
    },

    {
      title: '备注',
      dataIndex: 'title',
      valueType: 'input',
    },
  ];
  return (
    <section className={Style.planAndTaskEdit}>
      <div className={Style.body}>
        <div>
          1、{t(planAndTaskList.Notice3)}
          <br />
          2、{t(planAndTaskList.Notice4)}
        </div>
        <div className={Style.title}>
          {t(schoolTitle)}2021{t(title.PlanTitle)}
        </div>
        <Form layout="vertical">
          <Row>
            <Col span={6} className={Style.itemStyle}>
              <Form.Item label={t(student.name)} name="projectType" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
            <Col span={6} className={Style.itemStyle}>
              <Form.Item label={t(student.GUID)} name="subjectArea" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
            <Col span={6} className={Style.itemStyle}>
              <Form.Item label={t(detail.Academy)} name="programme" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={12} className={Style.itemStyle}>
              <Form.Item label={t(project.ProjectTitle)} name="projectType" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
          </Row>
        </Form>
        <EditableProTable headerTitle="任务计划表" columns={columns} rowKey="id" value={dataSource} onChange={setDataSource} recordCreatorProps={false} />
      </div>
      <FooterButtons
        btns={[
          <Button size={'large'} key={'submit'}>
            {t(Save)}
          </Button>,
          <Button key={'time'} size={'large'} type="primary">
            {t(Submit)}
          </Button>,
        ]}
      />
    </section>
  );
};

export default PlanEdit;
