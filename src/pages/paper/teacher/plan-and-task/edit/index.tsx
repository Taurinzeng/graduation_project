import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Style from './index.module.less';
import PlanEdit from './plan-edit';
import TaskEdit from './task-edit';
const PlanAndTaskEdit = () => {
  const { planAndTaskList } = translations;
  const { t } = useTranslation();
  const [currentTab, setCurrentTab] = useState('1');
  return (
    <section className={Style.planAndTaskEdit}>
      <Radio.Group onChange={(v) => setCurrentTab(v.target.value)} value={currentTab} style={{ marginTop: 20 }}>
        <Radio.Button value="1">{t(planAndTaskList.Task)}</Radio.Button>
        <Radio.Button value="2">{t(planAndTaskList.PlanTable)}</Radio.Button>
      </Radio.Group>
      <div style={{ display: currentTab === '1' ? 'block' : 'none' }}>
        <TaskEdit />
      </div>
      <div style={{ display: currentTab === '2' ? 'block' : 'none' }}>
        <PlanEdit />
      </div>
    </section>
  );
};

export default PlanAndTaskEdit;
