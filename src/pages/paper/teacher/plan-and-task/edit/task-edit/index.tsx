import FooterButtons from '@components/button';
import Notice from '@components/notice';
import RichEditor from '@components/rich-editor';
import { translations } from '@locales/i18n';
import { Button, Col, Form, Input, Row, Select } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import Style from './../index.module.less';
const TaskEdit = () => {
  const {
    planAndTaskList,
    title,
    schoolTitle,
    project,
    student,
    detail,
    supervisor,
    buttons: { Save, Submit },
  } = translations;
  const { t } = useTranslation();
  return (
    <section className={Style.planAndTaskEdit}>
      <div className={Style.body}>
        <div>
          1、{t(planAndTaskList.Notice1)}
          <br />
          2、{t(planAndTaskList.Notice2)}
        </div>
        <div className={Style.title}>
          {t(schoolTitle)}2021{t(title.TaskTitle)}
        </div>
        <Form layout="vertical">
          <Row>
            <Col span={6} className={Style.itemStyle}>
              <Form.Item label={t(student.name)} name="projectType" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
            <Col span={6} className={Style.itemStyle}>
              <Form.Item label={t(student.GUID)} name="subjectArea" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
            <Col span={6} className={Style.itemStyle}>
              <Form.Item label={t(detail.Academy)} name="programme" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
            <Col span={6} className={Style.itemStyle}>
              <Form.Item label={t(detail.Major)} name="sourceOfTopic" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={12} className={Style.itemStyle}>
              <Form.Item label={t(project.ProjectTitle)} name="formExpectedResults" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
            <Col span={6} className={Style.itemStyle}>
              <Form.Item label={t(project.ProjectFrom)} rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
            <Col span={6} className={Style.itemStyle}>
              <Form.Item label={t(project.ProjectResult)} name="email" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={12} className={Style.itemStyle}>
              <Form.Item label={t(detail.GuidenceUnit)} name="formExpectedResults" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
            <Col span={6} className={Style.itemStyle}>
              <Form.Item label={t(detail.Supervisor)} rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
            <Col span={6} className={Style.itemStyle}>
              <Form.Item label={t(supervisor.AcademicTitle)} name="email" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
          </Row>
          <Row>
            <Col span={12} className={Style.itemStyle}>
              <Form.Item label={t(detail.CreateUnit)} name="formExpectedResults" rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
            <Col span={6} className={Style.itemStyle}>
              <Form.Item label={t(detail.AuditorSign)} rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
          </Row>

          <Row>
            <Col span={6} className={Style.itemStyle}>
              <Form.Item label={t(detail.TimeRange)} rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
            <Col span={6} className={Style.itemStyle}>
              <Form.Item label={t(detail.Position)} rules={[{ required: true }]}>
                <Input disabled />
              </Form.Item>
            </Col>
          </Row>
          <Form.Item label={t(detail.MainTask)} name="projectDescription" rules={[{ required: true }]}>
            <RichEditor height="200px" />
          </Form.Item>
          <Form.Item label={t(detail.ExpectAndGoals)} name="projectDescription" rules={[{ required: true }]}>
            <RichEditor height="200px" />
          </Form.Item>
        </Form>
      </div>
      <FooterButtons
        btns={[
          <Button size={'large'} key={'submit'}>
            {t(Save)}
          </Button>,
          <Button key={'time'} size={'large'} type="primary">
            {t(Submit)}
          </Button>,
        ]}
      />
    </section>
  );
};

export default TaskEdit;
