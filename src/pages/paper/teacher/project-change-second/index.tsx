import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Notice from '@components/notice';
import Style from './index.module.less';
import ProjectM from './project-m';
import SupervisorM from './spuervisor-m';

const PorjectChangeSecond = () => {
  const { t } = useTranslation();
  const { projectChangeList } = translations;
  const [currentTable, setCurrentTable] = useState('p');
  return (
    <section className={Style.selectProjectList}>
      <Notice>
        1、{t(projectChangeList.Notice1)}；<br />
        2、{t(projectChangeList.Notice2)}；
      </Notice>

      <div className={Style.switchTab}>
        <Radio.Group value={currentTable} onChange={(v) => setCurrentTable(v.target.value)}>
          <Radio.Button value="p">{t(projectChangeList.ProjectChange)}</Radio.Button>
          <Radio.Button value="s">{t(projectChangeList.SupervisorChange)}</Radio.Button>
        </Radio.Group>
      </div>
      {currentTable === 'p' ? <ProjectM /> : <SupervisorM />}
    </section>
  );
};
export default PorjectChangeSecond;
