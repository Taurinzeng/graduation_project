import AuditForm from '@components/template/audit-form';
import MovingInfo from '@components/template/moving-info';
import TemplateComponent from '@components/template/template-component';
import { translations } from '@locales/i18n';
import { Col, Row, Table } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import Style from './index.module.less';

interface IProps {
  modal: 'edit' | 'detail';
  projectId: string;
}
const ChangeView = (props: IProps) => {
  const { modal, projectId } = props;
  const { project, table, detail, projectChangeList } = translations;
  const { t } = useTranslation();
  const columns = [
    {
      title: t(projectChangeList.ChangeField),
      dataIndex: 'field',
    },
    {
      title: t(projectChangeList.ContentBeforeChange),
      dataIndex: 'beforeChange',
    },
    {
      title: t(projectChangeList.ContentAfterChange),
      dataIndex: 'afterChange',
    },
  ];
  return (
    <>
      <TemplateComponent>
        <Row className="item">
          <Col className="label" span={3}>
            {t(project.ProjectTitle)}
          </Col>
          <Col className="value">xxx</Col>
        </Row>
        <Row className="item">
          <Col className="label" span={3}>
            {t(detail.Supervisor)}
          </Col>
          <Col className="value">xxx</Col>
        </Row>
        <Row className="item">
          <Col className="label" span={3}>
            {t(project.SelectedStudents)}
          </Col>
          <Col className="value">xxx</Col>
        </Row>
        <Table columns={columns} pagination={false} />
      </TemplateComponent>
      <MovingInfo />
    </>
  );
};

export default ChangeView;
