import PreliminaryGradeSingleEdit from '@components/template/preliminary-grade-single-edit';
import React from 'react';

const DoubleGrade = () => {
  return (
    <section className="page-detail-commoncss">
      <PreliminaryGradeSingleEdit />
    </section>
  );
};

export default DoubleGrade;
