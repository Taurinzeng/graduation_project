import PreliminaryGradeDoubleEdit from '@components/template/preliminary-first-grade-double-edit';
import React from 'react';

const DoubleGrade = () => {
  return (
    <section className="page-detail-commoncss">
      <PreliminaryGradeDoubleEdit />
    </section>
  );
};

export default DoubleGrade;
