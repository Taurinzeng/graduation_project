import AuditForm from '@components/template/audit-form';
import MovingInfo from '@components/template/moving-info';
import PreliminaryReport from '@components/template/preliminary-report-single';
import PreliminarySchoolTable from '@components/template/preliminary-school-table';
import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';

interface IProps {}
const PreliminaryCheckSingle = (props: IProps) => {
  const { inspectionManagementList } = translations;
  const { t } = useTranslation();
  const { type } = useParams<{ type: 'audit' | 'detail'; id: string }>();
  const [currentTab, setCurrentTab] = useState('report');
  const typeMap = {
    gOne: t(inspectionManagementList.Tab2),
    gTwo: t(inspectionManagementList.Tab3),
    task: t(inspectionManagementList.Tab4),
    plan: t(inspectionManagementList.Tab5),
    d: t(inspectionManagementList.Tab6),
    s: t(inspectionManagementList.Tab7),
  };
  return (
    <section className="page-detail-commoncss">
      <Radio.Group onChange={(v) => setCurrentTab(v.target.value)} value={currentTab} style={{ marginTop: 20 }}>
        <Radio.Button value="report">{t(inspectionManagementList.Tab1)}</Radio.Button>
        {type === 'detail' && (
          <>
            <Radio.Button value="d">{typeMap.d}</Radio.Button>
            <Radio.Button value="s">{typeMap.s}</Radio.Button>
          </>
        )}
      </Radio.Group>
      <div style={{ display: currentTab === 'report' ? 'block' : 'none' }}>
        <PreliminaryReport />
      </div>
      {type === 'detail' && (
        <>
          <div style={{ display: currentTab === 'd' ? 'block' : 'none' }}>
            <PreliminarySchoolTable type="d" tableId="" />
          </div>
          <div style={{ display: currentTab === 's' ? 'block' : 'none' }}>
            <PreliminarySchoolTable type="s" tableId="" />
          </div>
        </>
      )}
      {type === 'audit' && <AuditForm />}

      <MovingInfo />
    </section>
  );
};

export default PreliminaryCheckSingle;
