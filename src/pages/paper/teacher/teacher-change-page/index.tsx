/**
 * 教师变更详情页
 */
import AuditForm from '@components/template/audit-form';
import ChangeApplyTable from '@components/template/change-apply-table';
import MovingInfo from '@components/template/moving-info';
import ProjectInfo from '@components/template/project-info';
import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';

const AllotDetail = () => {
  const { projectChangeList } = translations;
  const { type, id } = useParams<{ type: string; id: string }>();
  const { t } = useTranslation();
  const [currentTab, setCurrentTab] = useState('p');
  return (
    <section className="page-detail-commoncss">
      <Radio.Group onChange={(v) => setCurrentTab(v.target.value)} value={currentTab} style={{ marginTop: 20 }}>
        <Radio.Button value="p">{t(projectChangeList.ProjectChangeView)}</Radio.Button>
        <Radio.Button value="b">{t(projectChangeList.BeforeChange)}</Radio.Button>
        <Radio.Button value="a">{t(projectChangeList.AfterChange)}</Radio.Button>
      </Radio.Group>
      <div style={{ display: currentTab === 'p' ? 'block' : 'none' }}>
        <ChangeApplyTable />
        {type === 'audit' && <AuditForm />}
        <MovingInfo />
      </div>
      <div style={{ display: currentTab === 'b' ? 'block' : 'none' }}>
        <ProjectInfo />
      </div>
      <div style={{ display: currentTab === 'a' ? 'block' : 'none' }}>
        <ProjectInfo />
      </div>
    </section>
  );
};

export default AllotDetail;
