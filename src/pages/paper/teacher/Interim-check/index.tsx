import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { Divider, Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

import Style from './index.module.less';
export default () => {
  const { t } = useTranslation();
  const {
    interimManagementList,
    table,
    student,
    buttons: { View, Approve, Appraise },
  } = translations;
  const columns = [
    {
      title: t(table.ProjectName),
      dataIndex: 'stuName',
    },
    {
      title: t(student.GUID),
      dataIndex: 'UESTCID',
    },
    {
      title: t(student.name),
      dataIndex: 'UESTCID',
    },
    {
      title: t(student.Degreetype),
      dataIndex: 'UESTCID',
    },
    {
      title: t(table.FirstAdviser),
      dataIndex: 'researchField',
    },
    {
      title: t(table.SecondAdviser),
      dataIndex: 'source',
    },
    {
      title: t(table.Submittime),
      dataIndex: 'adviser',
    },
    {
      title: t(table.GradingTime),
      dataIndex: 'academy',
    },
    { title: t(table.Reviewer), dataIndex: 'reviewer' },
    {
      title: t(table.Status),
      dataIndex: 'academy',
    },
    {
      title: t(table.Operation),
      dataIndex: 'id',
      render: (id: string, record: any, index: number) => {
        return record.studentType === 'single' ? (
          <>
            <Link to={`/work/paper_manage_t/interim/detail/s/${index}`}>{t(View)}</Link>
            <Divider type="vertical" />
            <Link to={`/work/paper_manage_t/interim/audit/s/${index}`}>{t(Approve)}</Link>
            <Divider type="vertical" />
            <Link to={`/work/paper_manage_t/interim/appraise/single/${index}`}>{t(Appraise)}</Link>
          </>
        ) : (
          <>
            <Link to={`/work/paper_manage_t/interim/detail/d/${index}`}>{t(View)}</Link>
            <Divider type="vertical" />
            <Link to={`/work/paper_manage_t/interim/audit/d/${index}`}>{t(Approve)}</Link>
            <Divider type="vertical" />
            <Link to={`/work/paper_manage_t/interim/appraise/double/${index}`}>{t(Appraise)}</Link>
          </>
        );
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(table.Status),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },
    {
      label: t(table.ProjectName),
      code: 'stuName',
      type: 'input',
    },
  ];
  return (
    <section className={Style.initialAuditPage}>
      <div className={Style.timeRemain}>{t(interimManagementList.TimeRemain)}</div>
      <TableTemplate columns={columns} queryAjax={() => Promise.resolve(resultData)} rowKey={(record: any) => record.id} filterFields={filterFields} />
    </section>
  );
};

const resultData = {
  code: 200,
  data: {
    list: [
      {
        id: 1,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uog',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 2,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
      {
        id: 3,
        supervisorName: '迪丽热巴',
        UESTCID: 1234556,
        GUID: 123456,
        degreeType: '双学位',
        type: 'uest',
        telNumber: 1888888888,
        email: '121@qq.com',
        academy: '计算机学院',
        specialtyName: '软件工程',
      },
    ],
  },
};
