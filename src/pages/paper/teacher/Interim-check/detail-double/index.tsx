import AuditForm from '@components/template/audit-form';
import InterimReportDouble from '@components/template/interim-report-double';
import InterimSchoolTable from '@components/template/Interim-school-table';
import MovingInfo from '@components/template/moving-info';
import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';

interface IProps {}
const PreliminaryCheckSingle = (props: IProps) => {
  const { interimManagementList } = translations;
  const { t } = useTranslation();
  const { type } = useParams<{ type: 'audit' | 'detail'; id: string }>();
  const [currentTab, setCurrentTab] = useState('report');
  const typeMap = {
    gOne: t(interimManagementList.Tab2),
    gTwo: t(interimManagementList.Tab3),
    d: t(interimManagementList.Tab4),
    s: t(interimManagementList.Tab5),
  };
  return (
    <section className="page-detail-commoncss">
      <Radio.Group onChange={(v) => setCurrentTab(v.target.value)} value={currentTab} style={{ marginTop: 20 }}>
        <Radio.Button value="report">{t(interimManagementList.Tab1)}</Radio.Button>
        {type === 'detail' && (
          <>
            <Radio.Button value="d">{typeMap.d}</Radio.Button>
            <Radio.Button value="s">{typeMap.s}</Radio.Button>
          </>
        )}
      </Radio.Group>
      <div style={{ display: currentTab === 'report' ? 'block' : 'none' }}>
        <InterimReportDouble />
      </div>
      {type === 'detail' && (
        <>
          <div style={{ display: currentTab === 'd' ? 'block' : 'none' }}>
            <InterimSchoolTable type="d" tableId="" />
          </div>
          <div style={{ display: currentTab === 's' ? 'block' : 'none' }}>
            <InterimSchoolTable type="s" tableId="" />
          </div>
        </>
      )}
      {type === 'audit' && <AuditForm />}

      <MovingInfo />
    </section>
  );
};

export default PreliminaryCheckSingle;
