import TableTemplate from '@components/table-template';
import { translations } from '@locales/i18n';
import React from 'react';
import { useTranslation } from 'react-i18next';

const ProjectInfo = () => {
  const { t } = useTranslation();
  const { profile } = translations;
  const columns = [
    {
      title: t(profile.no),
    },
    {
      title: t(profile.projectName),
    },
    {
      title: t(profile.role),
    },
    {
      title: t(profile.startTime),
    },
    {
      title: t(profile.endTime),
    },
    {
      title: t(profile.taskLast),
    },
  ];
  return (
    <section>
      <div className="profile_title">{t(profile.participateProjectInfo)}</div>
      <TableTemplate rowKey="id" columns={columns} queryAjax={() => Promise.resolve()} />
    </section>
  );
};

export default ProjectInfo;
