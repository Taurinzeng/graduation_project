import ChangeableInput from '@components/changeable-input';
import { translations } from '@locales/i18n';
import { Col, Row } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import Style from './index.module.less';
interface IProps {
  userInfo: any;
}
const ProfileInfo = (props: IProps) => {
  const { profile } = translations;
  const { userInfo } = props;
  const { t } = useTranslation();
  const colSpan = {
    md: 8,
    lg: 8,
    xl: 6,
  };
  return (
    <section className={Style.profile_info}>
      <div className="profile_title">{t(profile.profileInfo)}</div>
      <Row gutter={24}>
        <Col {...colSpan}>{t(profile.name)}: 王冕</Col>
        {userInfo.userType === 'stu' && (
          <>
            <Col {...colSpan}>{t(profile.gender)}: 男</Col>
            <Col {...colSpan}>{t(profile.UESTICID)}: 1213213</Col>
            <Col {...colSpan}>{t(profile.GUID)}: 1213213</Col>
            <Col {...colSpan}>{t(profile.classes)}: 1213213</Col>
            <Col {...colSpan}>{t(profile.grade)}: 1213213</Col>
            <Col {...colSpan}>{t(profile.stuType)}: 1213213</Col>
          </>
        )}
        {userInfo.userType === 'teacher' && (
          <>
            <Col {...colSpan}>{t(profile.workId)}: 男</Col>
            <Col {...colSpan}>{t(profile.workName)}: 男</Col>
            <Col {...colSpan}>{t(profile.teacherType)}: 男</Col>
            <Col {...colSpan}>{t(profile.IDNumber)}: 男</Col>
            <Col {...colSpan}>{t(profile.bankCardInfo)}: 男</Col>
          </>
        )}
        <Col {...colSpan}>
          {t(profile.tel)}: <ChangeableInput text="1111111" label={t(profile.tel)} />
        </Col>
        <Col {...colSpan}>
          {t(profile.email)}: <ChangeableInput text="1111111" label={t(profile.email)} />
        </Col>
        <Col {...colSpan}>{t(profile.academy)}: 111</Col>
        <Col {...colSpan}>{t(profile.specialty)}: 111</Col>
      </Row>
    </section>
  );
};

export default ProfileInfo;
