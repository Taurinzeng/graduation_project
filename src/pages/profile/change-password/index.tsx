import React from 'react';
import { Button, Form, Input } from 'antd';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import { SaveOutlined } from '@ant-design/icons';
import Style from './index.module.less';

const ChangePassword = () => {
  const { t } = useTranslation();
  const { profile } = translations;
  const confirmPasswordValidator = (p1: any, value: any) => {
    console.log(p1, value);
  };
  return (
    <section className={Style.change_password_page}>
      <div className="profile_title">{t(profile.profileInfo)}</div>
      <Form layout="vertical" labelCol={{ span: 4 }} wrapperCol={{ span: 8 }} className={Style.password_form}>
        <Form.Item name="oldPassword" label={t(profile.oldPassword)} rules={[{ required: true }]}>
          <Input type="password" placeholder={t(profile.oldPasswordHolder)} />
        </Form.Item>
        <Form.Item name="newPassword" label={t(profile.newPassword)} rules={[{ required: true }]}>
          <Input type="password" placeholder={t(profile.newPasswordHolder)} />
        </Form.Item>
        <Form.Item
          name="passwordConfirm"
          dependencies={['newPassword']}
          label={t(profile.passwordConfirm)}
          rules={[
            { required: true },
            ({ getFieldValue }) => ({
              validator(_, value) {
                if (!value || getFieldValue('newPassword') === value) {
                  return Promise.resolve();
                }
                return Promise.reject(new Error('The two passwords that you entered do not match!'));
              },
            }),
          ]}>
          <Input type="password" placeholder={t(profile.confirmPasswordHolder)} />
        </Form.Item>
        <Form.Item>
          <Button type="primary" icon={<SaveOutlined />}>
            {t(profile.confirmSave)}
          </Button>
        </Form.Item>
      </Form>
    </section>
  );
};
export default ChangePassword;
