import { translations } from '@locales/i18n';
import { Tabs } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';

const ProfileInfo = React.lazy(() => import('./profile-info'));
const ChangePassword = React.lazy(() => import('./change-password'));

import Style from './index.module.less';
import MovingInfo from './moving-info';
import ProjectInfo from './project-info';
import UpdateRecord from './update-record';
const ProfileSetting = () => {
  const { t } = useTranslation();
  const profile = translations.profile;
  return (
    <section className={Style.profile_setting}>
      <Tabs tabPosition="left" className={Style.cu_tabs}>
        <Tabs.TabPane tab={t(profile.profileInfo)} key="profileInfo">
          <ProfileInfo userInfo={{ userType: 'stu' }} />
        </Tabs.TabPane>
        <Tabs.TabPane tab={t(profile.changePassword)} key="changePassword">
          <ChangePassword />
        </Tabs.TabPane>
        <Tabs.TabPane tab={t(profile.participateProjectInfo)} key="participateProjectInfo">
          <ProjectInfo />
        </Tabs.TabPane>
        <Tabs.TabPane tab={t(profile.movingInfo)} key="moving">
          <MovingInfo />
        </Tabs.TabPane>
        <Tabs.TabPane tab={t(profile.updateRecord)} key="updateRecord">
          <UpdateRecord />
        </Tabs.TabPane>
      </Tabs>
    </section>
  );
};

export default ProfileSetting;
