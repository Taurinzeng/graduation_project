import TableTemplate from '@components/table-template';
import { translations } from '@locales/i18n';
import React from 'react';
import { useTranslation } from 'react-i18next';

const UpdateRecord = () => {
  const { t } = useTranslation();
  const { profile } = translations;
  const columns = [
    {
      title: t(profile.no),
    },
    {
      title: t(profile.performLink),
    },
    {
      title: t(profile.performer),
    },
    {
      title: t(profile.performTime),
    },
    {
      title: t(profile.performContent),
    },
  ];
  return (
    <section>
      <div className="profile_title">{t(profile.updateRecord)}</div>
      <TableTemplate rowKey="id" columns={columns} queryAjax={() => Promise.resolve()} />
    </section>
  );
};

export default UpdateRecord;
