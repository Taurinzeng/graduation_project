import React from 'react';
import { useParams } from 'react-router';
import StudentInfo from './student-detail';
import TeacherInfo from './teachder-detail';

const UserDetail = () => {
  const { type } = useParams<{ type: string }>();
  return type === 't' ? <TeacherInfo /> : <StudentInfo />;
};

export default UserDetail;
