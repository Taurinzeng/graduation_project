import FooterButtons from '@components/button';
import AuditForm from '@components/template/audit-form';
import MovingInfo from '@components/template/moving-info';
import { translations } from '@locales/i18n';
import { Button, Col } from 'antd';
import Row from 'rc-table/lib/Footer/Row';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { useParams } from 'react-router-dom';
import Style from './index.module.less';
const StudentInfo = () => {
  const { id } = useParams<{ id: string }>();
  const { t } = useTranslation();
  const { operationList, supervisor, commonInfo } = translations;
  const labelSpan = 2;
  const valueSpan = 4;
  return (
    <>
      <section className={Style.registerInfo}>
        <Row className={Style.item}>
          <Col className={Style.label} span={labelSpan}>
            {t(supervisor.SupervisorName)}
          </Col>
          <Col className={Style.value} span={valueSpan}>
            苹果产品在当前环境下的竞争优势分析
          </Col>
          <Col className={Style.label} span={labelSpan}>
            {t(commonInfo.PingYing)}
          </Col>
          <Col className={Style.value} span={valueSpan}>
            苹果产品在当前环境下的竞争优势分析
          </Col>
          <Col className={Style.label} span={labelSpan}>
            {t(supervisor.University)}
          </Col>
          <Col className={Style.value} span={valueSpan}>
            苹果产品在当前环境下的竞争优势分析
          </Col>
        </Row>
        <Row className={Style.item}>
          <Col className={Style.label} span={labelSpan}>
            {t(commonInfo.Department)}
          </Col>
          <Col className={Style.value} span={valueSpan}>
            苹果产品在当前环境下的竞争优势分析
          </Col>
          <Col className={Style.label} span={labelSpan}>
            {t(commonInfo.Phone)}
          </Col>
          <Col className={Style.value} span={valueSpan}>
            苹果产品在当前环境下的竞争优势分析
          </Col>
          <Col className={Style.label} span={labelSpan}>
            {t(commonInfo.Email)}
          </Col>
          <Col className={Style.value} span={valueSpan}>
            苹果产品在当前环境下的竞争优势分析
          </Col>
        </Row>
        <Row className={Style.item}>
          <Col className={Style.label} span={labelSpan}>
            {t(supervisor.AcademicTitle)}
          </Col>
          <Col className={Style.value} span={valueSpan}>
            苹果产品在当前环境下的竞争优势分析
          </Col>
          <Col className={Style.label} span={labelSpan}>
            {t(supervisor.JobNO)}
          </Col>
          <Col className={Style.value} span={valueSpan}>
            苹果产品在当前环境下的竞争优势分析
          </Col>
          <Col className={Style.label} span={labelSpan}>
            {t(supervisor.Type)}
          </Col>
          <Col className={Style.value} span={valueSpan}>
            苹果产品在当前环境下的竞争优势分析
          </Col>
        </Row>
        <Row className={Style.item}>
          <Col className={Style.label} span={labelSpan}>
            {t(supervisor.Bankcard)}
          </Col>
          <Col className={Style.value}>苹果产品在当前环境下的竞争优势分析</Col>
        </Row>
      </section>
      <MovingInfo />
    </>
  );
};

export default StudentInfo;
