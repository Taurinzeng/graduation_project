import CountBar from '@components/count-bar';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { queryUserList } from '@service/system/user';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Style from './index.module.less';

const TeacherM = () => {
  const { t } = useTranslation();
  const {
    table,
    buttons: { View },
    supervisor,
    commonInfo,
  } = translations;
  const columns = [
    {
      title: t(supervisor.JobNO),
      dataIndex: 'stuName',
    },
    {
      title: t(supervisor.SupervisorName),
      dataIndex: 'UESTCID',
    },
    {
      title: t(supervisor.Type),
      dataIndex: 'UESTCID',
    },
    {
      title: t(commonInfo.Phone),
      dataIndex: 'academy',
    },
    {
      title: t(commonInfo.Email),
      dataIndex: 'researchField',
    },
    {
      title: t(supervisor.University),
      dataIndex: 'source',
    },
    {
      title: t(supervisor.AcademicTitle),
      dataIndex: 'forProfessional',
    },
    {
      title: t(table.JoinTime),
      dataIndex: 'adviser',
    },
    {
      title: t(table.Status),
      dataIndex: 'supervisorType',
    },
    {
      title: t(table.Operation),
      dataIndex: 'id',
      render: (id: any, record: any, index: number) => {
        return <Link to={`/work/sys_manage/user/detail/t/${id}`}>{t(View)}</Link>;
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(supervisor.Type),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },
    {
      label: t(supervisor.AcademicTitle),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },
  ];
  return (
    <section className={Style.projectList}>
      <TableTemplate initSearchKey={{ type: '2' }} columns={columns} queryAjax={queryUserList} rowKey={(record: any) => record.id} filterFields={filterFields} />
    </section>
  );
};
export default TeacherM;
