import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import Notice from '@components/notice';
import Style from './index.module.less';
import StudentM from './student-m';
import TeacherM from './teacher-m';

export default () => {
  const { t } = useTranslation();
  const { unit, userList } = translations;
  const [currentTable, setCurrentTable] = useState('t');
  return (
    <section className={Style.selectProjectList}>
      <Notice>
        1、{t(userList.Notice1)}；<br />
        2、{t(userList.Notice2)}；
      </Notice>

      <div className={Style.switchTab}>
        <Radio.Group value={currentTable} onChange={(v) => setCurrentTable(v.target.value)}>
          <Radio.Button value="t">{t(userList.TeachM)}</Radio.Button>
          <Radio.Button value="s">{t(userList.StudentM)}</Radio.Button>
        </Radio.Group>
      </div>
      {currentTable === 't' ? <TeacherM /> : <StudentM />}
    </section>
  );
};
