import CountBar from '@components/count-bar';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { queryUserList } from '@service/system/user';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import Style from './index.module.less';

const StudentM = () => {
  const { t } = useTranslation();
  const {
    table,
    buttons: { View },
    student,
    commonInfo,
  } = translations;
  const columns = [
    {
      title: t(student.GUID),
      dataIndex: 'stuName',
    },
    {
      title: t(student.name),
      dataIndex: 'UESTCID',
    },
    {
      title: t(commonInfo.Gender),
      dataIndex: 'UESTCID',
    },
    {
      title: t(commonInfo.Major),
      dataIndex: 'researchField',
    },
    {
      title: t(commonInfo.Department),
      dataIndex: 'source',
    },
    {
      title: t(student.Class),
      dataIndex: 'forProfessional',
    },
    {
      title: t(student.Grade),
      dataIndex: 'adviser',
    },
    {
      title: t(commonInfo.Phone),
      dataIndex: 'academy',
    },
    {
      title: t(table.Status),
      dataIndex: 'supervisorType',
    },
    {
      title: t(table.Operation),
      dataIndex: 'id',
      render: (id: any) => {
        return <Link to={`/work/sys_manage/user/detail/s/${id}`}>{t(View)}</Link>;
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(commonInfo.Gender),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },
    {
      label: t(student.Class),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '单学位',
          value: '1',
        },
        {
          text: '双学位',
          value: '2',
        },
      ],
    },
  ];
  return (
    <section className={Style.projectList}>
      <TableTemplate initSearchKey={{ type: '2' }} columns={columns} queryAjax={queryUserList} rowKey={(record: any) => record.id} filterFields={filterFields} />
    </section>
  );
};
export default StudentM;
