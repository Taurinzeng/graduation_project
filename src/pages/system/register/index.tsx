import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { queryLoginLog } from '@service/system/operation-log';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';

const RegisterM = () => {
  const { t } = useTranslation();
  const {
    supervisor,
    table,
    commonInfo,
    buttons: { View, Approve },
  } = translations;
  const columns = [
    {
      title: t(supervisor.SupervisorName),
      dataIndex: 'userId',
    },
    {
      title: t(commonInfo.Phone),
      dataIndex: 'name',
    },
    {
      title: t(commonInfo.Email),
      dataIndex: 'ip',
    },
    {
      title: t(supervisor.University),
      dataIndex: 'operateContent',
    },
    {
      title: t(supervisor.AcademicTitle),
      dataIndex: 'loginTime',
    },
    {
      title: t(supervisor.Type),
      dataIndex: 'loginTime',
    },
    {
      title: t(table.RegisterTime),
      dataIndex: 'loginTime',
    },
    {
      title: t(table.Status),
      dataIndex: 'loginTime',
    },
    {
      title: t(table.Operation),
      dataIndex: 'id',
      render: (id: string) => {
        return (
          <>
            <Link to={`/work/sys_manage/register/detail/${id}`}>{t(View)}</Link>
            <Link to={`/work/sys_manage/register/audit/${id}`}>{t(Approve)}</Link>
          </>
        );
      },
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(table.Status),
      code: 'teacherName',
      type: 'dateRange',
    },
    {
      label: t(supervisor.Type),
      code: 'username',
      type: 'input',
    },
  ];
  return (
    <section className="operation-operationList-page">
      <TableTemplate columns={columns} queryAjax={queryLoginLog} rowKey={(record: any) => record.id} filterFields={filterFields} />
    </section>
  );
};
export default RegisterM;
