import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { queryLoginLog } from '@service/system/operation-log';
import React from 'react';
import { useTranslation } from 'react-i18next';

export default () => {
  const { t } = useTranslation();
  const { operationList, table } = translations;
  const columns = [
    {
      title: t(operationList.userId),
      dataIndex: 'userId',
    },
    {
      title: t(operationList.username),
      dataIndex: 'name',
    },
    {
      title: t(operationList.IPAddress),
      dataIndex: 'ip',
    },
    {
      title: t(operationList.operationContent),
      dataIndex: 'operateContent',
    },
    {
      title: t(operationList.loginTime),
      dataIndex: 'loginTime',
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(operationList.loginTime),
      code: 'teacherName',
      type: 'dateRange',
    },
    {
      label: t(operationList.username),
      code: 'username',
      type: 'input',
    },
  ];
  return (
    <section className="operation-operationList-page">
      <TableTemplate columns={columns} queryAjax={queryLoginLog} rowKey={(record: any) => record.id} filterFields={filterFields} />
    </section>
  );
};
