import PageTitle from '@components/page-title';
import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { queryOperationLog } from '@service/system/operation-log';
import { Button } from 'antd';
import React, { useEffect } from 'react';
import { useTranslation } from 'react-i18next';

import './index.less';
const OperationLog = () => {
  const { t } = useTranslation();
  const { operationList, table } = translations;
  const columns = [
    {
      title: t(operationList.userId),
      dataIndex: 'userId',
    },
    {
      title: t(operationList.username),
      dataIndex: 'name',
    },
    {
      title: t(operationList.IPAddress),
      dataIndex: 'ip',
    },
    {
      title: t(operationList.operationModule),
      dataIndex: 'operateModule',
    },
    {
      title: t(operationList.operationContent),
      dataIndex: 'operateContent',
    },
    {
      title: t(operationList.operationTime),
      dataIndex: 'operateTime',
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(operationList.operationModule),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '启用',
          value: '1',
        },
        {
          text: '停用',
          value: '2',
        },
      ],
    },
    {
      label: t(operationList.operationTime),
      code: 'teacherName',
      type: 'dateRange',
    },
    {
      label: t(operationList.username),
      code: 'username',
      type: 'input',
    },
  ];
  return (
    <section className="operation-log-page">
      <TableTemplate columns={columns} queryAjax={queryOperationLog} rowKey={(record: any) => record.id} filterFields={filterFields} />
    </section>
  );
};
export default OperationLog;
