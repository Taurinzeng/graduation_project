import { doLogin } from '@service/profile';
import { Spin } from 'antd';
import React, { useEffect } from 'react';

const LoginTransfer = () => {
  useEffect(() => {
    doLogin({ loginType: 2 }).then(() => {
      location.href = '/home'
    })
  }, []);
  return <Spin spinning />
}

export default LoginTransfer