import { ReducerTypes } from '@modal/store';
import _ from 'lodash';
import { Button, Form, Input, notification } from 'antd';
import { useForm } from 'antd/lib/form/Form';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { login } from '@modal/reducer/profile';
import AccountPanel from '../component/account-panel';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import { Link, useHistory } from 'react-router-dom';
import { LockFilled, UserOutlined } from '@ant-design/icons';
import './index.less';

export const openNoCheck = (history: any, t: any) => {
  const key = `open${Date.now()}`;
  const { buttons, message } = translations;
  const btn = (
    <>
      <Button size="small" onClick={() => notification.close(key)}>
        {t(buttons.Cancel)}
      </Button>
      <Button style={{
        marginLeft: 20,
      }} type="primary" size="small" onClick={() => {
        notification.close(key);
        history.push('/register');
      }}>
        {t(buttons.Update1)}
      </Button>
    </>
  );
  return notification.info({
    message: t(message.LoginTip),
    description:
      t(message.LoginNoCheckingTip),
    btn,
    key,
    onClose: close,
  });
};

export const openChecking = (t: any) => {
  const key = `open${Date.now()}`;
  const { buttons, message } = translations;
  const btn = (
    <Button type="primary" size="small" onClick={() => notification.close(key)}>
      {t(buttons.Iknow)}
    </Button>
  );
  return notification.info({
    message: t(message.LoginTip),
    description:
      t(message.LoginCheckingTip),
    btn,
    key,
    onClose: close,
  });
};

export default () => {
  const { profile } = translations;
  const [form] = useForm();
  const history = useHistory();
  const { submitting } = useSelector((state: ReducerTypes) => state.profile);
  const { t } = useTranslation();
  const dispatch = useDispatch();
  
  const handleFormSubmit = (values: any) => {
    //@ts-ignore
    dispatch(login(values)).then(({ payload }) => {
      if (payload.code === 200) {
        if (_.get(payload, 'data.flowStatus') === 1) {
          openChecking(t);
        } else if (_.get(payload, 'data.flowStatus') === 2) {
          window.location.href = '/work';
        } else {
          openNoCheck(history, t);
        }
      }
    });
  };
  const handleLogin = () => {
    form.validateFields().then((values: any) => {
      handleFormSubmit(values);
    });
  };
  return (
    <section className="login-page">
      <AccountPanel style={{ height: 500, padding: '20px 70px' }}>
        <div className="inner">
          <div className="right">
            <div className="login-title">{t(profile.loginTitle)}</div>
            <Form form={form} className="login-form">
              <Form.Item name={'email'} rules={[{ required: true, message: t(profile.telOrEmailHolder) }]}>
                <Input
                  placeholder={t(profile.telOrEmailHolder)}
                  prefix={<UserOutlined className="input-icon"/>}
                />
              </Form.Item>
              <Form.Item name={'password'} rules={[
                { required: true, message: t(profile.passwordHolder) },
              ]}>
                <Input
                  placeholder={t(profile.passwordHolder)}
                  prefix={<LockFilled className="input-icon"/>}
                />
              </Form.Item>
              <Link className="forget-password" to="/forget_password">
                {t(profile.forgetPassword)}
              </Link>
              <Button type="primary" onClick={handleLogin}>
                {t(profile.login)}
              </Button>
              <p className={'p-link'}>
                <Link to="/register">{t(profile.registerTitle)}</Link>
              </p>
            </Form>
          </div>
        </div>
      </AccountPanel>
    </section>
  );
};
