import React, { ReactElement } from 'react';
import './index.less';
interface IProps {
  children?: ReactElement;
  style?: any;
}
export default (props: IProps) => {
  const { style, children } = props;
  return (
    <div className="account-panel" style={style ? style : {}}>
      {children}
    </div>
  );
};
