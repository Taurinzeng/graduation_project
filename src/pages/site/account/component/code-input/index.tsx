import { Button, Input, message } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import { NumberOutlined } from '@ant-design/icons';
import './index.less';
import { registerSendCode } from '@service/profile';

interface IProps {
  onChange?: (...args: any[]) => void;
  value?: any;
  form: any;
}

export default (props: IProps) => {
  const { onChange, value, form } = props;
  const { t } = useTranslation();
  const [verifyCode, setVerifyCode] = useState(value || '');
  
  const handleChange = (v: any) => {
    setVerifyCode(v.target.value);
    onChange && onChange(v.target.value);
  };
  return (
    <div className="code-input">
      <Input
        value={verifyCode}
        placeholder={t(translations.profile.verificationCode)}
        onChange={handleChange}
        prefix={<NumberOutlined className="input-icon"/>}
      />
      <Button type="primary" onClick={() => {
        
        form.validateFields(['email']).then((values: any) => {
          const { telephone, email } = values;
          message.success('发送成功');
          registerSendCode({ telephone, email }).then((data: any) => {
            //todo
            // setVerifyCode('123456');
          });
        });
      }}>{t(translations.profile.sendCode)}</Button>
    </div>
  );
};
