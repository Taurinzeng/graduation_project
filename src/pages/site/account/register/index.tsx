import React, { useEffect, useState } from 'react';
import { Button, Form, Input, message, Radio, Select, Steps } from 'antd';
import {
  ApartmentOutlined,
  CheckCircleOutlined,
  LockFilled,
  MailOutlined,
  PhoneOutlined,
  UserOutlined,
} from '@ant-design/icons';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import CodeInput from '../component/code-input';
import AccountPanel from '../component/account-panel';
import './index.less';
import { Link, useHistory } from 'react-router-dom';
import { IconfontWidgetOperate } from '@components/iconfont';
import { codeQuery } from '@service/common';
import { TYPE_CODE } from '@modal/reducer/modal';
import { useForm } from 'antd/lib/form/Form';
import { doRegister, heckPhoneOrEmail } from '@service/profile';

const { Item } = Form;
export default () => {
  const { t } = useTranslation();
  const [form] = useForm();
  const history = useHistory();
  const { profile } = translations;
  const [currentStep, setCurrentStep] = useState(1);
  const [isHaveEmail, setHaveEmil] = useState(false);
  const [isHaveTel, setHaveTel] = useState(false);
  const [jobList, setJobList] = useState([]);
  const handleToNext = (nextStep: number) => {
    
    form.validateFields().then((values: any) => {
      doRegister(values).then(() => {
        setCurrentStep(nextStep);
      });
    });
    
  };
  useEffect(() => {
    codeQuery({ type: TYPE_CODE.JOB }).then((res: any) => {
      setJobList(res.data || []);
    });
  }, []);
  return (
    <section className="register-page">
      <AccountPanel>
        <>
          <div className="title">{t(profile.registerTitle)}</div>
          <Steps labelPlacement="vertical">
            <Steps.Step
              status={currentStep > 1 ? 'finish' : 'process'}
              title={<span className="step-title">{t(profile.fillInfo)}</span>}
            />
            <Steps.Step
              status={currentStep > 2 ? 'finish' : currentStep === 2 ? 'process' : 'wait'}
              title={<span className="step-title">{t(profile.setPassword)}</span>}
            />
            <Steps.Step
              status={currentStep === 3 ? 'process' : 'wait'}
              title={<span className="step-title">{t(profile.submitApply)}</span>}
            />
          </Steps>
          <Form form={form} className="custom-form">
            {currentStep === 1 && (
              <>
                <Item name="email" rules={[
                  { type: 'email', message: t(profile.emailValidHolder) },
                  { required: true, message: t(profile.emailHolder) },
                ]}>
                  <Input
                    onBlur={() => {
                      form.validateFields(['email']).then((values: any) => {
                        heckPhoneOrEmail({ email: form.getFieldValue('email') }).then((res: any) => {
                          if (res.code === 500) {
                            message.error(t(profile.emailHave));
                            setHaveEmil(true);
                          } else if (res.code === 200) {
                            setHaveEmil(false);
                          }
                        });
                      });
                    }}
                    placeholder={t(profile.emailHolder)}
                    prefix={<MailOutlined className="input-icon"/>}
                  />
                </Item>
                <Item name="telephone">
                  <Input
                    onBlur={() => {
                      if (form.getFieldValue('telephone')) {
                        heckPhoneOrEmail({ telephone: form.getFieldValue('telephone') }).then((res: any) => {
                          if (res.code === 500) {
                            message.error(t(profile.telHave));
                            setHaveTel(true);
                          } else if (res.code === 200) {
                            setHaveEmil(false);
                          }
                        });
                      }
                    }}
                    placeholder={t(profile.telHolder)}
                    prefix={<PhoneOutlined className="input-icon"/>}
                  />
                </Item>
                <Item name="verifyCode">
                  <CodeInput form={form} onChange={(value: any) => {
                    form.setFieldsValue({
                      'verifyCode': value,
                    });
                  }}/>
                </Item>
                <Item name="name" rules={[{ required: true, message: t(profile.nameHolder) }]}>
                  <Input
                    placeholder={t(profile.nameHolder)}
                    prefix={<UserOutlined className="input-icon"/>}
                  />
                </Item>
                <Item>
                  <Input
                    placeholder={t(profile.namePingHolder)}
                    prefix={<UserOutlined className="input-icon"/>}
                  />
                </Item>
                {/*<Item>*/}
                {/*  <Input*/}
                {/*    placeholder={t(profile.idNoHolder)}*/}
                {/*    prefix={<IdcardOutlined className="input-icon" />}*/}
                {/*  />*/}
                {/*</Item>*/}
                <Item name="schoolName" rules={[{ required: true, message: t(profile.unitName) }]}>
                  <Input
                    placeholder={t(profile.unitName)}
                    prefix={<ApartmentOutlined className="input-icon"/>}
                  />
                </Item>
                <Item name="jobTitle"
                  // rules={[{ required: true, message: t(profile.positionHolder) }]}
                >
                  <>
                    <IconfontWidgetOperate className="input-icon select-icon" type={'icon-position'}/>
                    <Select placeholder={t(profile.positionHolder)} options={jobList}/>
                  </>
                </Item>
                <Item name={'type'}>
                  <Radio.Group>
                    <Radio value="1">{t(profile.cnTeacher)}</Radio>
                    <Radio value="2">
                      {t(profile.enTeacher)}
                      <span className="radio-notice">（{t(profile.enTeacherMessage)}）</span>
                    </Radio>
                  </Radio.Group>
                </Item>
                <div className="form-footer">
                  <Button type="primary" disabled={isHaveEmail || isHaveTel} onClick={() => handleToNext(2)}>
                    {t(profile.nextStep)}：{t(profile.setPassword)}
                  </Button>
                  <br/>
                  <Link to="/login">{t(profile.goToLogin)}</Link>
                </div>
              </>
            )}
            {currentStep === 2 && (
              <>
                <Item name={'password'} rules={[
                  {
                    pattern: /^(?=.*\d)(?=.*[a-zA-Z])[0-9a-zA-Z]{6,18}$/,
                    message: t(profile.passwordValidMessage),
                  },
                  { required: true, message: t(profile.passwordHolder) },
                ]}>
                  <Input
                    type="password"
                    placeholder={t(profile.passwordHolder)}
                    prefix={<LockFilled className="input-icon"/>}
                  />
                </Item>
                <Item name={'confirmPassword'} dependencies={['password']} rules={[
                  {
                    required: true,
                    message: t(profile.confirmPasswordHolder),
                  },
                  ({ getFieldValue }) => ({
                    validator(_, value) {
                      if (!value || getFieldValue('password') === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject(new Error(t(profile.passwordConfirmValidMessage)));
                    },
                  }),
                ]}>
                  <Input
                    type="password"
                    placeholder={t(profile.confirmPasswordHolder)}
                    prefix={<LockFilled className="input-icon"/>}
                  />
                </Item>
                <div className="form-footer">
                  <Button type="primary" onClick={() => handleToNext(3)}>
                    {t(profile.nextStep)}：{t(profile.submitApply)}
                  </Button>
                  <br/>
                  <a onClick={() => setCurrentStep(1)}>{t(profile.backToPreStep)}</a>
                </div>
              </>
            )}
            {currentStep === 3 && (
              <>
                <div className="step3-message">
                  <CheckCircleOutlined className="success-icon"/>
                  <br/>
                  <span className="success">{t(profile.registerSuccess)}</span>
                  <br/>
                  <br/>
                  <span className="success-text">{t(profile.successMessage)}</span>
                </div>
                <div className="form-footer">
                  <Button type="primary" onClick={() => history.push('/login')}>
                    {t(profile.finish)}
                  </Button>
                  <br/>
                  <Link to="/login">{t(profile.goToLogin)}</Link>
                </div>
              </>
            )}
          </Form>
        </>
      </AccountPanel>
    </section>
  );
};
