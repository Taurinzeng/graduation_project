import { Button, Form, Input, Tabs } from 'antd';
import React, { useState } from 'react';
import AccountPanel from '../component/account-panel';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import CodeInput from '../component/code-input';
import { LockFilled, MailOutlined, PhoneOutlined } from '@ant-design/icons';
import './index.less';
import { Link, useHistory } from 'react-router-dom';
import { useForm } from 'antd/lib/form/Form';
import { doUpdatePassword } from '@service/profile';

const { TabPane } = Tabs;

export default () => {
  const {
    profile,
    placeholder,
    buttons: { Confirm },
  } = translations;
  const { t } = useTranslation();
  const [form] = useForm();
  const history = useHistory();
  const [foundType, setFoundType] = useState('e');
  const {} = translations;
  const switchFoundType = () => {
    form.resetFields();
    setFoundType(foundType === 't' ? 'e' : 't');
  };
  return (
    <section className="forget-password-page">
      <AccountPanel style={{ height: 600, width: 400 }}>
        <div>
          <div className="title">{t(profile.passwordFound)}</div>
          <div className="found-type">
            <Tabs activeKey={foundType} onChange={switchFoundType} centered>
              <TabPane tab={t(profile.emailFound)} key="e" />
              <TabPane tab={t(profile.telFound)} key="t" />
            </Tabs>
          </div>
          <Form form={form} className="forget-password-form">
            {foundType === 't' ? (
              <Form.Item name={'telephone'} rules={[{ required: true, message: t(placeholder.telHolder1) }]}>
                <Input placeholder={t(placeholder.telHolder1)} prefix={<PhoneOutlined className="input-icon" />} />
              </Form.Item>
            ) : (
              <Form.Item
                name="email"
                rules={[
                  { type: 'email', message: t(profile.emailValidHolder) },
                  { required: true, message: t(profile.emailHolder) },
                ]}>
                <Input placeholder={t(profile.emailHolder)} prefix={<MailOutlined className="input-icon" />} />
              </Form.Item>
            )}
            <Form.Item>
              <CodeInput
                form={form}
                onChange={(value: any) => {
                  form.setFieldsValue({
                    verifyCode: value,
                  });
                }}
              />
            </Form.Item>
            <Form.Item
              name={'password'}
              rules={[
                {
                  pattern: /^(?=.*\d)(?=.*[a-zA-Z])[0-9a-zA-Z]{6,18}$/,
                  message: t(profile.passwordValidMessage),
                },
                { required: true, message: t(profile.passwordHolder) },
              ]}>
              <Input type="password" placeholder={t(profile.newPassword)} prefix={<LockFilled className="input-icon" />} />
            </Form.Item>
            <Form.Item
              name={'confirmPassword'}
              dependencies={['password']}
              rules={[
                {
                  required: true,
                  message: t(profile.confirmPasswordHolder),
                },
                ({ getFieldValue }) => ({
                  validator(_, value) {
                    if (!value || getFieldValue('password') === value) {
                      return Promise.resolve();
                    }
                    return Promise.reject(new Error(t(profile.passwordConfirmValidMessage)));
                  },
                }),
              ]}>
              <Input type="password" placeholder={t(profile.passwordConfirm)} prefix={<LockFilled className="input-icon" />} />
            </Form.Item>
            <Button
              type="primary"
              onClick={() => {
                form.validateFields().then((values: any) => {
                  doUpdatePassword(values).then(() => {
                    history.push('/login');
                  });
                });
              }}
              className="confirm-button">
              {t(Confirm)}
            </Button>
            <br />
            <br />
            <p className={'p-link'}>
              <Link to="/login">{t(profile.goToLogin)}</Link>
            </p>
          </Form>
        </div>
      </AccountPanel>
    </section>
  );
};
