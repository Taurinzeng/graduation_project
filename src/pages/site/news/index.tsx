import React from 'react';
import { Redirect, Route, Switch } from 'react-router';

import PageModule from '@pages/site/module';

const NewContent = React.lazy(() => import('@pages/site/news/content'));
const NewDetail = React.lazy(() => import('@pages/site/news/detail'));
export default () => {
  return <PageModule>
    <Switch>
      <Route exact path="/exclude/new" render={() => <Redirect to="/exclude/new/:type"/>}/>
      <Route path="/exclude/new/:type/:id" component={NewDetail}/>
      <Route path="/exclude/new/:type" component={NewContent}/>
    </Switch>
  </PageModule>;
}