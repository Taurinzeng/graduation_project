import React from 'react';
import { Route, Switch } from 'react-router';

import PageModule from '@pages/site/module';

const GuideContent = React.lazy(() => import('@pages/site/guide/content'));
const GuideDetail = React.lazy(() => import('@pages/site/news/detail'));
export default () => {
  return <PageModule>
    <Switch>
      <Route path="/exclude/guide/:type/:id" component={GuideDetail}/>
      <Route path="/exclude/guide/:type" component={GuideContent}/>
    </Switch>
  </PageModule>;
}