import React from 'react';
import { Route, Switch } from 'react-router';

import PageModule from '@pages/site/module';

const MaterialContent = React.lazy(() => import('@pages/site/material/content'));
const MaterialDetail = React.lazy(() => import('@pages/site/news/detail'));
export default () => {
  return <PageModule>
    <Switch>
      <Route path="/exclude/material/:type/:id" component={MaterialDetail}/>
      <Route path="/exclude/material/:type" component={MaterialContent}/>
    </Switch>
  </PageModule>;
}