import React from 'react';
import Style from './index.module.less';
import { Redirect, Route, Switch } from 'react-router';

const News = React.lazy(() => import('@pages/site/news'));
const Guide = React.lazy(() => import('@pages/site/guide'));
const Material = React.lazy(() => import('@pages/site/material'));
const Question = React.lazy(() => import('@pages/site/question'));
export default () => {
  return (
    <section className={Style.no_home_page}>
      <Switch>
        <Route exact path="/exclude" render={() => <Redirect to="/exclude/new"/>}/>
        <Route path="/exclude/new" component={News}/>
        <Route path="/exclude/guide" component={Guide}/>
        <Route path="/exclude/material" component={Material}/>
        <Route path="/exclude/question" component={Question}/>
      </Switch>
    </section>
  );
};