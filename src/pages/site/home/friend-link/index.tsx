import React from 'react';
import { useTranslation } from 'react-i18next';
import Style from './index.module.less';
import { translations } from '@locales/i18n';
import { testApi, testApi2 } from '@service/profile';

export default () => {
  const { t } = useTranslation();
  const handleTest = () => {
    testApi();
  }
  const handleTest2 = () => {
    testApi2({});
  }
  return (
    <div className={Style.module_panel}>
      <section>
        <div className={Style.header}>
          <span className={Style.title}>{t(translations.home.friendLink)}</span>
        </div>
        <div className={Style.a_link}>
          <a onClick={handleTest}>电子科技大学</a>
          <a onClick={handleTest2}>电子科技大学格拉斯哥学院</a>
          <a>中华人民共和国教育部</a>
          <a>教育涉外监管信息网</a>
          <a>电子科技大学</a>
        </div>
      </section>
    </div>
  );
};
