import React, { useEffect } from 'react';
import { Carousel } from 'antd';
import News from './news';
import Login from './login-panel';
import Service from './service';
import Data from './data';
import FAQ from './FAQ';
import AboutUs from './about-us';
import FriendLink from './friend-link';
import './index.less';

export default () => {
  useEffect(() => {

  }, []);
  return (
    <div className={'back-home'}>
      <section className="home-page">
        <Carousel className={'carousel'}>
          <div>
            <img
              className="carousel-image"
              src={require('./u14.jpeg')}
            />
          </div>
          <div>
            <img
              className="carousel-image"
              src="https://vkceyugu.cdn.bspapp.com/VKCEYUGU-566db12e-1d3c-4aea-99af-eabf8a1d35c9/a972ea0c-4d6f-488a-a2ae-560955a901b6.jpg"
            />
          </div>
        </Carousel>
        <div className="panel-line">
          <News />
          <Login />
        </div>
        <div className="panel-line">
          <Service />
          <Data />
        </div>
        <div className="panel-line">
          <FAQ />
          <AboutUs />
        </div>
        <div className="panel-line">
          <FriendLink />
        </div>
      </section>
    </div>
  );
};
