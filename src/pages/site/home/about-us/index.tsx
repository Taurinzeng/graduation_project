import React from 'react';
import Panel from '@components/panel';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import { IconfontWidgetOperate } from '@components/iconfont';
import './index.less';
import { Divider } from 'antd';

export default () => {
  const { t } = useTranslation();
  return (
    <Panel title={t(translations.home.aboutUs)} moreUrl="/news">
      <div className={'about-us'}>
        <div className="item-us">
          <IconfontWidgetOperate className={'icon-font icon'} type={'icon-school'}/>
          <p>{t(translations.home.schoolDetail)}</p>
        </div>
        <Divider className={'divider'} type="vertical"/>
        <div className="item-us">
          <IconfontWidgetOperate className={'icon-font icon'} type={'icon-department'}/>
          <p>{t(translations.home.departmentDetail)}</p>
        </div>
        <Divider className={'divider'} type="vertical"/>
        <div className="item-us">
          <IconfontWidgetOperate className={'icon-font icon'} type={'icon-leader'}/>
          <p>{t(translations.home.leaderDetail)}</p>
        </div>
      </div>
    </Panel>
  );
};
