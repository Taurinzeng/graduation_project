import { translations } from '@locales/i18n';
import { Divider } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
import './index.less';
import { IconfontWidgetOperate } from '@components/iconfont';
import { getLoginUrl } from '@service/profile';
export default () => {
  const { t } = useTranslation();
  const { home } = translations;
  const handleCommonLogin = () => {
    getLoginUrl().then((res: any) => {
      if (res.code === 302) {
        window.location.href = res.url;
      }
    })
  }
  return (
    <div className="login-panel">
      <div className={'title'}>{t(home.systemLogin)}</div>
      <div className={'login-btn'}>
        <div>
          <p className="login-type" onClick={handleCommonLogin}>
            <IconfontWidgetOperate type={'icon-user'} style={{ color: '#1890ff', verticalAlign: 'sub' }} />
            {t(home.schoolUserLogin)}
          </p>
          <p className="type-message">
            {t(home.schoolUserLoginMessage)}
          </p>
        </div>
      </div>
      <Divider />
      <div className={'login-btn'}>
        <Link to="/login">
          <p className="login-type">
            <IconfontWidgetOperate type={'icon-add_user_male'} style={{ color: '#1890ff', fontSize: 22 }} />
            {t(home.otherUserLogin)}
          </p>
          <p className="type-message">
            {t(home.otherUserLoginMessage)}
          </p>
        </Link>
      </div>
    </div>
  );
};
