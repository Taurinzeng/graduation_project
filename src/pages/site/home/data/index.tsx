import React from 'react';
import Panel from '@components/panel';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
export default () => {
  const { t } = useTranslation();
  return (
    <Panel title={t(translations.home.dataDownload)} moreUrl="/news">
      <>
        <div className="item">
          <span>组织召开学懂弄通做实重要讲话精神理论研讨会</span>
          <span>2021-09-09</span>
        </div>
        <div className="item">
          <span>组织召开学懂弄通做实重要讲话精神理论研讨会</span>
          <span>2021-09-09</span>
        </div>
        <div className="item">
          <span>组织召开学懂弄通做实重要讲话精神理论研讨会</span>
          <span>2021-09-09</span>
        </div>
      </>
    </Panel>
  );
};
