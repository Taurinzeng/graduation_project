import React from 'react';
import { Breadcrumb } from 'antd';
import Style from './index.module.less';

export default () => {
  return <Breadcrumb className={Style.breadcrumb}>
    <Breadcrumb.Item>Home</Breadcrumb.Item>
    <Breadcrumb.Item>
      <a href="">Application Center</a>
    </Breadcrumb.Item>
    <Breadcrumb.Item>
      <a href="">Application List</a>
    </Breadcrumb.Item>
    <Breadcrumb.Item>An Application</Breadcrumb.Item>
  </Breadcrumb>;
}