import React from 'react';
import { Menu } from 'antd';
import Style from './index.module.less';
import { useHistory } from 'react-router';

export default () => {
  const history = useHistory();
  return <Menu
    className={Style.menu}
    defaultSelectedKeys={['1']}
    mode={'inline'}
    theme={'light'}
  >
    <Menu.Item key="1" onClick={() => {
      history.push('/exclude/new/1/1');
    }}>
      新闻公告1
    </Menu.Item>
    <Menu.Item key="2">
      新闻公告2
    </Menu.Item>
  </Menu>;
}