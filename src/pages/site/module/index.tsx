import React from 'react';
import Style from './index.module.less';

import BreadcrumbHeader from './nav-link';
import LeftMenu from './left-menu';

export default (props: any) => {
  
  return <div className={Style.page}>
    <div className={Style.header}>
      <BreadcrumbHeader/>
    </div>
    <div className={Style.content}>
      <div className={Style.left}>
        <LeftMenu/>
      </div>
      <div className={Style.right}>
        {props.children}
      </div>
    </div>
  </div>;
}