import React from 'react';
import { Route, Switch } from 'react-router';

import PageModule from '@pages/site/module';

const QuestionContent = React.lazy(() => import('@pages/site/question/content'));
const QuestionDetail = React.lazy(() => import('@pages/site/news/detail'));
export default () => {
  return <PageModule>
    <Switch>
      <Route path="/exclude/question/:type/:id" component={QuestionDetail}/>
      <Route path="/exclude/question/:type" component={QuestionContent}/>
    </Switch>
  </PageModule>;
}