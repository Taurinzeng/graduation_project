import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { Button } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { ExportOutlined } from '@ant-design/icons';

const SubjectCompleted = () => {
  const { t } = useTranslation();
  const { table, placeholder, buttons } = translations;
  const columns = [
    {
      title: t(table.StudentName),
      dataIndex: 'stuName',
    },
    {
      title: t(table.StudentNumber),
      dataIndex: 'UESTCID',
    },
    {
      title: t(table.SubjectName),
      dataIndex: 'researchField',
    },
    {
      title: t(table.SubjectSource),
      dataIndex: 'source',
    },
    {
      title: t(table.FormOfExpectedResults),
      dataIndex: 'adviser',
    },
    {
      title: t(table.GraduationProjectThesisCategory),
      dataIndex: 'UESTCID',
    },
    {
      title: t(table.WritingLanguage),
      dataIndex: 'researchField',
    },
    {
      title: t(table.ExcellentThesisAtUniversityLevel),
      dataIndex: 'source',
    },
    {
      title: t(table.GuidanceUnit),
      dataIndex: 'forProfessional',
    },
    {
      title: t(table.Region),
      dataIndex: 'adviser',
    },
    {
      title: t(table.InstructorsName),
      dataIndex: 'adviser',
    },
    {
      title: t(table.TitleOfInstructor),
      dataIndex: 'adviser',
    },
    {
      title: t(table.EscrowTeacher),
      dataIndex: 'adviser',
    },
    {
      title: t(table.PerformanceEvaluation),
      dataIndex: 'adviser',
    },
    {
      title: t(table.Remark),
      dataIndex: 'adviser',
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(table.SubjectSource),
      code: 'degree',
      type: 'select',
      options: [],
    },
    {
      label: t(table.AchievementForm),
      code: 'degree',
      type: 'select',
      options: [],
    },
    {
      label: t(table.DesignCategory),
      code: 'degree',
      type: 'select',
      options: [],
    },
    {
      label: t(table.WritingLanguage),
      code: 'degree',
      type: 'select',
      options: [],
    },
    {
      code: 'degree',
      type: 'input',
      placeholder: t(placeholder.SubjectNameOrStudentNameOrInstructorName),
    },
  ];
  return (
    <TableTemplate
      columns={columns}
      queryAjax={() => Promise.resolve()}
      rowKey={(record: any) => record.id}
      filterFields={filterFields}
      topButtons={[
        <Button type="primary" icon={<ExportOutlined />} onClick={() => {}}>
          {t(buttons.Export)}
        </Button>,
      ]}
    />
  );
};
export default SubjectCompleted;
