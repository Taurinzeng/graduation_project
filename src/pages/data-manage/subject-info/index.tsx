import { translations } from '@locales/i18n';
import { Radio } from 'antd';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';
import SubjectCompleted from '@pages/data-manage/subject-info/completed';
import './index.less';
import SubjectGraduation from '@pages/data-manage/subject-info/graduation';

export default () => {
  const { t } = useTranslation();
  const { expertList } = translations;
  const [currentTab, setCurrentTab] = useState('1');
  return (
    <section className="subject-info-page">
      <div className="table-part">
        <div className="expert-switch">
          <Radio.Group value={currentTab} onChange={(e: any) => setCurrentTab(e.target.value)}>
            <Radio.Button value="1">{t(expertList.spic)}</Radio.Button>
            <Radio.Button value="2">{t(expertList.ktxxtj)}</Radio.Button>
            <Radio.Button value="3">{t(expertList.yxbysjtj)}</Radio.Button>
          </Radio.Group>
        </div>
        {currentTab === '1' && <SubjectCompleted/>}
        {currentTab === '3' && <SubjectGraduation/>}
      </div>
    </section>
  );
};
