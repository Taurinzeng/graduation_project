import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import { Button } from 'antd';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { ExportOutlined } from '@ant-design/icons';

const SubjectGraduation = () => {
  const { t } = useTranslation();
  const { table, placeholder, buttons } = translations;
  const columns = [
    {
      title: t(table.BranchSchool),
      dataIndex: 'stuName',
    },
    {
      title: t(table.StudentNumber),
      dataIndex: 'UESTCID',
    },
    {
      title: t(table.SubjectName),
      dataIndex: 'researchField',
    },
    {
      title: t(table.StudentName),
      dataIndex: 'source',
    },
    {
      title: t(table.ThesisResults),
      dataIndex: 'forProfessional',
    },
    {
      title: t(table.InstructorsName),
      dataIndex: 'adviser',
    },
    {
      title: t(table.TitleOfInstructor),
      dataIndex: 'UESTCID',
    },
    {
      title: t(table.Region),
      dataIndex: 'researchField',
    },
    {
      title: t(table.Level),
      dataIndex: 'source',
    },
    {
      title: t(table.Remark),
      dataIndex: 'adviser',
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(table.Level),
      code: 'degree',
      type: 'select',
      options: [],
    },
    {
      code: 'degree',
      type: 'input',
      placeholder: t(placeholder.SubjectNameOrStudentNameOrInstructorName),
    },
  ];
  return (
    <TableTemplate
      columns={columns}
      queryAjax={() => Promise.resolve()}
      rowKey={(record: any) => record.id}
      filterFields={filterFields}
      topButtons={[
        <Button type="primary" icon={<ExportOutlined />} onClick={() => {}}>
          {t(buttons.Export)}
        </Button>,
      ]}
    />
  );
};
export default SubjectGraduation;
