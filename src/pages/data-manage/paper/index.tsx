import TableTemplate from '@components/table-template';
import { FieldType } from '@components/table-template/filter-form/model';
import { translations } from '@locales/i18n';
import React from 'react';
import { ExportOutlined } from '@ant-design/icons';

import { useTranslation } from 'react-i18next';
import { Button } from 'antd';

export default () => {
  const { t } = useTranslation();
  const { table, buttons } = translations;
  const columns = [
    {
      title: t(table.ID),
      dataIndex: 'no',
      render: (text: any, record: any, index: number) => {
        return index + 1;
      },
    },
    {
      title: t(table.BranchSchool),
      dataIndex: 'stuName',
    },
    {
      title: t(table.InstructorJobNumber),
      dataIndex: 'UESTCID',
    },
    {
      title: t(table.InstructorsName),
      dataIndex: 'researchField',
    },
    {
      title: t(table.TitleOfInstructor),
      dataIndex: 'forProfessional',
    },
    {
      title: t(table.College),
      dataIndex: 'adviser',
    },
    {
      title: t(table.SalaryNumber),
      dataIndex: 'specialtyName',
    },
    
    {
      title: t(table.NumberOfExcellentPapers),
      dataIndex: 'operation',
    },
    {
      title: t(table.Awards),
      dataIndex: 'operation',
    },
    {
      title: t(table.Remark),
      dataIndex: 'operation',
    },
  ];
  const filterFields: FieldType[] = [
    {
      label: t(table.College),
      code: 'degree',
      type: 'select',
      options: [
        {
          text: '全部',
          value: '',
        },
        {
          text: '启用',
          value: '1',
        },
        {
          text: '停用',
          value: '2',
        },
      ],
    },
    {
      label: t(table.PositionLevel),
      code: 'degree',
      type: 'select',
      options: [],
    },
    {
      code: 'degree',
      type: 'input',
      placeholder: t(table.ProjectNameOrInstructorName),
    },
  ];
  return (
    <section className="paper-manage-page">
      <TableTemplate
        columns={columns}
        queryAjax={() => Promise.resolve()}
        rowKey={(record: any) => record.id}
        filterFields={filterFields}
        topButtons={[<Button type="primary" icon={<ExportOutlined/>} onClick={() => {
        
        }}>
          {t(buttons.Export)}
        </Button>]}
      />
    </section>
  );
};
