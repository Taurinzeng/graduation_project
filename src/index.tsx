import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import zh_CN from 'antd/lib/locale-provider/zh_CN';
import en_US from 'antd/lib/locale-provider/en_US';
import Router from './router';
import store from '@modal/store';
import '@locales/i18n';
import './index.less';
import { ConfigProvider } from 'antd';
const App: React.FC = () => {
  const language = localStorage.getItem('i18nextLng') === 'cn' ? zh_CN : en_US;
  return (
    <ConfigProvider locale={language}>
      <Provider store={store}>
        <Router />
      </Provider>
    </ConfigProvider>
  );
};

ReactDOM.render(<App />, document.getElementById('root'));
