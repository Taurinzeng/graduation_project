export const menus = [
  {
    name: 'home',
    icon: '',
    title: '首页',
    subs: [
      {
        name: 'overview',
        title: '概览',
      },
      {
        name: 'my_todo',
        title: '我的待办',
      },
      {
        name: 'guide_log',
        title: '指导日志',
      },
    ],
  },
  {
    name: 'paper_manage',
    title: '论文管理',
    subs: [
      {
        name: 'project_setting',
        title: '项目基本设置',
      },
      {
        name: 'pro_time_setting',
        title: '项目时间设置',
      },
      {
        name: 'stu_manage',
        title: '学生管理',
      },
      {
        name: 'teacher_manage',
        title: '教师管理',
        noMenuSubs: [
          {
            name: 'project_list',
            title: '查看出题情况',
            noMenuSubs: [
              {
                name: 'detail',
                title: '题目详情',
              },
            ],
          },
        ],
      },
      {
        name: 'expert_manage',
        title: '专家管理',
      },
      {
        name: 'sub_audit_manage',
        title: '题目管理',
        noMenuSubs: [
          {
            name: 'detail',
            title: '题目详情',
          },
        ],
      },
      {
        name: 'pre_allot_manage',
        title: '预分配管理',
        noMenuSubs: [
          {
            name: 'detail',
            title: '题目详情',
          },
        ],
      },
      {
        name: 'select_sub_manage',
        title: '选题管理',
        noMenuSubs: [
          {
            name: 'choose_detail',
            title: '题目选题详情',
          },
        ],
      },
      {
        name: 'project_change_manage',
        title: '题目变更管理',
        noMenuSubs: [
          {
            name: 'change_detail',
            title: '变更题目内容',
          },
        ],
      },
      {
        name: 'distb_second_teacher',
        title: '第二指导教师管理',
        noMenuSubs: [
          {
            name: 'detail',
            title: '题目详情',
          },
        ],
      },
      {
        name: 'tor_audit',
        title: '任务书与计划表审核',
        noMenuSubs: [
          {
            name: 'detail',
            title: '任务书与计划详情',
          },
        ],
      },
      {
        name: 'begin_audit_manage_double',
        title: '初期检查管理（双学位）',
        noMenuSubs: [
          {
            name: 'detail',
            title: '初期检查详情',
          },
        ],
      },
      {
        name: 'begin_audit_manage_single',
        title: '初期检查管理（单学位）',
        noMenuSubs: [
          {
            name: 'detail',
            title: '初期检查详情',
          },
        ],
      },
      {
        name: 'interim_audit_manage_double',
        title: '中期检查管理（双学位）',
        noMenuSubs: [
          {
            name: 'detail',
            title: '中期检查详情',
          },
        ],
      },
      {
        name: 'interim_audit_manage_single',
        title: '中期检查管理（单学位）',
        noMenuSubs: [
          {
            name: 'detail',
            title: '中期检查详情',
          },
        ],
      },
      {
        name: 'reveiw_expert_allot',
        title: '评审专家分配管理',
        noMenuSubs: [
          {
            name: 'detail',
            title: '查看评审结果',
          },
        ],
      },
      {
        name: 'duplicate_check_manage',
        title: '论文查重管理',
        noMenuSubs: [
          {
            name: 'view',
            title: '论文查重报告',
          },
          {
            name: 'audit',
            title: '论文查重报告审批',
          },
        ],
      },
      {
        name: 'arbitration_manage',
        title: '论文仲裁管理',
        noMenuSubs: [
          {
            name: 'detail',
            title: '仲裁详情',
          },
        ],
      },
      {
        name: 'expenses_manage',
        title: '报销管理',
      },
      {
        name: 'buy_manage',
        title: '采购管理',
      },
    ],
  },
  {
    name: 'content_manage',
    title: '内容管理',
    subs: [
      {
        name: 'banner_manage',
        title: 'banner管理',
      },
      {
        name: 'friendly-url_manage',
        title: '友情链接管理',
      },
      {
        name: 'nav_manage',
        title: '导航管理',
      },
      {
        name: 'content_info_manage',
        title: '内容管理',
        noMenuSubs: [
          {
            name: 'create',
            title: '新建内容',
          },
          {
            name: 'edit',
            title: '内容编辑',
          },
          {
            name: 'detail',
            title: '内容详情',
          },
        ],
      },
      {
        name: 'message_manage',
        title: '消息管理',
        noMenuSubs: [
          {
            name: 'create',
            title: '消息创建',
          },
          {
            name: 'edit',
            title: '消息编辑',
          },
          {
            name: 'detail',
            title: '消息详情',
          },
        ],
      },
    ],
  },
  {
    name: 'data_manage',
    title: '数据统计',
    subs: [
      {
        name: 'paper',
        title: '优秀论文统计',
      },
      {
        name: 'subject_info',
        title: '课题信息统计',
      },
      {
        name: 'subject_related',
        title: '课题关联统计',
      },
    ],
  },
  {
    name: 'sys_manage',
    title: '系统管理',
    subs: [
      {
        name: 'register',
        title: '注册管理',
        noMenuSubs: [
          {
            name: 'audit',
            title: '用户审核',
          },
          {
            name: 'detail',
            title: '用户详情',
          },
        ],
      },
      {
        name: 'user',
        title: '用户管理',
        noMenuSubs: [
          {
            name: 'detail',
            title: '用户详情',
          },
        ],
      },
      {
        name: 'login_log',
        title: '登录日志',
      },
      {
        name: 'operate_log',
        title: '操作日志',
      },
    ],
  },
  {
    name: 'profile',
    title: '个人中心',
  },
];
