import request, { prefix } from '@utils/request';

export const doLogin = (params: any) => {
  return request.post(`${prefix}/base/Login/userLogin`, params).then((res: any) => res.data);
};
export const doRegister = (params: any) => {
  return request.post(`${prefix}/base/SysUser/registerUser`, params).then((res: any) => res.data);
};

export const registerSendCode = (params: any) => {
  return request.post(`${prefix}/base/SysUser/sendVerifyCode`, params).then((res: any) => res.data);
};

export const heckPhoneOrEmail = (params: any) => {
  // @ts-ignore
  return request.post(`${prefix}/base/SysUser/checkPhoneOrEmail`, params, { hideErrorMessage: true }).then((res: any) => res.data);
};

export const doUpdatePassword = (params: any) => {
  return request.post(`${prefix}/base/SysUser/updateUserByUserInfo`, params).then((res: any) => res.data);
};

export const sendCode = (params: any) => {
  return request.post(`${prefix}/login`, params).then((res: any) => res.data);
};
export const getLoginUrl = () => {
  return request.get(`${prefix}/base/Login/toCas`).then((res: any) => res.data);
};

export const testApi = () => {
  return request.get(`${prefix}/base/auditAPI/getKey`).then((res: any) => res.data);
};
export const testApi2 = (params: any) => {
  return request.post(`${prefix}/base/SysOperateLog/deleteOperateLog`, params).then((res: any) => res.data);
};
