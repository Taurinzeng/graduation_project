export const stuMenu = [
  {
    name: 'user_paper_manage',
    title: '论文管理',
    subs: [
      {
        name: 'pre_assign',
        title: '预分配管理',
      },
      {
        name: 'choose_project',
        title: '论文选题管理',
      },
      {
        name: 'project_change',
        title: '题目变更管理',
      },
      {
        name: 'task_and_plan',
        title: '查看任务书与计划表',
      },
      {
        name: 'initial_check',
        title: '初期检查',
      },
      {
        name: 'middle_check',
        title: '中期检查',
      },
      {
        name: 'repeat_check',
        title: '论文查重及上传',
      },
      {
        name: 'check_defence_info',
        title: '查看答辩信息',
      },
      {
        name: 'check_grade',
        title: '查看论文成绩',
      },
      {
        name: 'guidce_log',
        title: '指导日志',
      },
      {
        name: 'project_file',
        title: '论文过程文档',
      },
      {
        name: 'expenses_manage',
        title: '报销管理',
      },
      {
        name: 'buy_manage',
        title: '采购管理',
      },
    ],
  },
];
