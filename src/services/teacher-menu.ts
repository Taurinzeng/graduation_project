export const teacherMenu = [
  {
    name: 'home',
    icon: '',
    title: '首页',
    subs: [
      {
        name: 'overview',
        title: '概览',
      },
      {
        name: 'my_todo',
        title: '我的待办',
      },
      {
        name: 'guide_log',
        title: '指导日志',
      },
    ],
  },
  {
    name: 'paper_manage_t',
    title: '论文管理',
    subs: [
      {
        name: 'project_m',
        title: '题目管理',
        noMenuSubs: [
          {
            name: 'create',
            title: '新增题目',
          },
          {
            name: 'edit',
            title: '编辑题目',
          },
          {
            name: 'view',
            title: '题目详情',
          },
        ],
      },
      {
        name: 'project_change_f',
        title: '题目变更（一导）',
        noMenuSubs: [
          {
            name: 'project_edit',
            title: '题目变更修改',
          },
          {
            name: 'project_view',
            title: '题目变更详情',
          },
          {
            name: 'teacher_change',
            title: '变更指导教师',
          },
        ],
      },
      {
        name: 'project_change_s',
        title: '题目变更（二导）',
        noMenuSubs: [
          {
            name: 'project_view',
            title: '题目变更详情',
          },
          {
            name: 'teacher_change',
            title: '变更指导教师',
          },
        ],
      },
      {
        name: 'pre_allot',
        title: '预分配管理',
        noMenuSubs: [
          {
            name: 'view',
            title: '预分配查看',
          },
          {
            name: 'audit',
            title: '预分配审核',
          },
        ],
      },
      {
        name: 'apply_second',
        title: '申请第二指导教师',
        noMenuSubs: [
          {
            name: 'view',
            title: '课题详情',
          },
          {
            name: 'view_f',
            title: '已选课题详情',
          },
        ],
      },
      {
        name: 'task_and_plan',
        title: '下发任务书与计划表',
        noMenuSubs: [
          {
            name: 'view',
            title: '任务书与计划表详情',
          },
          {
            name: 'edit',
            title: '任务书与计划表填写',
          },
        ],
      },
      {
        name: 'preliminary',
        title: '初期检查管理',
        noMenuSubs: [
          {
            name: 'detail',
            title: '初期检查详情',
          },
          {
            name: 'audit',
            title: '初期检查审核',
          },
          {
            name: 'appraise',
            title: '评阅论文',
          },
        ],
      },
      {
        name: 'interim',
        title: '中期检查管理',
        noMenuSubs: [
          {
            name: 'detail',
            title: '中期检查详情',
          },
          {
            name: 'audit',
            title: '中期检查审核',
          },
          {
            name: 'appraise',
            title: '评阅论文',
          },
        ],
      },
    ],
  },
];
