import { menus } from './manager-menu';
import { stuMenu } from './student_menu';
import { teacherMenu } from './teacher-menu';
const prefix = '/api';
export const queryMenus = () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve({
        success: true,
        data: teacherMenu,
      });
    }, 1000);
  });
};
export const queryUserMenus = () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve({
        success: true,
        data: stuMenu,
      });
    }, 1000);
  });
};
