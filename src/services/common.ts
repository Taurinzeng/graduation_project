import request, { prefix } from '@utils/request';

export const upload = (params: any) => {
  return request.post(`${prefix}/base/uploadFile/upload`, params).then((res: any) => res.data);
};

// type类型1撰写语言2用户注册状态3职称4教师类型5审核结果6消息类型7消息查阅状态8消息状态）
export const codeQuery = (params: any) => {
  return request.post(`${prefix}/business/bsMbData/getMbDataByMbObj`, params).then((res: any) => res.data);
};
