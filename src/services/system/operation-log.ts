import request, { prefix } from '@utils/request';

export const queryOperationLog = (params: any) => {
  return request.post(`${prefix}/base/SysOperateLog/getOperateLogList`, params).then((res: any) => res.data);
};
export const queryLoginLog = (params: any) => {
  return request.post(`${prefix}/base/SysLoginLog/getLoginLogList`, params).then((res: any) => res.data);
};
