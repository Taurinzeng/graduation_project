import request, { prefix } from '@utils/request';

export const queryUserList = (params: any) => {
  return request.post(`${prefix}/base/SysUser/getUserList`, params).then((res: any) => res.data);
};
