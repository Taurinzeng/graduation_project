import request, { prefixB } from '@utils/request';

export const queryTeacherList = (params: any) => {
  return request.post(`${prefixB}/business/bsTeacherManage/getTeacherList`, params).then((res: any) => res.data);
};
export const getTeacherTotalInfo = () => {
  return request.post(`${prefixB}/business/bsTeacherManage/getTeacherNumInfo`).then((res: any) => res.data);
};
export const queryTeacherProject = () => {
  return request.post(`${prefixB}/business/bsTeacherManage/queryTeacherSubjs`).then((res: any) => res.data);
};
