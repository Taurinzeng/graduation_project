import request, { prefixB } from '@utils/request';

export const queryExpertList = (params: any) => {
  return request.post(`${prefixB}/business/bsExpertManage/getExpertList`, params).then((res: any) => res.data);
};
export const queryUnSelectExpertList = (params: any) => {
  return request.post(`${prefixB}/business/bsExpertManage/getUnSelectTeacher`, params).then((res: any) => res.data);
};
export const getExpertTotalInfo = () => {
  return request.post(`${prefixB}/business/bsExpertManage/getExpertNumInfo`).then((res: any) => res.data);
};
export const queryTeacherProject = () => {
  return request.post(`${prefixB}/business/bsTeacherManage/queryTeacherSubjs`).then((res: any) => res.data);
};
