import request, { prefixB } from '@utils/request';

export const queryStudentList = (params: any) => {
  return request.post(`${prefixB}/business/bsStudentManage/getStudentList`, params).then((res: any) => res.data);
};
export const queryUnSelectedStudentList = (params: any) => {
  return request.post(`${prefixB}/business/bsStudentManage/getUnSelectStudent`, params).then((res: any) => res.data);
};
export const getStudentTotalInfo = () => {
  return request.post(`${prefixB}/business/bsStudentManage/getStudentNumInfo`).then((res: any) => res.data);
};
export const removeStudent = (params: any) => {
  return request.post(`${prefixB}/business/bsStudentManage/deleteStudent`, params).then((res: any) => res.data);
};
export const updateStudent = (params: any) => {
  return request.post(`${prefixB}/business/bsStudentManage/updateStudentDegree`, params).then((res: any) => res.data);
};
export const selectStudent = (params: any) => {
  return request.post(`${prefixB}/business/bsStudentManage/selectStudent`, params).then((res: any) => res.data);
};
