import request, { prefixB } from '@utils/request';

export const saveProjectConfig = (params: any) => {
  return request.post(`${prefixB}/business/bsProject/prjConfig`, params).then((res: any) => res.data);
};
export const getProjectConfig = () => {
  return request.post(`${prefixB}/business/bsProject/getPrjInfo`).then((res: any) => res.data);
};

export const getProjectList = () => {
  return request.post(`${prefixB}/business/bsSubjectManage/getSubjectManageList`).then((res: any) => res.data);
};
