import request, { prefix } from '@utils/request';

export const queryContentList = (params: any) => {
  return request.post(`${prefix}/base/bsContentManage/getContentList`, params).then((res: any) => res.data);
};
export const getContentDetail = (params: any) => {
  return request.post(`${prefix}/base/bsContentManage/getContentById`, params).then((res: any) => res.data);
};
export const addContent = (params: any) => {
  return request.post(`${prefix}/base/bsContentManage/addContent`, params).then((res: any) => res.data);
};
export const editContent = (params: any) => {
  return request.post(`${prefix}/base/bsContentManage/updateContent`, params).then((res: any) => res.data);
};
export const getNavigationList = () => {
  return request.post(`${prefix}/base/bsNavigationManage/getNavigation`).then((res: any) => res.data);
};
