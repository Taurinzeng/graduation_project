import request, { prefix } from '@utils/request';

export const queryBannerList = (params: any) => {
  return request.post(`${prefix}/base/bsBannerManage/getBannerList`, { ...params, type: '1' }).then((res: any) => res.data);
};
export const addBanner = (params: any) => {
  return request.post(`${prefix}/base/bsBannerManage/addBanner`).then((res: any) => res.data);
};
export const editBanner = (params: any) => {
  return request.post(`${prefix}/base/bsBannerManage/updateBanner`).then((res: any) => res.data);
};
export const deleteBanner = (params: any) => {
  return request.post(`${prefix}/base/bsBannerManage/deleteBanner`, { ...params, type: '1' }).then((res: any) => res.data);
};
export const queryLinkList = (params: any) => {
  return request.post(`${prefix}/base/bsBannerManage/getBannerList`, { ...params, type: '1' }).then((res: any) => res.data);
};
export const addLink = (params: any) => {
  return request.post(`${prefix}/base/bsBannerManage/addLink`).then((res: any) => res.data);
};
export const editLink = (params: any) => {
  return request.post(`${prefix}/base/bsBannerManage/updateLink`).then((res: any) => res.data);
};
export const deleteLink = (params: any) => {
  return request.post(`${prefix}/base/bsBannerManage/deleteBanner`, { ...params, type: '2' }).then((res: any) => res.data);
};
