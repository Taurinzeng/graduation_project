import request, { prefix } from '@utils/request';

export const queryMessageList = (params: any) => {
  return request.post(`${prefix}/base/bsMessagetManage/getMessageList`, params).then((res: any) => res.data);
};
export const addMessage = (params: any) => {
  return request.post(`${prefix}/base/bsMessagetManage/addMessage`, params).then((res: any) => res.data);
};
export const editMessage = (params: any) => {
  return request.post(`${prefix}/base/bsMessagetManage/updateMessage`, params).then((res: any) => res.data);
};
export const getMessageDetail = (params: any) => {
  return request.post(`${prefix}/base/bsMessagetManage/getMessageDetailById`, params).then((res: any) => res.data);
};

export const queryRoles = () => {
  return request.post(`${prefix}/base/bsMessagetManage/getSendRoles`).then((res: any) => res.data);
};

export const queryPersonByRoleId = (params: any) => {
  return request.post(`${prefix}/base/bsMessagetManage/getUsersByRoleAndPrj`, params).then((res: any) => res.data);
};
