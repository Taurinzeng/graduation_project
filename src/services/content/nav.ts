import request, { prefix } from '@utils/request';

export const queryNavList = (params: any) => {
  return request.post(`${prefix}/base/bsNavigationManage/getNavigation`, params).then((res: any) => res.data);
};
export const addNav = (params: any) => {
  return request.post(`${prefix}/base/bsNavigationManage/navigation`, params).then((res: any) => res.data);
};
export const editNav = (params: any) => {
  return request.put(`${prefix}/base/bsNavigationManage/navigation`, params).then((res: any) => res.data);
};
export const deleteNav = (params: any) => {
  return request.delete(`${prefix}/base/bsNavigationManage/navigation`, params).then((res: any) => res.data);
};
