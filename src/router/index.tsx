import React, { Suspense, useEffect } from 'react';
import { BrowserRouter, Redirect, Route } from 'react-router-dom';
import { CacheRoute, CacheSwitch } from 'react-router-cache-route';
import WorkLayout from '../layout/work-layout';
import { Spin } from 'antd';
import HomeLayout from '../layout/home-layout';
import Test from '@pages/test';
import MasterRouter from './master-router';
import TeacherRouter from './teacher-router';

const LoginTransfer = React.lazy(() => import('@pages/site/account/login/login-transfer'));
const Login = React.lazy(() => import('@pages/site/account/login'));
const Overview = React.lazy(() => import('@pages/work-home/overview'));
const Home = React.lazy(() => import('@pages/site/home'));
const NoHome = React.lazy(() => import('@pages/site/no-home'));
const Register = React.lazy(() => import('@pages/site/account/register'));
const ForgetPassword = React.lazy(() => import('@pages/site/account/forget-password'));
//学生路由

export default () => {
  useEffect(() => {
    initGlobalCSSVariables();
  }, []);
  const getCatchKey = (props: any) => {
    return props.pathname;
  };

  return (
    <BrowserRouter>
      <Suspense fallback={<Spin spinning={true} />}>
        <CacheSwitch>
          <Route path="/login" component={Login} />
          <Route path="/loginTransfer" component={LoginTransfer} />
          <Route path="/register" component={Register} />
          <Route path="/forget_password" component={ForgetPassword} />
          <Route path="/test" component={Test} />
          <CacheRoute path="/work/">
            <WorkLayout>
              <CacheRoute exact path="/work" render={() => <Redirect to="/work/home/overview" />} />
              <CacheRoute path="/work/home/overview" component={Overview} />
              <MasterRouter />
              <TeacherRouter />
            </WorkLayout>
          </CacheRoute>

          <Route exact path="/" render={() => <Redirect to="/home" />} />
          <HomeLayout>
            <Route path="/home" component={Home} />
            <Route path="/exclude" component={NoHome} />
          </HomeLayout>
        </CacheSwitch>
      </Suspense>
    </BrowserRouter>
  );
};

const initGlobalCSSVariables = () => {
  const style = document.documentElement.style;
  style.setProperty('--fz-more-text-color', '#666');
};
