import React, { useEffect } from 'react';
import { CacheRoute } from 'react-router-cache-route';
const Todo = React.lazy(() => import('@pages/work-home/todo'));
const ProjectManageT = React.lazy(() => import('@pages/paper/teacher/project-manage'));
const ProjectManageTEdit = React.lazy(() => import('@pages/paper/teacher/project-manage/project-edit'));
const PorjectChangeFirst = React.lazy(() => import('@pages/paper/teacher/project-change-first'));
const PorjectChangeDetail = React.lazy(() => import('@pages/paper/teacher/project-change-first/project-change'));
const PorjectChangeSecond = React.lazy(() => import('@pages/paper/teacher/project-change-second'));
const PorjectChangeDetailSecond = React.lazy(() => import('@pages/paper/teacher/project-change-second/project-change'));
const PorjectChangeEdit = React.lazy(() => import('@pages/paper/teacher/project-change-first/project-edit'));
const PorjectChangeTChange = React.lazy(() => import('@pages/paper/teacher/teacher-change-page'));
const PreAllotT = React.lazy(() => import('@pages/paper/teacher/pre-allot-t'));
const PreAllotDetail = React.lazy(() => import('@pages/paper/teacher/pre-allot-t/allot-detail'));
const ApplySecondSupervisor = React.lazy(() => import('@pages/paper/teacher/apply-second-supervisor'));
const ProjectDetailPage = React.lazy(() => import('@pages/paper/teacher/project-detail-page'));
const PlanAndTaskPage = React.lazy(() => import('@pages/paper/teacher/plan-and-task'));
const PlanAndTaskDetail = React.lazy(() => import('@pages/paper/teacher/plan-and-task/detail'));
const PlanAndTaskEdit = React.lazy(() => import('@pages/paper/teacher/plan-and-task/edit'));
const PreliminaryCheck = React.lazy(() => import('@pages/paper/teacher/preliminary-check'));
const PreliminaryCheckDetailD = React.lazy(() => import('@pages/paper/teacher/preliminary-check/detail-double'));
const PreliminaryCheckDetailS = React.lazy(() => import('@pages/paper/teacher/preliminary-check/detail-single'));
const PreliminaryGradeDouble = React.lazy(() => import('@pages/paper/teacher/preliminary-check/double-grade'));
const PreliminaryGradeSingle = React.lazy(() => import('@pages/paper/teacher/preliminary-check/single-grade'));

const InterimCheck = React.lazy(() => import('@pages/paper/teacher/interim-check'));
const InterimCheckDetailD = React.lazy(() => import('@pages/paper/teacher/interim-check/detail-double'));
const InterimCheckDetailS = React.lazy(() => import('@pages/paper/teacher/interim-check/detail-single'));
const InterimGradeDouble = React.lazy(() => import('@pages/paper/teacher/interim-check/double-grade'));
const InterimGradeSingle = React.lazy(() => import('@pages/paper/teacher/interim-check/single-grade'));
const TeacherRouter = () => {
  const getCatchKey = (props: any) => {
    return props.pathname;
  };

  return (
    <>
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/project_m" component={ProjectManageT} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/project_m/:type/:id?" component={ProjectManageTEdit} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/project_change_f" component={PorjectChangeFirst} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/project_change_s" component={PorjectChangeSecond} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/project_change_f/project_edit/:id" component={PorjectChangeEdit} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/project_change_f/project_view/:id" component={PorjectChangeDetail} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/project_change_f/teacher_change/:type/:id" component={PorjectChangeTChange} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/project_change_s/project_view/:id" component={PorjectChangeDetailSecond} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/project_change_s/teacher_change/:type/:id" component={PorjectChangeTChange} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/pre_allot" component={PreAllotT} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/pre_allot/:type/:id" component={PreAllotDetail} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/apply_second" component={ApplySecondSupervisor} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/apply_second/:type/:id" component={ProjectDetailPage} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/task_and_plan" component={PlanAndTaskPage} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/task_and_plan/edit/:id?" component={PlanAndTaskEdit} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/task_and_plan/view/:id" component={PlanAndTaskDetail} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/preliminary" component={PreliminaryCheck} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/preliminary/appraise/double/:id" component={PreliminaryGradeDouble} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/preliminary/appraise/single/:id" component={PreliminaryGradeSingle} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/preliminary/:type/d/:id" component={PreliminaryCheckDetailD} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/preliminary/:type/s/:id" component={PreliminaryCheckDetailS} />

      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/interim" component={InterimCheck} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/interim/appraise/double/:id" component={InterimGradeDouble} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/interim/appraise/single/:id" component={InterimGradeSingle} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/interim/:type/d/:id" component={InterimCheckDetailD} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage_t/interim/:type/s/:id" component={InterimCheckDetailS} />
    </>
  );
};

export default TeacherRouter;
