import React, { useEffect } from 'react';
import { CacheRoute } from 'react-router-cache-route';
const ProjectList = React.lazy(() => import('@pages/paper/manager/teacher-management/project-list'));
const ProjectDetail = React.lazy(() => import('@pages/paper/manager/teacher-management/project-detail'));
const Todo = React.lazy(() => import('@pages/work-home/todo'));
const GuidanceLogList = React.lazy(() => import('@pages/work-home/guidance-log-list'));
const ProjectBaseSetting = React.lazy(() => import('@pages/paper/manager/project-base-setting'));
const ProjectTimeSetting = React.lazy(() => import('@pages/paper/manager/project-time-setting'));
const StudentManagement = React.lazy(() => import('@pages/paper/manager/student-management'));
const Detail = React.lazy(() => import('@pages/paper/detail'));
const TeacherManagement = React.lazy(() => import('@pages/paper/manager/teacher-management'));
const ExpertManagement = React.lazy(() => import('@pages/paper/manager/expert-management'));
const AuditManagement = React.lazy(() => import('@pages/paper/manager/project-management-list'));
const ProjectDetailSub = React.lazy(() => import('@pages/paper/manager/project-management-list/project-detail'));
const PreAllotManagement = React.lazy(() => import('@pages/paper/manager/pre-allot-management'));
const PreAllotDetail = React.lazy(() => import('@pages/paper/manager/pre-allot-management/allot-detail'));
const SelectManagement = React.lazy(() => import('@pages/paper/manager/select-management'));
const ChooseDetail = React.lazy(() => import('@pages/paper/manager/select-management/choose-detail'));
const PorjectChangeManagement = React.lazy(() => import('@pages/paper/manager/project-change-management'));
const PorjectChangeDetail = React.lazy(() => import('@pages/paper/manager/project-change-management/project-change'));
const AllotSecondAdviser = React.lazy(() => import('@pages/paper/manager/allot-second-adviser'));
const AllotSecondAdviserDetail = React.lazy(() => import('@pages/paper/manager/allot-second-adviser/project-detail'));
const TaskAndPlan = React.lazy(() => import('@pages/paper/manager/task-and-plan'));
const TaskAndPlanDetail = React.lazy(() => import('@pages/paper/manager/task-and-plan/detail'));
const PreliminaryAuditManagementDouble = React.lazy(() => import('@pages/paper/manager/preliminary-audit-double'));
const PreliminaryAuditManagementDoubleDetail = React.lazy(() => import('@pages/paper/manager/preliminary-audit-double/detail'));
const PreliminaryAuditManagementSingle = React.lazy(() => import('@pages/paper/manager/preliminary-audit-single'));
const PreliminaryAuditManagementSingleDetail = React.lazy(() => import('@pages/paper/manager/preliminary-audit-single/detail'));
const InterimAuditManagementDouble = React.lazy(() => import('@pages/paper/manager/interim-audit-double'));
const InterimAuditManagementDoubleDetail = React.lazy(() => import('@pages/paper/manager/interim-audit-double/detail'));
const InterimAuditManagementSingle = React.lazy(() => import('@pages/paper/manager/interim-audit-single'));
const InterimAuditManagementSingleDetail = React.lazy(() => import('@pages/paper/manager/interim-audit-single/detail'));
const ReviewExpertManage = React.lazy(() => import('@pages/paper/manager/review-expert-manage'));
const ReviewExpertManageDetail = React.lazy(() => import('@pages/paper/manager/review-expert-manage/detail'));
const ExpenseManagement = React.lazy(() => import('@pages/paper/manager/expense-management'));
const ProcurementManagement = React.lazy(() => import('@pages/paper/manager/procurement-management'));
const DuplicateCheckManagement = React.lazy(() => import('@pages/paper/manager/duplicate-check-management'));
const DuplicateCheckManagementDetail = React.lazy(() => import('@pages/paper/manager/duplicate-check-management/detail'));
const ArbitrationManage = React.lazy(() => import('@pages/paper/manager/arbitration-manage'));
const ArbitrationManageDetail = React.lazy(() => import('@pages/paper/manager/arbitration-manage/detail'));
const BannerManagement = React.lazy(() => import('@pages/content/banner-management'));
const LinksManagement = React.lazy(() => import('@pages/content/links-management'));
const NewsManagement = React.lazy(() => import('@pages/content/message-management'));
const NewsManagementOperate = React.lazy(() => import('@pages/content/message-management/operate-new'));
const NavManagement = React.lazy(() => import('@pages/content/nav-management'));
const ContentManagement = React.lazy(() => import('@pages/content/content-management'));
const ContentManagementOperate = React.lazy(() => import('@pages/content/content-management/operate-content'));
const OperationLog = React.lazy(() => import('@pages/system/operation-log'));
const LoginLog = React.lazy(() => import('@pages/system/login-log'));
const UserM = React.lazy(() => import('@pages/system/user'));
const RegisterM = React.lazy(() => import('@pages/system/register'));
const RegisterInfo = React.lazy(() => import('@pages/system/register/register-info'));
const Profile = React.lazy(() => import('@pages/profile'));
const DataManagePaper = React.lazy(() => import('@pages/data-manage/paper'));
const DataManageSubjectInfo = React.lazy(() => import('@pages/data-manage/subject-info'));

const MasterRouter = () => {
  const getCatchKey = (props: any) => {
    return props.pathname;
  };

  return (
    <>
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/home/my_todo" component={Todo} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/home/guide_log" component={GuidanceLogList} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/project_setting" component={ProjectBaseSetting} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/pro_time_setting" component={ProjectTimeSetting} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/stu_manage" component={StudentManagement} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/stu_manage/detail" component={Detail} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/teacher_manage" component={TeacherManagement} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/teacher_manage/project_list" component={ProjectList} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/teacher_manage/project_list/detail/:id" component={ProjectDetail} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/expert_manage" component={ExpertManagement} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/sub_audit_manage" component={AuditManagement} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/sub_audit_manage/detail/:id" component={ProjectDetailSub} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/pre_allot_manage" component={PreAllotManagement} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/pre_allot_manage/detail/:id" component={PreAllotDetail} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/select_sub_manage" component={SelectManagement} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/select_sub_manage/choose_detail/:id" component={ChooseDetail} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/project_change_manage" component={PorjectChangeManagement} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/project_change_manage/change_detail/:type/:id" component={PorjectChangeDetail} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/distb_second_teacher" component={AllotSecondAdviser} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/distb_second_teacher/detail/:id" component={AllotSecondAdviserDetail} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/tor_audit" component={TaskAndPlan} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/tor_audit/detail/:type/:id?" component={TaskAndPlanDetail} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/begin_audit_manage_double" component={PreliminaryAuditManagementDouble} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/begin_audit_manage_double/detail/:type/:id" component={PreliminaryAuditManagementDoubleDetail} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/begin_audit_manage_single" component={PreliminaryAuditManagementSingle} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/begin_audit_manage_single/detail/:type/:id" component={PreliminaryAuditManagementSingleDetail} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/interim_audit_manage_double" component={InterimAuditManagementDouble} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/interim_audit_manage_double/detail/:type/:id" component={InterimAuditManagementDoubleDetail} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/interim_audit_manage_single" component={InterimAuditManagementSingle} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/interim_audit_manage_single/detail/:type/:id" component={InterimAuditManagementSingleDetail} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/reveiw_expert_allot" component={ReviewExpertManage} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/reveiw_expert_allot/detail/:type/:id" component={ReviewExpertManageDetail} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/expenses_manage" component={ExpenseManagement} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/buy_manage" component={ProcurementManagement} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/duplicate_check_manage" component={DuplicateCheckManagement} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/duplicate_check_manage/:type/:id" component={DuplicateCheckManagementDetail} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/arbitration_manage" component={ArbitrationManage} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/paper_manage/arbitration_manage/detail/:id" component={ArbitrationManageDetail} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/content_manage/banner_manage" component={BannerManagement} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/content_manage/friendly-url_manage" component={LinksManagement} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/content_manage/message_manage" component={NewsManagement} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/content_manage/message_manage/:type/:id?" component={NewsManagementOperate} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/content_manage/nav_manage" component={NavManagement} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/content_manage/content_info_manage/:type/:id?" component={ContentManagementOperate} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/content_manage/content_info_manage" component={ContentManagement} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/data_manage/paper" component={DataManagePaper} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/data_manage/subject_info" component={DataManageSubjectInfo} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/profile" component={Profile} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/sys_manage/operate_log" component={OperationLog} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/sys_manage/login_log" component={LoginLog} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/sys_manage/register" component={RegisterM} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/sys_manage/register/:type/:id" component={RegisterInfo} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/sys_manage/user" component={UserM} />
      <CacheRoute multiple cacheKey={getCatchKey} exact path="/work/sys_manage/user/detail/:type/:id" component={UserM} />
    </>
  );
};

export default MasterRouter;
