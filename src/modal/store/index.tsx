import { combineReducers, configureStore } from '@reduxjs/toolkit';
import reducers from '@modal/reducer';
const rootReducers = combineReducers({
  ...reducers,
});

export type ReducerTypes = ReturnType<typeof rootReducers>;
export default configureStore({
  reducer: rootReducers,
});
