import profile from './profile';
import work from './work';

export const listPageCommonState = {
  pageInfo: {
    pageSize: 10,
    current: 1,
    total: 0,
  },
};
export default {
  profile,
  work,
};
