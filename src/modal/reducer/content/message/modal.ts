export interface MessageEntity {
  content: string;
  createTime: string;
  createUserid: number;
  createUsername: string;
  details: {
    id: number;
    messageId: number;
    userId: number;
  }[];
  files: {
    fileName: string;
    fileUrl: string;
  };
}
