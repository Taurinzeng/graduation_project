import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { listPageCommonState } from '@modal/reducer';
import { BannerEntity, BannerStateType } from './modal';
import { PageInfoType } from '@modal/reducer/modal';

const appSlice = createSlice({
  name: 'banner',
  initialState: {
    pageInfo: listPageCommonState.pageInfo,
    dataSource: [],
  } as BannerStateType,
  reducers: {
    setPageInfo: (state: BannerStateType, action: PayloadAction<PageInfoType>) => {
      state.pageInfo = action.payload;
    },
    setDataSource: (state: BannerStateType, action: PayloadAction<BannerEntity[]>) => {
      state.dataSource = action.payload;
    },
  },
});

export default appSlice.reducer;
