import { ListPageCommonState } from '@modal/reducer/modal';

export interface BannerStateType extends ListPageCommonState<BannerEntity> {}
export interface BannerEntity {
  id: number;
  /**
   * 标题
   */
  title: string;

  /**
   * 1 banner 2友情链接
   */
  type: string;

  /**
   * 更新时间
   */
  updateTime: string;

  /**
   * 是否英文站（1是 0否）
   */
  enFlag: string;

  /**
   * 是否中文站（1是 0否）
   */
  zhFlag: string;

  /**
   * 图片名称
   */
  imageName: string;

  /**
   * 图片地址
   */
  imagePath: string;

  /**
   * 链接地址英文
   */
  redirectEnUrl: string;
  /**
   * 链接地址
   */
  redirectUrl: string;
  /**
   * 排序权重
   */
  sortWeight: number;
  /**
   * 1启用 0禁用
   */
  status: number;
}
