export interface ListPageCommonState<T> {
  pageInfo: PageInfoType;
  dataSource: T[];
  selectedKeys?: string[];
  searchKey?: any;
}

export interface ResponseType {
  data: any;
  /**
   * 200表示成功
   */
  code: number;
  msg: string;
}

export interface PageInfoType {
  current: number;
  pageSize: number;
  total: number;
  pages?: number;
}

export enum TYPE_CODE {
  /**
   * 语言
   */
  LANGUAGE = '1',
  /**
   * 注册状态
   */
  REGISTER_STATUS = '2',
  /**
   * 职称
   */
  JOB = '3',
  /**
   * 指导教师类型
   */
  SUPERVISOR_TYPE = '4',
  /**
   * 审核结果
   */
  AUDIT_RESULT = '5',
  /**
   * 消息类型
   */
  MESSAGE_TYPE = '6',
  /**
   * 消息查阅状态
   */
  MESSAGE_CHECK_STATUS = '7',
  /**
   * 消息状态
   */
  MESSAGE_STATUS = '8'
}