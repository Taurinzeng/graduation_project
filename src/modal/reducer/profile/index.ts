import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import * as service from '@service/profile';

const initialState = {
  submitting: false,
};
export const login: any = createAsyncThunk('profile/login', async (params: any, { dispatch }) => {
  const { setSubmitting } = profileSlice.actions;
  dispatch(setSubmitting(true));
  const res = await service.doLogin(params);
  dispatch(setSubmitting(false));
  return res;
});
const profileSlice = createSlice({
  name: 'profile',
  initialState,
  reducers: {
    setSubmitting: (state: any, action: PayloadAction<boolean>) => {
      state.submitting = action.payload;
    },
  },
});

export default profileSlice.reducer;
