export interface MenuItemType {
  name: string;
  icon: string;
  title: string;
  subs: MenuItemType[];
  hideMenu: number;
}

export interface TabMenuType {
  url: string;
  tabName: string;
}
export interface WorkStateType {
  menus: MenuItemType[];
  selectedMenu?: MenuItemType[];
  selectedMenuKeys: string[];
  openMenuKeys: string[];
  tabMenus: TabMenuType[];
}
