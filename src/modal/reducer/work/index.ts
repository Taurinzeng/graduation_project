import { createAction, createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';
import { MenuItemType, TabMenuType, WorkStateType } from './model';
import { queryMenus } from '@service/work';
import { getCachingKeys, dropByCacheKey, getCachingComponents } from 'react-router-cache-route';
const initialState: WorkStateType = {
  menus: [] as MenuItemType[],
  selectedMenuKeys: [],
  selectedMenu: [] as MenuItemType[],
  openMenuKeys: [],
  tabMenus: [
    {
      url: 'home/overview',
      tabName: '预览',
    },
  ] as TabMenuType[],
};

const findMenuInfo = (menus: any[], items: MenuItemType[], paths: string[], pathDeep: number) => {
  for (let menu of menus) {
    if (menu.name === paths[pathDeep]) {
      items.push(menu);
      if (menu.subs) {
        findMenuInfo(menu.subs, items, paths, pathDeep + 1);
      }
      if (menu.noMenuSubs) {
        findMenuInfo(menu.noMenuSubs, items, paths, pathDeep + 1);
      }
      break;
    }
  }
};
const setTabMenusInfo = (currentMenuInfo: MenuItemType[], keyPath: string[], dispatch: any, tabMenus: TabMenuType[]) => {
  const url = keyPath.join('/');
  for (let menuInfo of tabMenus) {
    if (menuInfo.url === url) {
      return;
    }
  }
  // @ts-ignore
  dispatch(setTabMenus([...tabMenus, { url, tabName: currentMenuInfo[currentMenuInfo.length - 1].title }]));
};
export const setAllMenus = createAsyncThunk('work/setAllMenus', (keyPath: any, { dispatch, getState }) => {
  const menusInfo = [] as MenuItemType[];
  // @ts-ignore
  const { menus, tabMenus } = getState().work;
  findMenuInfo(menus, menusInfo, keyPath, 0);
  dispatch(setSelectedMenu(menusInfo));
  dispatch(setSelectedMenuKeys(keyPath));
  setTabMenusInfo(menusInfo, keyPath, dispatch, tabMenus);
});
export const getMenus = createAsyncThunk('work/getMenus', async (params: any, { dispatch }) => {
  const result: any = await queryMenus();
  if (result.success) {
    dispatch(setMenus(result.data));
  }
});
export const closePage = createAsyncThunk('work/closePage', async (params: any, { dispatch, getState }) => {
  // @ts-ignore
  const { tabMenus } = getState().work;
  const leftMenu = tabMenus.filter((menu: TabMenuType) => menu.url !== params.path);
  dispatch(setTabMenus(leftMenu));
  // 关闭当前tab后跳转上个页面
  if (!params.noJump) {
    params.history.replace('/work/' + leftMenu[leftMenu.length - 1].url);
  }
  //删除缓存页
  setTimeout(() => {
    dropByCacheKey('/work/' + params.path);
  }, 500);
  return leftMenu;
});
const appSlice = createSlice({
  name: 'work',
  initialState,
  reducers: {
    setSelectedMenuKeys: (state: WorkStateType, action: PayloadAction<string[]>) => {
      state.selectedMenuKeys = action.payload;
    },
    setOpenMenuKeys: (state: WorkStateType, action: PayloadAction<string[]>) => {
      state.openMenuKeys = action.payload;
    },
    setSelectedMenu: (state: WorkStateType, action: PayloadAction<MenuItemType[]>) => {
      state.selectedMenu = action.payload;
    },
    setMenus: (state: WorkStateType, action: PayloadAction<MenuItemType[]>) => {
      state.menus = action.payload;
    },
    setTabMenus: (state: WorkStateType, action: PayloadAction<TabMenuType[]>) => {
      state.tabMenus = action.payload;
    },
  },
});
export const { setSelectedMenuKeys, setOpenMenuKeys, setMenus, setSelectedMenu, setTabMenus } = appSlice.actions;
export default appSlice.reducer;
