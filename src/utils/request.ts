import { message } from 'antd';
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

const instance = axios.create();

instance.interceptors.request.use((request: AxiosRequestConfig) => {
  const lng = localStorage.getItem('i18nextLng');
  request.headers = {
    ...request.headers,
    'Accept-Language': lng === 'cn' ? 'zh' : lng,
  };
  return request;
});

instance.interceptors.response.use((response: AxiosResponse<any>) => {
  if (response.status === 200) {
    if (response.data.code === 302) {
      return response;
    }
    // @ts-ignore
    if (response.data.code !== 200 && !response.config.hideErrorMessage) {
      message.error(response.data.message);
      throw new Error();
    } else {
      return response;
    }
  } else if (response.status === 401) {
    window.location.href = '/login';
    return response;
  } else {
    // @ts-ignore
    if (!response.config.hideErrorMessage) {
      message.error(response.data.message);
      throw new Error();
    }
    return response;
  }
});
console.log(process.env.NODE_ENV);
export const prefix = '/api';
export const prefixB = '/bus';

export default instance;
