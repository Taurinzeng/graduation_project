import { Layout } from 'antd';
import CustomerHeader from './header';
import CustomerFooter from './footer';
import React from 'react';
import './index.less';

const { Header, Content, Footer } = Layout;
export default (props: any) => {
  return (
    <Layout className="custom-home-layout">
      <Header className="custom-header">
        <CustomerHeader/>
      </Header>
      <Layout className="custom-content">
        <Content className={'custom-content-content'}>{props.children}</Content>
        <Footer className={'custom-footer'}>
          <CustomerFooter/>
        </Footer>
      </Layout>
      
    </Layout>
  );
};
