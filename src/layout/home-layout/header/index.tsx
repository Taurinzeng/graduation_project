import React from 'react';
import Logo from '@assets/logof.png';
import En from '@assets/images/en.png';
import Cn from '@assets/images/cn.png';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import { Divider } from 'antd';

import './index.less';
import { useHistory } from 'react-router';
export default () => {
  const history = useHistory();

  const { t, i18n } = useTranslation();
  const { home } = translations;
  const isCn = localStorage.getItem('i18nextLng') === 'cn';
  const switchLanguage = (lan: string) => {
    i18n.changeLanguage(lan);
    location.reload();
  };
  return (
    <header className="home-header">
      <div className="left">
        <img src={Logo} />
        <Divider type="vertical" className="custom-divider" />
        <span className="header-title">{t(home.title)}</span>
      </div>
      <div className="right">
        <span
          className={`menu-item ${isCn ? '' : ' en'}`}
          onClick={() => {
            history.push('/exclude/home');
          }}>
          {t(home.home)}
        </span>
        <span
          className={`menu-item ${isCn ? '' : ' en'}`}
          onClick={() => {
            history.push('/exclude/new');
          }}>
          {t(home.news)}
        </span>
        <span
          className={`menu-item ${isCn ? '' : ' en'}`}
          onClick={() => {
            history.push('/exclude/guide');
          }}>
          {t(home.serviceDirectory)}
        </span>
        <span
          className={`menu-item ${isCn ? '' : ' en'}`}
          onClick={() => {
            history.push('/exclude/material');
          }}>
          {t(home.dataDownload)}
        </span>
        <span
          className={`menu-item ${isCn ? '' : ' en'}`}
          onClick={() => {
            history.push('/exclude/question');
          }}>
          {t(home.FAQ)}
        </span>
        <span className={`menu-item ${isCn ? '' : ' en'}`}>{t(home.aboutUs)}</span>
        {isCn ? (
          <img
            src={En}
            width="25"
            height="25"
            title="Switch to English"
            onClick={() => switchLanguage('en')}
          />
        ) : (
          <img src={Cn} width="25" height="25" title="切换成中文" onClick={() => switchLanguage('cn')} />
        )}
      </div>
    </header>
  );
};
