import React from 'react';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import { Divider } from 'antd';

import Style from './index.module.less';

export default () => {
  const { t } = useTranslation();
  return <div className={Style.footer}>
    <div className={Style.content}>
      <p>
        {t(translations.home.websitFirst)}
        <Divider type="vertical"/>
        {t(translations.home.tip)}
        <Divider type="vertical"/>
        {t(translations.home.show)}
      </p>
      <p>
        <span>COPYRIGHT</span>
        <span>2021</span>
        <span>Axure设计</span>
        <span>ALLRIGHT RESERVED.</span>
      </p>
      <p>
        <span><label>地址：</label>青海省西宁市昆仑路18号</span>
        <span><label>邮政编码：</label>010001</span>
        <span><label>联系电话：</label>020-63202558</span>
      </p>
      <p>
        <span>ICP备05001566</span>
        <span>公网安备</span>
        <span> 63280002000101号</span>
      </p>
    </div>
  </div>;
}