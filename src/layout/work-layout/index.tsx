import React, { useEffect, useState } from 'react';
import { Layout, Menu } from 'antd';
import CustomerSider from './sider';
import CustomerHeader from './header';
import Breadcrumb from './breadcrumb';
import PageTabs from './page-tabs';
import Style from './index.module.less';
import { useHistory } from 'react-router';
import { useDispatch, useSelector } from 'react-redux';
import { setAllMenus } from '@modal/reducer/work';
import { ReducerTypes } from '@modal/store';
const { Header, Sider, Content } = Layout;

export default (props: any) => {
  const [collapsed, setCollapsed] = useState(false);
  const history = useHistory();
  const dispatch = useDispatch();
  console.log(props);
  useEffect(() => {
    // 监听路由变化，创建标签页
    history.listen((cuLocation: any) => {
      if (cuLocation.pathname.startsWith('/work')) {
        const path = cuLocation.pathname.replace('/work/', '').split('/');
        dispatch(setAllMenus(path));
      }
    });
  }, []);
  return (
    <Layout className={Style.customer_layout}>
      <Sider
        className={Style.layout_sider}
        trigger={null}
        collapsible
        collapsed={collapsed}
        theme="light"
        width={260}>
        <CustomerSider />
      </Sider>
      <Layout>
        <Header className={Style.layout_header}>
          <CustomerHeader />
        </Header>
        <PageTabs />
        <Content className={Style.layout_content}>
          <Breadcrumb />
          <div className={Style.router_content}>{props.children}</div>
        </Content>
      </Layout>
    </Layout>
  );
};
