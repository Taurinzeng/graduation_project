import React, { useEffect } from 'react';
import { Menu } from 'antd';
import { useHistory } from 'react-router';
import { MenuItemType } from '@modal/reducer/work/model';
import { ReducerTypes } from '@modal/store';
import { useDispatch, useSelector } from 'react-redux';
import { setOpenMenuKeys, getMenus, setAllMenus } from '@modal/reducer/work';
import { HomeOutlined, FileTextOutlined, AppstoreOutlined, BarChartOutlined, GlobalOutlined, UserOutlined } from '@ant-design/icons';
import Logo from '@assets/logob.png';
import './index.less';

const menuIconMap = {
  home: <HomeOutlined />,
  paper_manage: <FileTextOutlined />,
  content_manage: <GlobalOutlined />,
  sys_manage: <AppstoreOutlined />,
  data_manage: <BarChartOutlined />,
  profile: <UserOutlined />,
};
interface IProps {}
export default (props: IProps) => {
  const dispatch = useDispatch();
  const { selectedMenuKeys, openMenuKeys, menus } = useSelector((state: ReducerTypes) => state.work);
  const history = useHistory();
  const renderMenu = (leftMenu: MenuItemType[]) => {
    return leftMenu.map((item: MenuItemType) => {
      if (item.subs) {
        return (
          //@ts-ignore
          <Menu.SubMenu key={item.name} icon={menuIconMap[item.name]} title={item.title}>
            {renderMenu(item.subs)}
          </Menu.SubMenu>
        );
      } else {
        return (
          //@ts-ignore
          <Menu.Item key={item.name} icon={menuIconMap[item.name]}>
            {item.title}
          </Menu.Item>
        );
      }
    });
  };
  const handleMenuClick = (item: any) => {
    const path = item.keyPath.slice().reverse();
    history.push(`/work/${path.join('/')}`);
  };
  const handleOpenChange = (openKeys: any) => {
    dispatch(setOpenMenuKeys(openKeys));
  };

  useEffect(() => {
    if (menus.length > 0) {
      const paths = location.pathname.replace('/work/', '').split('/');
      dispatch(setOpenMenuKeys(paths));
      dispatch(setAllMenus(paths));
    } else {
      dispatch(getMenus({}));
    }
  }, [menus]);
  return (
    <div className="customer-sider">
      <div className="title">
        {/* {
          // t(translations.work.title)
          'xxxxxxx'
        } */}
        <img src={Logo} />
      </div>
      <Menu className="customer-menu" mode="inline" selectedKeys={selectedMenuKeys} onClick={handleMenuClick} onOpenChange={handleOpenChange} openKeys={openMenuKeys}>
        {renderMenu(menus)}
      </Menu>
    </div>
  );
};
