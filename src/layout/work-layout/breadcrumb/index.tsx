import { MenuItemType } from '@modal/reducer/work/model';
import { ReducerTypes } from '@modal/store';
import { Breadcrumb } from 'antd';
import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';

import Style from './index.module.less';

export default () => {
  const { selectedMenu = [] } = useSelector((state: ReducerTypes) => state.work);
  const generateBreadCrumb = () => {
    return selectedMenu?.map((menu: MenuItemType) => (
      <Breadcrumb.Item key={menu.name}>{menu.title}</Breadcrumb.Item>
    ));
  };
  return (
    <div className={Style.custom_breadcrumb}>
      <Breadcrumb>{generateBreadCrumb()}</Breadcrumb>
      <div className={Style.title}>
        {selectedMenu.length > 0 ? selectedMenu[selectedMenu.length - 1].title : ''}
      </div>
    </div>
  );
};
