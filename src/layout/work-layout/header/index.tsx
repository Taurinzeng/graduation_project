import React from 'react';
import AvatarImage from '@assets/images/2.jpg';
import { Dropdown, Menu, Avatar } from 'antd';
import { useTranslation } from 'react-i18next';
import { translations } from '@locales/i18n';
import ProjectSelect from './project-select';
import { UserOutlined, SettingOutlined, LogoutOutlined } from '@ant-design/icons';
import './index.less';
import { Link } from 'react-router-dom';
export default () => {
  const { t, i18n } = useTranslation();
  const menu = (
    <Menu style={{ width: 170, textAlign: 'center' }}>
      <Menu.Item>
        <Link to="/work/profile" style={{ padding: '10px 0' }}>
          <SettingOutlined />
          {t(translations.app.layout.header.profileCenter)}
        </Link>
      </Menu.Item>
      <Menu.Item>
        <a style={{ padding: '10px 0' }}>
          <LogoutOutlined />
          {t(translations.app.layout.header.logout)}
        </a>
      </Menu.Item>
    </Menu>
  );
  const lanMenu = (
    <Menu>
      <Menu.Item onClick={() => handleLanChange('en')}>
        <a>English</a>
      </Menu.Item>
      <Menu.Item onClick={() => handleLanChange('cn')}>
        <a>中文</a>
      </Menu.Item>
    </Menu>
  );
  const handleLanChange = (lan: string) => {
    i18n.changeLanguage(lan);
    location.reload();
  };
  return (
    <header className="customer-header">
      <ProjectSelect />
      <div className="header-right">
        {/* <Dropdown overlay={lanMenu}>
          <div className="language-icon">
            <TranslationOutlined />
          </div>
        </Dropdown> */}
        <Dropdown overlay={menu} placement="bottomRight">
          <div>
            <Avatar src={AvatarImage} />
            <span className="username">Taurin</span>
          </div>
        </Dropdown>
        {/* <BellOutlined style={{ fontSize: 24, color: '#fff' }} /> */}
      </div>
    </header>
  );
};
