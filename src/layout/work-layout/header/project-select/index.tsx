import { translations } from '@locales/i18n';
import { Divider, Dropdown, Menu, Modal } from 'antd';
import { CaretDownOutlined } from '@ant-design/icons';
import React, { useState } from 'react';
import { useTranslation } from 'react-i18next';

export default () => {
  const [currentKey, setCurrentKey] = useState('2020');
  const { t } = useTranslation();
  const handleMenuClick = (record: any) => {
    if (record.key === 'create') {
      Modal.confirm({
        content: (
          <div>
            {t(translations.work.createConfirm)}
            <br />
            <span style={{ color: 'rgba(0,0,0,0.5)', fontSize: 12 }}>{t(translations.work.createName)}</span>
          </div>
        ),
      });
    } else {
      setCurrentKey(record.key);
    }
  };
  const menu = (
    <Menu
      onClick={handleMenuClick}
      selectedKeys={[currentKey]}
      style={{ width: 170, textAlign: 'center', color: 'red' }}>
      <Menu.Item key="2020">
        <a style={{ padding: '10px 0' }}>2020毕业设计</a>
      </Menu.Item>
      <Menu.Item key="2021">
        <a style={{ padding: '10px 0' }}>2021毕业设计</a>
      </Menu.Item>
      <Divider style={{ margin: 0 }} />
      <Menu.Item key="create">新增</Menu.Item>
    </Menu>
  );

  return (
    <Dropdown overlay={menu} trigger={['click']}>
      <a>
        {/* 2020毕业设计 <CaretDownOutlined /> */}
        test <CaretDownOutlined />
      </a>
    </Dropdown>
  );
};
