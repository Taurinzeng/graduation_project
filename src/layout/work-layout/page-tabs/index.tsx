import React, { useEffect, useState } from 'react';
import { Tabs } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import { useHistory } from 'react-router-dom';
import { TabMenuType } from '@modal/reducer/work/model';
import { ReducerTypes } from '@modal/store';
import { CloseOutlined } from '@ant-design/icons';
import Style from './index.module.less';
import { closePage, setTabMenus } from '@modal/reducer/work';
export default () => {
  const { selectedMenuKeys, tabMenus } = useSelector((state: ReducerTypes) => state.work);
  const [activeKey, setActiveKey] = useState<string>('');
  const dispatch = useDispatch();
  const history = useHistory();
  const handleTabClick = (key: string) => {
    setActiveKey(key);
    history.push('/work/' + key);
  };
  const handelTabClose = (targetKey: any) => {
    const noJump = targetKey !== activeKey;
    dispatch(closePage({ path: targetKey, history, noJump }));
    // if (tabMenus.length >= 2) {
    //   const leftMenu = tabMenus.filter((menu: TabMenuType) => menu.url !== targetKey);
    //   dispatch(setTabMenus(leftMenu));
    //   if (targetKey === activeKey) {
    //     handleTabClick(leftMenu[leftMenu.length - 1].url);
    //   }
    // }
  };
  const tabName = (name: string, key: string, hideClose: boolean) => {
    return (
      <div className={Style.tabName}>
        <span className={Style.text} onClick={() => handleTabClick(key)}>
          {name}
        </span>
        {!hideClose && <CloseOutlined className={Style.tabIcon} onClick={() => handelTabClose(key)} />}
      </div>
    );
  };
  useEffect(() => {
    setActiveKey(selectedMenuKeys.join('/'));
  }, [selectedMenuKeys]);
  return (
    <div className={Style.page_tabs}>
      <Tabs type={'card'} hideAdd activeKey={activeKey}>
        {tabMenus.map((menu: TabMenuType, index: number) => (
          <Tabs.TabPane tab={tabName(menu.tabName, menu.url, index === 0)} key={menu.url} />
        ))}
      </Tabs>
    </div>
  );
};
