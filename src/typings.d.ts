declare module '*.module.less' {
  const resource: { [key: string]: string };
  export = resource;
}

declare module '*.jpg' {
  const content: string;
  export default content;
}

declare module '*.png' {
  const content: string;
  export default content;
}

declare module '*.pdf' {
  const content: string;
  export default content;
}
declare module '*.svg' {
  const content: string;
  export default content;
}

declare module 'quill' {
  const Quill: any;
  export default Quill;
}
declare module 'quill-image-resize-module' {
  const QuillModule: any;
  export default QuillModule;
}
declare module 'react-quill';
