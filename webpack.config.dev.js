const { merge } = require('webpack-merge');
const base = require('./webpack.config.base');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = merge(base, {
  entry: './src/index.tsx',
  mode: 'development',
  devtool: 'eval-cheap-module-source-map',
  devServer: {
    port: 3005,
    historyApiFallback: true,
    hot: true,
    proxy: {
      '/api/*': {
        target: 'http://47.108.13.8:3000/mock/10',
        pathRewrite: { '^/api': '' },
        changeOrigin: true,
      },
      '/bus/*': {
        target: 'http://47.108.13.8:3000/mock/86',
        pathRewrite: { '^/bus': '' },
        changeOrigin: true,
      },
    },
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'src/index.html',
    }),
  ],
});
