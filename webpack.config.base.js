const path = require('path');
const webpack = require('webpack');
module.exports = {
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'index.js',
    assetModuleFilename: 'static/[hash][ext][query]',
    publicPath: '/'
  },
  module: {
    rules: [
      {
        test: /\.(js|jsx|ts|tsx)$/,
        use: [
          {
            loader: 'babel-loader',
            options: {
              cacheDirectory: false,
              presets: ['@babel/react', '@babel/env'],
            },
          },

          {
            loader: 'ts-loader',
            options: {
              // configFile: path.resolve(__dirname, './tsconfig.json'),
              // happyPackMode: true
            },
          },
        ],
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
          },
        ],
      },
      {
        test: /\.less$/,
        use: [
          {
            loader: 'style-loader',
          },
          {
            loader: 'css-loader',
          },
          {
            loader: 'less-loader',
            options: {
              lessOptions: {
                modifyVars: {
                  '@primary-color': '#1890ff',
                },
                javascriptEnabled: true,
              },
             
            }
          },
        ],
      },
      {
        test: /\.(jpg|jpeg|gif|png|svg|pdf)$/,
        type: 'asset',
        parser: {
          dataUrlCondition: {
            maxSize: 1 * 1024 // 4kb
          }
        },
      },
      {
        test: /\.(eot|ttf|woff|woff2)$/,
        type: 'asset',
        parser: {
          dataUrlCondition: {
            maxSize: 1 * 1024 // 4kb
          }
        }
      },
    ],
  },
  plugins: [
    new webpack.ProvidePlugin({
      'window.Quill': 'quill/dist/quill.js',
      'Quill': 'quill/dist/quill.js'
    })
  ],
  resolve: {
    extensions: [".ts", ".tsx", ".js", "jsx"],
    alias: {
      "@utils": path.resolve("src/utils"),
      "@assets": path.resolve("src/assets"),
      "@components": path.resolve("src/components"),
      "@pages": path.resolve("src/pages"),
      "@modal": path.resolve("src/modal"),
      "@service": path.resolve("src/services"),
      "@locales": path.resolve("src/locales"),
    },
  },
};
